Data Logging
============

Software requirements and software artefacts
--------------------------------------------

The following tables reports the software requirements and the software artefacts to be installed for Logging component.  The Elasticsearch platform is used as a generic efficient data storage and analytics tool. The Graylog integrates it in order to populate the logging event information. The WeLive logging building block is built on top of both providing the APIs to insert data as well as to perform various queries regarding that data.

+--------------------------+--------------------------------------+-----------+
| WeLive tool              | Software requirement                 | Version   |
+==========================+======================================+===========+
| LOG                      | Oracle Java                          | 7         |
+--------------------------+--------------------------------------+-----------+
|                          | ElasticSearch                        | 1.7.3     |
+--------------------------+--------------------------------------+-----------+
|                          | MongoDB                              | 2.x       |
+--------------------------+--------------------------------------+-----------+
|                          | Graylog 2 server                     | 1.2.2     |
+--------------------------+--------------------------------------+-----------+
|                          | Graylog 2 web interface              | 1.2.2     |
+--------------------------+--------------------------------------+-----------+
|                          | Apache Tomcat                        | 7.x       |
+--------------------------+--------------------------------------+-----------+

Table 1 – Software requirements of Logging BB

+--------------------------+-----------------------------------+---------------+
| WeLive tool              | Software artefact                 | Artefact type |
+==========================+===================================+===============+
| LOG                      | welive.logging.war                | war           |
+--------------------------+-----------------------------------+---------------+

Table 2 – Software artefact of Logging BB

Installation guide
------------------

Installing software components
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Elasticsearch
^^^^^^^^^^^^^

Install Elasticsearch with following instructions::

	Download from project site application binaries (version 1.7.3)
	Unzip the archive on the server installation folder

More details can be found here: https://www.elastic.co/products/elasticsearch

MongoDB
^^^^^^^

Install MongoDB with following instructions::

	Install MongoDB from your Linux distribution package system.

More details can be found here: https://docs.mongodb.org/manual/installation/

Graylog
^^^^^^^

Install Graylog server with following instructions::

	Download from project site application binaries version 1.2.2
	Unzip the archive on the server installation folder
	Install Graylog web interface with following instructions:
	Download from project site application binaries version 1.2.2
	Unzip the archive on the server installation folder

More details may be found here: https://www.graylog.org/download

Configuration
-------------

Elasticsearch
^^^^^^^^^^^^^

Open config/elasticsearch.yml and set cluster.name: graylog2

Start Elasticsearch server with the following command (from Elasticsearch root folder)::

	> ./bin/elasticsearch

Using a REST client run following REST call to configure index (used curl in the example)::

	curl -X PUT “http://localhost:9200/graylog2_0”

.. code-block:: json

	{
	"mappings": {
		"my_type": {
			"dynamic_templates": [{
				"notanalyzed": {
					"match": "*",
					"match_mapping_type": "string",
					"mapping": {
						"type": "string",
						"index": "not_analyzed"
					}
				}
			}]
		}
	}
	}

Graylog Server
^^^^^^^^^^^^^^

Copy the example configuration file::

	> cp graylog.conf.example conf/server.conf

You can leave most variables as they are for the first start.

Configure at least the following variables in conf/server.conf::

	is_master = true

Set only one graylog-server node as the master. This node will perform periodical and maintenance actions that slave nodes will not. Every slave node will accept messages just as the master nodes. Nodes will fall back to slave mode if there already is a master in the cluster.::

	password_secret

You must set a secret that is used for password encryption and salting here. The server will refuse to start if it is not set. Generate a secret with for example pwgen -N 1 -s 96. If you run multiple graylog-server nodes, make sure you use the same password_secret for all of them!::

	root_password_sha2

A SHA2 hash of a password you will use for your initial login. Set this to a SHA2 hash generated with echo -n yourpassword | shasum -a 256 and you will be able to log in to the web interface with username admin and password yourpassword.::

	elasticsearch_max_docs_per_index = 20000000

How many log messages to keep per index. This setting multiplied with elasticsearch_max_number_of_indices results in the maximum number of messages in your Graylog setup. It is always better to have several more smaller indices than just a few larger ones.::

	elasticsearch_max_number_of_indices = 20

How many indices to have in total. If this number is reached, the oldest index will be deleted. Also take a look at the other retention strategies that allow you to automatically delete messages based on their age.::

	elasticsearch_shards = 4

The number of shards for your indices. A good setting here highly depends on the number of nodes in your Elasticsearch cluster. If you have one node, set it to 1.::

	elasticsearch_replicas = 0

The number of replicas for your indices. A good setting here highly depends on the number of nodes in your Elasticsearch cluster. If you have one node, set it to 0. ::

	mongodb_*

Enter your MongoDB connection and authentication information here. Make sure that you connect the web interface to the same database. You don’t need to configure mongodb_user and mongodb_password if mongodb_useauth is set to false.
Start the server with following command::

	> export GRAYLOG_HOME=<GRAYLOG_INSTALLATION_FOLDER>
	> export GRAYLOG_CONF=${GRAYLOG_HOME}/conf/server.conf
	> $GRAYLOG_HOME/bin/graylogctl start


Graylog Web interface
^^^^^^^^^^^^^^^^^^^^^

Open conf/graylog-web-interface.conf and set the two following variables::

	> cp graylog.conf.example conf/server.conf

::

	graylog2-server.uris="http://127.0.0.1:12900/":

1. This is the list of graylog-server nodes the web interface will try to use. You can configure one or multiple, separated by commas. Use the rest_listen_uri (configured in graylog.conf) of your graylog-server instances here.::

		application.secret="":

2. A secret for encryption. Use a long, randomly generated string here. (for example generated using pwgen -N 1 -s 96)

Start the web interface with following command::

	> bin/graylog-web-interface

Open a browser and visit url http://server_ip:9000

Log in console with default user admin/admin

Go to System > Users and create a new user with Admin role.

Go to System > Inputs and create a GELF HTTP input::

	Title : logging input
	Bind Address: 0.0.0.0
	Port : 9999

Leave other fields with default values.

WeLive logging packaging and deployment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Git clone the 'welive-logging' project source from::

	https://bitbucket.org/welive/welive-logging/

Edit configuration in file src/main/resources/application.properties and set user admin created in previous paragraph .in following properties graylog.admin.username graylog.admin.password. Also set the corresponding 'query' and 'store' endpoint::

	graylog.store.endpoint=http://localhost:9999/gelf
	graylog.query.endpoint=http://localhost:12900
	graylog.admin.username=admin
	graylog.admin.password=admin
	
	elastic.url=http://localhost:9200
	elastic.index=graylog2_0
	
	logging.secured=true
	logging.basic.token=<Basic Auth Token to be checked for validation>
	aac.url=<Endpoint of AAC>
	logging.protected=<List of protected application ids>
	logging.valitation.apps=<List of application Ids to be validated for before logging>
	logging.mongo.host=<Endpoint of Mongo host>
	logging.mongo.port=<Port of Mongo host>


To create an application archive (war file), the Logging BB implementation relies on Apache Maven packaging system. That is the following command should be executed from the root of the 'welive-logging' source folder::

	> mvn clean package

The resulting welive.logging.war file can be found at target folder and should be deployed in the J2EE Web application container. In case of Apache Tomcat, it is sufficient to copy the file to the webapps folder of the Tomcat installation.
