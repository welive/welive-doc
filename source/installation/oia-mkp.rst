Ideation Board (Open Innovation Area) & Services (Marketplace)
=========================================================

Requirements
-------------

Software
^^^^^^^^
Open Innovation Area (OIA) and Marketplace (MKP) have the following requirements that must be correctly installed and configured. Follow the instructions provided for your platform to install them before proceeding.

*	`Java SE Development Kit 7 <http://docs.oracle.com/javase/7/docs/webnotes/install/>`_
* `Liferay Portal Community Edition - Bundled with Tomcat 6.2 GA4 <https://www.liferay.com/it/documentation/liferay-portal/6.2/user-guide/-/ai/installation-and-setup-liferay-portal-6-2-user-guide-15-en>`_
* `PostgreSQL 8.1 <https://wiki.postgresql.org/wiki/Detailed_installation_guides>`_
* `Liferay Social Office Community Edition 3.1.0 <https://www.liferay.com/it/marketplace/-/mp/application/15190404>`_
* `Sesame OpenRDF 2.7 <http://rdf4j.org/>`_

Artefacts
^^^^^^^^^
These are the artefacts that must be installed in order to run the OIA and MKP:

**OIA & MKP**

* *CustonThumbsRatingDisplay-hook.war*
* *Essential-core-social-theme-6.2.0.1.war*
* *Essential-core-theme-6.2.0.1.war*
* *Essential-Dockbar-hook-6.2.0.1.war*
* *Essential-Login-hook-6.2.0.1.war*
* *Essential-Overlay-portlet-6.2.0.1.war*
* *FlagAs-hook.war*
* *language-welive-hook-6.2.0.1.war*
* *OpenSocial Gadget CE.lpkg*
* *portal-ext.properties*
* *search-url-users-redirectToSOprofile-hook-6.2.0.1.war*
* *Social Office CE_mod.lpkg*
* *welive_logo.png*

**OIA**

* *contacts-portlet-6.2.0.10.war*
* *endorser-service-portlet-6.2.0.1.war*
* *Reputation-portlet-6.2.0.1.war*
* *Challenge62-portlet-6.2.0.1.war*
* *chat-portlet-6.2.0.3.war*
* *ideaWorkflow_welive.xml*
* *tasks-portlet-6.2.0.1.war*

**MKP**

* *MarketplaceStore-portlet-6.2.0.1.war*

Liferay Setup
-------------
The installation of OIA and MKT is based on Liferay, a Java free and open source enterprise portal software product, distributed under the GNU Lesser General Public License. So once Java and PostgreSQL are installed, `install Liferay <https://www.liferay.com/it/documentation/liferay-portal/6.2/user-guide/-/ai/installation-and-setup-liferay-portal-6-2-user-guide-15-en>`_.

Bundled Tomcat configuration and startup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In order to increase the duration of the working sessions, modify the file **web.xml**, in **tomcat/conf**, inside the bundled Liferay.

.. code-block:: xml

	<!-- =========== Default Session Configuration ================= -->
	<!-- You can set the default session timeout (in minutes) for all newly   -->
	<!-- created sessions by modifying the value below.                       -->

	<session-config>
	    <session-timeout>30</session-timeout>
	</session-config>

increasing ``session-timeout`` to 90.

In order to increase the allocation of RAM memory, modify the file **setenv.sh**, in **tomcat/bin**. The file should contain the following lines:

.. code-block:: sh

	CATALINA_OPTS="$CATALINA_OPTS -Dfile.encoding=UTF8 -Djava.net.preferIPv4Stack=true  -Dorg.apache.catalina.loader.WebappClassLoader.ENABLE_CLEAR_REFERENCES=false -Duser.timezone=Europe/Rome -Xms2048m -Xmx2048m -XX:PermSize=512m

At this point, `startup the bundled Tomcat <https://www.liferay.com/it/documentation/liferay-portal/6.2/user-guide/-/ai/installing-a-bundle-liferay-portal-6-2-user-guide-15-en>`_.

Basic configuration
^^^^^^^^^^^^^^^^^^^
Now, make the first access to the portal, accessing the address and the port where the Tomcat is listening, i.e. http://localhost:8080/. In the panel *Basic configuration* set:

* Section *Portal*

	* **Portal name**: WeLive

* Section *Administrator User*

	* **First name**: WeLive
	* **Last name**: Administrator

* Section *Database*

	* Select *Change*
	* **Database Type**: PostgreSQL
	* **JDBC URL**: jdbc:postgresql://localhost:5432/dbname, where dbname  is the database name chosen in the phase of Postgres installation, i.e. welive
	* **Username**: the username chosen in the phase of PostgreSQL installation, i.e. postgres
	* **Password**: the password chosen in the phase of PostgreSQL installation, i.e. postgres

Follow the wizard to choose the administrator password and complete the installation.

Portal Settings
^^^^^^^^^^^^^^^
Once finished the installation procedure, login as administrator (user WeLive Administrator) and go to *Control Panel > Portal Setting*. Then:

* In *General panel* set:

	* **Mail Domain**: mail domain chosen, i.e. welive.eu
	* **Virtual Host**: address domain chosen, i.e. welive.eu

* In *Users* panel add to *Roles* field:

	* **Citizen**
	* **Social Office User**

* In *Email Notifications* panel set:

	* **Name**: WeLive Administrator
	* **Address**: the mail address chosen, i.e. admin@welive.eu

* In *Display Settings* panel set as *Current Available Languages*:

	* English (United Kingdom)
	* Finnish (Finland)
	* Serbian (Serbia)
	* Serbian (Serbia, latin)
	* Spanish (Spain)

Mail Configuration
^^^^^^^^^^^^^^^^^^
Go to *Server Administration > Mail* and set:

* **Outgoing SMTP Server**: the IP or address of SMTP service chosen
* **Outgoing Port**: the port of SMTP service chosen
* Check **Use a Secure Network Connection** if necessary
* **User Name**, **Password** if necessary

Portal-ext properties and restart
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The file named **portal-ext.properties** must be placed in the same directory where is located the self-generated file **portal-setup-wizard.properties**, that is at first level of Liferay directory installation.

Restart the Tomcat server.

Apps installation
^^^^^^^^^^^^^^^^^
Now access to Liferay as Administrator and move to *Apps > App Manager > Install*. From here, install all the following LPKGs:

* *OpenSocial Gadget CE.lpkg*
* *Social Office CE_mod.lpkg*

Then proceed with the following WARs:

* *contacts-portlet-6.2.0.10.war*
* *CustonThumbsRatingDisplay-hook.war*
* *endorser-service-portlet-6.2.0.1.war*
* *Essential-core-social-theme-6.2.0.1.war*
* *Essential-core-theme-6.2.0.1.war*
* *Essential-Dockbar-hook-6.2.0.1.war*
* *Essential-Login-hook-6.2.0.1.war*
* *Essential-Overlay-portlet-6.2.0.1.war*
* *FlagAs-hook.war*
* *language-welive-hook-6.2.0.1.war*
* *Reputation-portlet-6.2.0.1.war*
* *search-url-users-redirectToSOprofile-hook-6.2.0.1.war*

Now, deactivate the following unnecessary portlets:

* *Amazon Rankings*
* *Hello Velocity*
* *Hello World*
* *Loan Calculator*
* *Network Utilities*
* *Password Generator*
* *Recent Downloads*
* *Shopping*
* *Web Proxy*
* *XSL Content*

Roles and Permissions
^^^^^^^^^^^^^^^^^^^^^
Go to *Control Panel > Users* and add *Regular Role*:

	* AdminMarketplace
	* Authority
	* Business
	* Citizen

Give to **AdminMarketplace** role the following permissions:

* **Documents and Media > Document**: *Delete*
* **Documents and Media > Document**: *Permissions*
* **Documents and Media > Document**: *Update*
* **Documents and Media > Document**: *View*
* **Documents and Media > Documents**: *Add Document*
* **Documents and Media > Documents**: *Add Folder*
* **Documents and Media > Documents**: *Permissions*
* **Documents and Media > Documents**: *Update*
* **Documents and Media > Documents**: *View*
* **Documents and Media > Documents Folder**: *Access*
* **Documents and Media > Documents Folder**: *Add Document*
* **Documents and Media > Documents Folder**: *Add Subfolder*
* **Documents and Media > Documents Folder**: *Delete*
* **Documents and Media > Documents Folder**: *Permissions*
* **Documents and Media > Documents Folder**: *Update*
* **Documents and Media > Documents Folder**: *View*
* **Marketplace Configuration**: *Access in Site Administration*
* **Marketplace Configuration**: *Permissions*
* **Marketplace Configuration**: *View*
* **Sites > Site**: *Go to Site Administration*

Give to **Authority** role the following permissions:

* **Calendar**: *View Event Details*
* **Documents and Media**: *Permissions*
* **Documents and Media > Document**: *Delete*
* **Documents and Media > Document**: *Permissions*
* **Documents and Media > Document**: *Update*
* **Documents and Media > Document**: *View*
* **Documents and Media > Documents**: *Add Document*
* **Documents and Media > Documents**: *Add Folder*
* **Documents and Media > Documents**: *Permissions*
* **Documents and Media > Documents**: *Update*
* **Documents and Media > Documents**: *View*
* **Documents and Media > Documents Folder**: *Access*
* **Documents and Media > Documents Folder**: *Add Subfolder*
* **Documents and Media > Documents Folder**: *Permissions*
* **Documents and Media > Documents Folder**: *View*
* **Favorite Challenges**: *Add to Page*
* **Favorite Challenges**: *View*
* **My Challenges**: *Add to Page*
* **My Challenges**: *View*
* **Tasks**: *View*
* **Tasks > Tasks**: *Add Entry*
* **Tasks > Tasks Entry**: *Add Discussion*
* **Tasks > Tasks Entry**: *Delete Discussion*
* **Tasks > Tasks Entry**: *Update*
* **Tasks > Tasks Entry**: *Update Discussion*
* **Tasks > Tasks Entry**: *View*

Give to **Business** role the following permissions:

* **Documents and Media > Document**: *Delete*
* **Documents and Media > Document**: *Permissions*
* **Documents and Media > Document**: *Update*
* **Documents and Media > Document**: *View*
* **Documents and Media > Documents**: *Add Document*
* **Documents and Media > Documents**: *Add Folder*
* **Documents and Media > Documents**: *Permissions*
* **Documents and Media > Documents**: *Update*
* **Documents and Media > Documents**: *View*
* **Documents and Media > Documents Folder**: *Access*
* **Documents and Media > Documents Folder**: *Add Document*
* **Documents and Media > Documents Folder**: *Add Subfolder*
* **Documents and Media > Documents Folder**: *Delete*
* **Documents and Media > Documents Folder**: *Permissions*
* **Documents and Media > Documents Folder**: *Update*
* **Documents and Media > Documents Folder**: *View*

Give to **Citizen** role the following permissions:

* **Calendar**: *View Event Details*
* **Documents and Media**: *Permissions*
* **Documents and Media > Document**: *Permissions*
* **Documents and Media > Documents**: *Add Document*
* **Documents and Media > Documents**: *Add Folder*
* **Documents and Media > Documents**: *Update*
* **Documents and Media > Documents**: *View*
* **Ideas explorer**: *View*
* **Tasks**: *View*
* **Tasks > Tasks**: *Add Entry*
* **Tasks > Tasks Entry**: *Add Discussion*
* **Tasks > Tasks Entry**: *Delete Discussion*
* **Tasks > Tasks Entry**: *Update*
* **Tasks > Tasks Entry**: *Update Discussion*
* **Tasks > Tasks Entry**: *View*

Add to **Power User** role the following permission:

* **Site Administration > Content > Documents and Media >  Document Folder**: *View*

Users
^^^^^
All users who sign up to the Portal have roles of *User*, *Power User*, *Social Office User* and *Citizen*.

The role of *AdminMarketplace* must be assigned to an user, possibly the Portal Administrator, i.e. WeLive Administrator.

The roles *Authority* (Public Administration, Municipalities etc) and *Business* (SME, freelance professional, startupper etc) must be assigned by the Portal Administrator, when register these users on WeLive.

Open Innovation Area
--------------------

Installation
^^^^^^^^^^^^
Access to Liferay as Administrator and move to *Apps > App Manager > Install*. From here, install all the following WARs:

* *Challenge62-portlet-6.2.0.1.war*
* *chat-portlet-6.2.0.3.war*
* *tasks-portlet-6.2.0.1.war*

Workflow Setup
^^^^^^^^^^^^^^
In order to setting up the workflow, go to *Control Panel > Configuration > Workflow*. Then:

* From *Definitions* tab, upload the file *ideaWorkflow.xml*
* From *Default Configuration* tab, associate to asset ``model.resource.it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea`` the workflow just uploaded

Configuration
^^^^^^^^^^^^^
In order to define the categories for Challenges and Ideas, go to *Site Administration > Content > Categories* and define *Vocabularies* and *Categories* of Site. Then, go to *Open Innovation Area configuration* and enable *Vocabularies* for Open Innovation Area:

* In the *Suffix* section, enter:

	* **Ideas suffix**: ideas_explorer
	* **Challenges suffix**: challenges_explorer

* In the *Map parameters* section, enter:

	* **Latitude**: 40.353237
	* **Longitude**: 18.172545

	Or the coordinates where to center the maps.

* In the *Challenges calendar*, chose:

	* WeLive (Site Calendar)

Marketplace
-----------

Sesame Installation
^^^^^^^^^^^^^^^^^^^
First of all, install and configure Sesame. Download *openrdf-sesame.war* and *openrdf-workbench.war* and install them on an independent Tomcat (do not use the bundled Liferay Tomcat!).

Now access openrdf-workbench page by the tomcat address plus */openrdf-workbench/*. From here, create a new repository with the following values:

	* **Type**: Native Java Store
	* **ID** and **Title**: a chosen label, e.g. welive1

Installation
^^^^^^^^^^^^
Open *MarketplaceStore-portlet-6.2.0.1.war*. Go to *WEB-INF/classes/portlet.properties* and set these properties:

.. code-block:: jproperties

	Crud.SesameRepositoryName=SESAME_REPOSITORY_NAME
	Crud.SesamServerURI=SESAME_REPOSITORY_BASE_URL

replacing ``SESAME_REPOSITORY_NAME`` with the ID chosen for the Sesame repository (i.e. welive1) and ``SESAME_REPOSITORY_BASE_URL`` with the base URL of Sesame repositories (i.e. https://dev.welive.eu/openrdf-sesame/repositories/).

At this point, access to Liferay as Administrator and move to *Apps > App Manager > Install* and install the WAR.

Final Configuration
-------------------

Pages
^^^^^
Login as administrator (user WeLive Administrator) and go to *Control Panel > Sites > WeLive*. Then add the following pages:

+-----------------------------+-----------------------+-----------------------------+
|  Page name                  |  Page type            |  Note                       |
+=============================+=======================+=============================+
|  overlay                    |  1 Column             |  Hide from Navigation Menu  |
+-----------------------------+-----------------------+-----------------------------+
|  Home	                      |  2 Columns (30/70)    |  Or rename Welcome page     |
+-----------------------------+-----------------------+-----------------------------+
|  Open Innovation Area       |	 2 Columns (50/50)    |	                            |
+-----------------------------+-----------------------+-----------------------------+
|  Marketplace                |  1-2 Columns (70/30)  |                             |
+-----------------------------+-----------------------+-----------------------------+
|  Artefacts definition tool  |	 1-2 Columns (70/30)  |	                            |
+-----------------------------+-----------------------+-----------------------------+
|  challenges_explorer        |  1 Column             |  Hide from Navigation Menu  |
+-----------------------------+-----------------------+-----------------------------+
|  ideas_explorer             |  1 Column             |  Hide from Navigation Menu  |
+-----------------------------+-----------------------+-----------------------------+

Only for the page **Artefacts definition tool** remove the *View* permission to *Guest* role and add it to *Authority*, *Business* and *Citizen* roles.

Now, go to *Site Templates* and choose *Social Office User Home*. Add the following page:

+-----------------------------+-----------------------+----------------------------------------------------------------+
|  Page name                  |  Page type            |  Note                                                          |
+=============================+=======================+================================================================+
|  My Page                    |  2 Columns (70/30)    |  Allow Site Administrators to Modify This Page for Their Site  |
+-----------------------------+-----------------------+----------------------------------------------------------------+

Go to *Site Templates* and choose *Social Office User Profile*. Go to *Profile page*, access the *Profile portlet* configuration and un-check the **Show Tags** properties.

Portlets
^^^^^^^^
Continuing as Admin:

* Go to *Overlay* page and install *Essential Overlay* portlet.
* Go to *Home page* and install *Reputation ranking* portlet.
* Go to *Open Innovation Area* page and install each of the following portlet (in the specified order):

	+----------------------------------+----------------------------------+
	|  Portlets left                   |  Portlets right                  |
	+==================================+==================================+
	|  Favourite Challenges\*\*        |  Favourite Ideas\*\*             |
	+----------------------------------+----------------------------------+
	|  Challenges Explorer\*           |  Ideas explorer\*                |
	+----------------------------------+----------------------------------+
	|  My Ideas\*\*                    |  My Need Reports\*\*             |
	+----------------------------------+----------------------------------+
	|  My Ideas in elaboration\*\*     |  My Ideas in refinement\*\*      |
	+----------------------------------+----------------------------------+
	|  My Ideas in implementation\*\*  |  My ideas in Monitoring\*\*      |
	+----------------------------------+----------------------------------+
	|  Ideas which I collaborate\*\*   |  Stats\*\*                       |
	+----------------------------------+----------------------------------+
	|  My Challenges\*\*\*             |  Need Reports\*\*\*              |
	+----------------------------------+----------------------------------+
	|  Ideas in elaboration\*\*\*      |  Ideas in refinement\*\*\*       |
	+----------------------------------+----------------------------------+
	|  Ideas in implementation\*\*\*   |  Ideas in monitoring\*\*\*       |
	+----------------------------------+----------------------------------+
	|  Authority Stats\*\*\*           |                                  |
	+----------------------------------+----------------------------------+

	where:

		\* must be accessible to all, also **Guest** users, so you have to give the *View* permission to *Guest* role by portlet configuration.

		\*\* must be accessible only to **Citizen**, so you have to give the *View* permission to *Citizen* role by portlet configuration.

		\*\*\* must be accessible only to **Authority**, so you have to give the *View* permission to *Authority* role by portlet configuration.

* Go to *Marketplace* page and install the *MarketplaceStore* portlet at the top of the page.
* Go to *Artefacts definition tool* page and install the *Usdl Editor* portlet at the top of the page.
* Go to *challenges_explorer* page and install *Challenges Explorer* portlet.
* Go to *ideas_explorer* page and install *Ideas Explorer* portlet.

Search
^^^^^^
Continuing as Admin:

* Go to *Control Panel > Sites* and choose *WeLive*. Then for each site pages install the *Search* portlet at the bottom of the page
* Go to *Control Panel > Sites Templates* and choose *Social Office User Home*. Then for each site templates pages install the *Search* portlet at the bottom of the page.
* Go to *Control Panel > Sites Templates* and choose *Social Office User Profile*. Then for each site templates pages install the *Search* portlet at the bottom of the page.F or each Search portlet just installed, access to its *Configuration*:

	* Give the *View* permission to *Guest* role.
	* Go to *Advanced configuration* and modify the JSON by removing all the values under ``values`` element and adding:

		.. code-block:: json

			[
				"com.liferay.portal.model.User",
				"it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge",
				"it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea",
				"it.eng.rspa.marketplace.artefatto.model.Artefatto"
			]

		This step is necessary to configure the portal search at the top of each page.

Theme
^^^^^
Continuing as Admin:

* Go to *Control Panel > Sites* and choose *WeLive*; then:

	* Set **Essential Core** with Colour Schemes **Core 2**
	* Insert in footer-text field **© 2015 WeLive | All rights reserved**
	* Go to *Logo* section and upload **welive_logo.png**

* Go to *Control Panel > Sites Templates* and choose *Social Office User Home*; then:

	* Set **Essential Core** with Colour Schemes **Core 2**
	* Insert in footer-text field **© 2015 WeLive | All rights reserved**
	* Go to *Logo* section and upload **welive_logo.png**

* Repeat the last steps choosing *Social Office User Profile*
