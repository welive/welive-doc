Welive Player
=============

The server provide data required by client. It makes additional REST calls in PRE and POST stages of operations to match requirement and to ensure data consistency. It help improves overall security by validating access token from client and at the same time improves the performance of access by maintaining internal cache.

Welive Player Server
--------------------

The server provide data required by client. It makes additional REST calls in PRE and POST stages of operations to match requirement and to ensure data consistency. It help improves overall security by validating access token from client and at the same time improves the performance of access by maintaining internal cache.

Software requirements and software artefacts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following tables reports the software requirements and the software artefacts to be installed for the WeLivePlayer Server.  

+------------------------------+---------------------------------------+--------------+
| WeLive tool                  | Software requirement                  | Version      |
+==============================+=======================================+==============+
| WeLive Player server         | Oracle Java                           | 7            |
+------------------------------+---------------------------------------+--------------+
|                              | Apache Tomcat                         | 7.0          |
+------------------------------+---------------------------------------+--------------+

Table 1 – Software requirements of WeLive Player

+--------------------------+-----------------------------------+----------------------+
| WeLive tool              | Software artefact                 | Artefact type        |
+==========================+===================================+======================+
| WeLive Player server     | weliveplayer-web.war              | war                  |
+--------------------------+-----------------------------------+----------------------+

Table 2 – Software artefact of WeLive Player

Installation guide
^^^^^^^^^^^^^^^^^^
Git clone the 'welive-player' project source from::

	https://bitbucket.org/welive/welive-player
	
Basic configuration
"""""""""""""""""""

Go to ‘src/main/resource’ and open file ‘weliveplayer.properties’. You will find the following list of properties:

+-----------------------+-------------------------+----------------------------------------------+
| Tool                  | Name                    | Value                                        |
+=======================+=========================+==============================================+
| AAC                   | ext.aacURL              | WeLive AAC endpoint                          |
+-----------------------+-------------------------+----------------------------------------------+
| CDV                   | welive.cdv.             | CDV profile API endpoint                     |
|                       | getUserprofile.uri      |                                              |
+-----------------------+-------------------------+----------------------------------------------+
|                       | welive.cdv.             | CDV update user profile endpoint             |
|                       | updateUserprofile.uri   |                                              |
+-----------------------+-------------------------+----------------------------------------------+
|                       | welive.cdv.getUserTags. | CDV get user tags endpoint                   |
|                       | uri                     |                                              |
+-----------------------+-------------------------+----------------------------------------------+
| MKP                   | welive.mkp.uri          | Marketplace API endpoint                     |
+-----------------------+-------------------------+----------------------------------------------+
|                       | welive.mkp.singleApp.uri| Marketplace API for single app information   |
+-----------------------+-------------------------+----------------------------------------------+
| WeLiveServer          | welive.server           | WeLive server endpoint address               |
+-----------------------+-------------------------+----------------------------------------------+
| DE                    | welive.de.user.recomm.  | Decision engine endpoint to get user         |
|                       | apps.uri                | recommendation                               |
+-----------------------+-------------------------+----------------------------------------------+
|                       |app.recommendation.radius| Default radius in km for search(10Km)        |
+-----------------------+-------------------------+----------------------------------------------+
| LOG                   | log.endpoint            | Logging endpoint                             |
+-----------------------+-------------------------+----------------------------------------------+
| PILOT(s) coordinates  | trento                  | 46.070023,11.128116                          |
+-----------------------+-------------------------+----------------------------------------------+
|                       | bilbao                  | 43.266192,-2.934357                          |
+-----------------------+-------------------------+----------------------------------------------+
|                       | novisad                 | 45.266114,19.834333                          |
+-----------------------+-------------------------+----------------------------------------------+
|                       | uusimaa                 | 60.213682,25.222895                          |
+-----------------------+-------------------------+----------------------------------------------+


Deployment
""""""""""
To create an application archive (war file), the welive-player implementation relies on Apache Maven packaging system. then, create web application archive (war) by executing the following command from the root director::

	> mvn clean package

The resulting war file can be found at target folder and should be deployed in the J2EE Web application container. In case of Apache Tomcat, it is sufficient to copy the file to the webapps folder of the Tomcat installation.

Welive Player Client
--------------------
WeLivePlayer client is a hybrid mobile application based on ionic framework, Apache Cordova, and Node.js. The source code can be found inside to the 'welive-player' project::

	welive.player\weliveplayer-mobile

Software requirements and software artefacts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following tables reports the software requirements and the software artefacts to be installed for the WeLivePlayer client.

+--------------------------+--------------------------------------+------------+
| WeLive tool              | Software requirement                 | Version    |
+==========================+======================================+============+
| WeLivePlayer CLient      |Ionic Framework                       | 3.7.0      |
+--------------------------+--------------------------------------+------------+
|                          |Ionic CLI                             | 2.0.0      |
+--------------------------+--------------------------------------+------------+
|                          |Ionic App Lib                         | 0.6.5      |
+--------------------------+--------------------------------------+------------+
|                          |Apache Cordova CLI                    | 7.0.1      |
+--------------------------+--------------------------------------+------------+
|                          |Node.js                               | 6.11.0     |
+--------------------------+--------------------------------------+------------+

Table 3 – Software requirements of WeLive Player client

+--------------------------+-----------------------------------+---------------+
| WeLive tool              | Software artefact                 | Artefact type |
+==========================+===================================+===============+
| WeLive Player client     | Weliveplayer.apk                  | Android       |
|                          |                                   | application   |
+--------------------------+-----------------------------------+---------------+

Table 4 – Software artefact of WeLive Player

Installation guide
^^^^^^^^^^^^^^^^^^

The application can be installed on Android and iOS platform using respective package installer.

Basic configuration
"""""""""""""""""""

Go to ‘/www/js/services’ and open file ‘ConfigSrv.js. You will find the following list of properties:

+----------------------+-----------------------+--------------------------------------+
| Tool                 | Name                  | Value                                |
+======================+=======================+======================================+
| AAC                  | AAC_SERVER_URL        | https://dev.welive.eu/aac            |
+----------------------+-----------------------+--------------------------------------+
|                      | CLIENT_ID             | WeLive AAC Client ID                 |
+----------------------+-----------------------+--------------------------------------+
|                      | CLIENT_SECRET_KEY     | WeLive AAC client secret (mobile)    |
+----------------------+-----------------------+--------------------------------------+
| WeLivePlayer Server  | WELIVE_PROXY          | WeLive Player REST API endpoint      |
+----------------------+-----------------------+--------------------------------------+
| LOG                  | LOG_URI               | URL of the logging BB endpoint       |
+----------------------+-----------------------+--------------------------------------+
| WeLivePlayer Client  | PILTO_IDS             | Pilot ids                            |
+----------------------+-----------------------+--------------------------------------+
|                      | APP_TYPE              | The artefact type managed by the     |
|                      |                       | player (PSA)                         |
+----------------------+-----------------------+--------------------------------------+

Build and execute
"""""""""""""""""
Create artifact package(apk) by executing the following command from the root directory(weliveplayer-mobile)::

	> npm install
	> ionic cordova build android

Move the apk file from  ../weliveplayer-mobile/platforms/<android|ios>/build/outputs folder to your device and install using package installer.
