WeLive REST API
===============

Requirements
-------------

The WeLive REST API has the following requirements that must be correctly installed
and configured. Follow the instructions provided for your platform to install
them before proceeding.

*	`Java SE Development Kit 7 <http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html>`_
* 	`Maven 3 <http://maven.apache.org/>`_
*   `CKAN <http://ckan.org/>`_
*   `OpenLink Virtuoso <http://virtuoso.openlinksw.com>`_

Dependencies
------------

The following dependencies are required to to build the WeLive REST API architecture:

Download and install the `Amaca common-lib` ::

    git clone https://github.com/sciamlab/common-lib.git
    cd common-lib
    mvn install

Download an install the `ckan4j` java library ::

    git clone https://github.com/sciamlab/ckan4j.git
    cd ckan4j
    mvn install

Install the OpenLink Virtuoso Jena Provider and Virtuoso JDBC 4 Driver JAR file
required to communicate with the Virtuoso server. The latest library files can
be downloaded from
`here <http://virtuoso.openlinksw.com/dataspace/doc/dav/wiki/Main/VOSDownload#Jena%20Provider>`_.

After downloading the libraries install them using the following Maven
commands ::

    mvn install:install-file -Dfile=virt_jena2.jar -DgroupId=virtuoso.jena.driver \
    -DartifactId=virtuoso-jena-driver -Dversion=2 -Dpackaging=jar

    mvn install:install-file -Dfile=virtjdbc4.jar -DgroupId=virtuoso \
    -DartifactId=virtuoso -Dversion=4 -Dpackaging=jar

Build server
------------
First, install the required query-mapper library distributed with the source
code by executing the following instruction inside the ``rest-api`` directory.::

  mvn validate

The following command will download all required dependencies, execute all automatic tests
and generate a deployable war file called ``welive-api.war`` in the ``rest-interface/target``
directory. The command must be executed inside the project's main directory. ::

	mvn install

.. note::

	If there are some failing tests the build process will stop and not generate the deployable
	war file. In order to build the code ignoring failing tests run the following command. ::

		mvn install -Dmaven.test.failure.ignore=true

If the build process finishes correctly, a deployable ``welive-api.war`` file can be found
inside the ``rest-api/target`` directory.

.. warning::

	**Not recommended.** In order to skip tests add the following parameter to the build
	command ::

		mvn install -DskipTests

Testing the server
------------------

The server can be tested using an embedded Jetty server that automatically starts and
deploys the previously generated war. Inside the ``rest-interface`` directory run ::

	mvn jetty:run

After launching the embedded server, the web methods are described and can be
tested by connecting to the following URL

	http://127.0.0.1:8080/welive/swagger/index.html

.. _deploying_war:

Deploying war
-------------

The WeLive REST API server can be deployed in a servlet container such as
`Apache Tomcat <http:://apache.tomcat.org>`_. If the server was correctly built, the
``war`` file is located in the ``rest-interface/target`` directory.

In the case of Apache Tomcat, copy the ``welive.war`` file to the ``webapps``
directory.

.. note::

	Create the ``data`` directory inside the Tomcat directory (e.g /var/lib/tomcat7)
	and add permissions for Tomcat on that directory.


Configuring WeLive REST API
---------------------------

The configuration of the WeLive REST API is done in the ``welive.properties`` file.
The following code block shows the configurable properties.

.. code-block:: jproperties

    #ODS configuration
    apiurl=https://dev.welive.eu/dev/api

    #Swagger configuration
    swaggerurl=dev/api
    swaggerappclientid=SWAGGER_CLIENT_ID

    #Query mapper configuration
    datadir=QM_DATA_DIR
    schedulerthreadcount=50

    #CKAN instance configuration
    ckanurl=https://dev.welive.eu/ods

    #Components configuration
    loggingserviceurl=https://dev.welive.eu/welive.logging/log
    aacserviceurl=https://dev.welive.eu/aac
    decisionengineurl=http://localhost:8002
    udeustodeurl=http://localhost:8000
    wlplayerurl=https://dev.welive.eu/weliveplayer

    #Liferay configuration
    liferayURL=https://dev.welive.eu/api/jsonws
    mkppath=/MarketplaceStore-portlet.mkp
    lumpath=/LiferayUserManager-portlet.apilum
    oiapath=/Challenge62-portlet.clsidea
    cdvpath=/CitizenDataVault-portlet.cdv
    vcURL=https://dev.welive.eu/visualcomposer/webservice

    #ADS COnfiguration
    URL_WELIVE_LOGIN=welive.logging
    BASE_URL=https://dev.welive.eu
    KPI1_1=/dev/api/cdv/getKPI_1.1/
    KPI4_3=/dev/api/cdv/getKPI_4.3/
    KPI11_1=/dev/api/cdv/getKPI_11.1/
    KPI11_2=/dev/api/cdv/getKPI_11.2/
    WELIVE_LOGGIN_QUERY=/welive.logging/log/aggregate
    WELIVE_LOG_INSERT=/welive.logging/log

    #Internal WeLive admin user
    adminuser=WELIVE_BASIC_AUTH_USER
    adminpass=WELIVE_BASIC_AUTH_PASSWORD

    #OPTIONAL: Graphite configuration
    graphiteHost=test.welive.eu
    graphitePort=2003
    graphitePrefix=dev.welive.eu


apiurl
    Complete URL of the deployed REST API. Used by the ODS for callbacks.

swaggerurl
    Relative path required by Swagger to generate the complete URL for method
    annotation by concatenating it to the base path.

datadir
    Directory used by the Query mapper to store the generated information. The
    directory must exist before launching the WeLive REST API.

schedulerthreadcount
    Number of threads launched by the internal scheduler of the Query Mapper to
    process and convert the connected data sources. It defaults to 50
    simultaneous threads.

ckanurl
    URL of the CKAN API that contains the dataset information. The values for
    the *ckanendpoint* and *ckansearchapi* properties are optional, as they are
    automatically obtained from the *ckanurl* property value
    (http://127.0.0.1:5000/api/3 and http://127.0.0.1:5000/api/search/
    respectively).

aacserviceurl
    URL pointing to the WeLive Authentication and Authorization service.

loggingserviceurl
    URL pointing to the WeLive Logging service.

decisionengineurl
    URL pointing to the WeLive Decision Engine service.

udeustodeurl
    URL pointing to the UDEUSTO WeLive Decision Engine service.

wlplayerurl
    URL pointing to the WeLive player service.

adminuser
    Internal administrator user and pass required to by the REST API to
    communicate with other WeLive components.

liferayURL
    URL pointing to the Liferay service.

vcURL
    URL pointing to the Visual Composer service.

adminuser
    Basic Auth user for connecting to other WeLive platform's components.

adminpass
    Basic Auth password for connecting to other WeLive platform's components.

graphiteHost
    Host of the `Graphite <https://graphiteapp.org>`_. server for monitoring API's stats.

graphitePort
    Port of the `Graphite <https://graphiteapp.org>`_. server for monitoring API's stats.

graphitePrefix
    Prefix of the `Graphite <https://graphiteapp.org>`_. server for monitoring API's stats.

After the WeLive API is deployed the Swagger description should be accessible at

	http://server/welive/swagger

or any other port depending of the Tomcat configuration.
