Service Composer (Visual Composer)
==========================================

Requirements
-------------

Software
^^^^^^^^
The Visual Composer has the following requirements that must be correctly installed and configured. Follow the instructions provided for your platform to install them before proceeding.

*	`Java SE Development Kit 7 <http://docs.oracle.com/javase/7/docs/webnotes/install/>`_
* `Apache Tomcat 7 <https://tomcat.apache.org/tomcat-7.0-doc/setup.html>`_
* `PostgreSQL 9.1 <https://wiki.postgresql.org/wiki/Detailed_installation_guides>`_

Artefacts
^^^^^^^^^
These are the artefacts that must be installed in order to run the Visual Composer:

* visualcomposer.war
* emml.war
* z-folder.rar
* insertStartupRows.sql

Database creation
-----------------
The Visual Composer relies on a PostgreSQL database to store all the application and user data. So before deploying the application, it is necessary to create a new database and annotate its name and owner (name and password). To create a new database, you can follow the `official documentation <http://www.postgresql.org/docs/9.1/static/manage-ag-createdb.html>`_.

Projects repository creation
----------------------------
The Visual Composer relies on several files (Javascript libraries, configuration files, operators etc) that must be stored in the file system; furthermore, the VC writes the mashup and mockup projects on the file system as text files.

So, create a new folder in your home directory and extract here the content of **z-folder.rar** archive. The following tree of folders will be created:

.. code-block:: none

	+ vme-editor
	+ --- emmlscripts
	+ --- extresources
	+ --- guieditor
	+ --- mashupeditor

where:

* *emmlscripts* contains the mashup projects repository;
* *extresources* contains the OpenSocial gadgets repository;
* *guieditor* contains the mockup projects repository and the mockup palette components;
* *mashupeditor* contains the mashup operators.

Configuration
-------------
In order to make the Visual Composer deploying and running properly, it is necessary to open the **visualcomposer.war** and modify some configuration files.

Database
^^^^^^^^
Go to **WEB-INF/spring** and open the file **root-context.xml**. Then modify the following bean:

.. code-block:: xml

	<bean id="mainDataSource" class="com.jolbox.bonecp.BoneCPDataSource" destroy-method="close">
		<property name="driverClass" value="org.postgresql.Driver" />
		<property name="jdbcUrl" value="jdbc:postgresql://localhost:5432/DB_NAME" />
		<property name="username" value="DB_OWNER_USERNAME" />
		<property name="password" value="DB_OWNER_PASSWORD" />
		<property name="idleConnectionTestPeriodInMinutes" value="30" />
		<property name="idleMaxAgeInMinutes" value="10" />
		<property name="maxConnectionsPerPartition" value="10" />
		<property name="minConnectionsPerPartition" value="2" />
		<property name="partitionCount" value="3" />
		<property name="acquireIncrement" value="2" />
		<property name="statementsCacheSize" value="20" />
	</bean>

replacing ``DB_NAME``, ``DB_OWNER_USERNAME`` and ``DB_OWNER_PASSWORD`` with the proper values.

Properties
^^^^^^^^^^
Go to **WEB-INF/spring** and open the file **config.properties**. You will find the following list of properties:

.. code-block:: jproperties

	# BASIC VC CONF
	app.baseDir=Z_FOLDER_LOCATION

	app.publicUrl=VC_HOST_URL
	app.url.name=visualcomposer

	app.extResourcesPath=${app.baseDir}/vme-editor/extresources
	app.guiEditorResources=${app.baseDir}/vme-editor/guieditor
	app.mashupEditorResources=${app.baseDir}/vme-editor/mashupeditor

	emml.engineContext=EMML_BASE_URL
	emml.scriptFolder=${app.baseDir}/vme-editor/emmlscripts

	gadget.endpointBaseUrl=${app.publicUrl}/${app.url.name}/extresources

	marketplace.image_repository=${app.baseDir}/marketplace/images
	marketplace.mashup_repository=${app.baseDir}/marketplace/mashup_project

	oauth.callbackContext=${app.publicUrl}/${app.url.name}

	# AAC
	aac.basicUrl=AAC_BASE_URL

	# CAS
	cas.server.url=${aac.basicUrl}/cas
	cas.client.url=${app.publicUrl}/${app.url.name}

	# MARKETPLACE
	usdleditor.portlet=usdleditor_WAR_usdleditorportlet
	mkp.baseUrl=LIFERAY_BASE_URL
	mkp.apiUrl=MKP_APIS_BASE_URL
	mkp.searchArtefacts.serviceUrl=${mkp.apiUrl}/get-artefacts-by-keywords/keywords/!KEYWORDS!/artefact-types/vc-all/pilot/!PILOT_ID!
	mkp.allArtefacts.serviceUrl=${mkp.apiUrl}/get-all-artefacts/pilot-id/!PILOT_ID!/artefact-types/vc-all
	mkp.getSuggestedArtefacts.serviceUrl=${mkp.apiUrl}/get-artefacts-by-ids/ids/!ID_LIST!
	mkp.canPublish.serviceUrl=${mkp.apiUrl}/can-publish
	mkp.artefact.pageUrl=${mkp.baseUrl}/marketplace/-/marketplace/view/
	mkp.gadgetPublishUrl=${mkp.baseUrl}/wizard-usdl?p_p_id=usdleditor_WAR_usdleditorportlet&p_p_mode=view&_usdleditor_WAR_usdleditorportlet_redirect=%2Fweb%2Fguest%2Fwizard-usdl&_usdleditor_WAR_usdleditorportlet_mvcPath=%2Fview.jsp
	mkp.serviceTypeId.REST=4
	mkp.serviceTypeId.SOAP=201
	mkp.serviceTypeId.ODATA=101

	# OPEN INNOVATION AREA
	idea.baseUrl=LIFERAY_BASE_URL
	idea.apiUrl=OIA_APIS_BASE_URL
	idea.publishScreenshot.serviceUrl=${idea.apiUrl}/publish-screen-shot
	idea.newProject.serviceUrl=${idea.apiUrl}/vc-project-update
	idea.ideaPage=${idea.baseUrl}/innovation-area/-/ideas_explorer_contest/!IDEA_ID!/view
	idea.screenshotPage=${idea.baseUrl}/innovation-area/-/ideas_explorer_contest/!IDEA_ID!/screenshots

	# DECISION ENGINE
	decisionEngine.getSuggestedArtefacts.serviceUrl=DE_SUGGESTIONS_SERVICE_URL

	# OPEN DATA STACK
	ods.apiUrl=ODS_APIS_BASE_URL
	ods.getDatasetById.serviceUrl=${ods.apiUrl}/ods/dataset/!DATASET_ID!
	ods.getDatasetSchema.serviceUrl=${ods.apiUrl}/dataset/!DATASET_ID!/resource/!RESOURCE_ID!/mapping/info
	ods.queryResource.serviceUrl=${ods.apiUrl}/dataset/!DATASET_ID!/resource/!RESOURCE_ID!/query

	# LOGGING BB
	logbb.notify.serviceUrl=LOGGINGBB_NOTIFICATION_SERVICE_URL

	#WeLive Tools Basic Auth
	basic.auth.enabled=true
	welive.tools.username=WELIVE_TOOLS_USERNAME
	welive.tools.password=WELIVE_TOOLS_PASSWORD

Tomcat
^^^^^^
The ``emml.scriptFolder`` is a special folder shared between the Visual Composer and the EMML engine and so it has to be added to the Tomcat classpath.

To do that, open the file **TOMCAT_HOME/bin/catalina.sh** and locate the following line:

.. code-block:: bash

	CLASSPATH="$CLASSPATH""$CATALINA_HOME"/bin/bootstrap.jar

Then add the following line:

.. code-block:: sh

	CLASSPATH="$CLASSPATH":emml.scriptFolder

replacing ``emml.scriptFolder`` with the value defined in the previous section.

Others
^^^^^^
Open the z-folder and browse the file system to **vme-editor/guieditor/project_template/js**. Open the file **main.js** and modify the following section:

.. code-block:: javascript

	baseUrl: 'HOSTNAME/visualcomposer/guieditor/project_template/js/lib',
	paths: {
	       app: ' HOSTNAME/visualcomposer/guieditor/palette_component/app',
	       customHandlers: 'HOSTNAME/visualcomposer/guieditor/palette_component/customBindingHandlers',
		jquery : '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min',
		jqueryui: '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui',
		bootstrap: '//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min',
		knockout: '//cdnjs.cloudflare.com/ajax/libs/knockout/3.1.0/knockout-min',
		applib: 'HOSTNAME/visualcomposer/guieditor/palette_component/libs',
		pager: 'HOSTNAME/visualcomposer/guieditor/project_template/js/lib/pager.min'
	}

replacing ``HOSTNAME`` with the proper value.

Then open the file **screenshot.js** and modify the following section:

.. code-block:: javascript

	$("[data-vme-itemid='vme-gmap']").after('<img class="gmap_fake" src="HOSTNAME/visualcomposer/guieditor/palette_component/gmap.jpg" height="'+height+'" width="'+width+'" style="margin:'+margin+';padding:'+padding+';display:none" />');

replacing ``HOSTNAME`` with the proper value.

WARs deployment
---------------
Now that the Visual Composer is properly configured, move **visualcomposer.rar** and **emml.rar** to the webapps folder of the Tomcat and wait until they are deployed.

Database Startup
----------------
In order to allow that the WeLive Administrator can access the Visual Composer, it is necessary to add it to the database (the Administrator user cannot be propagated to the VC). So open the **insertStartupRows.sql** and edit the following:

.. code-block:: sql

	INSERT INTO profile (id, created_at, email, first_name, last_name, updated_at) VALUES (1, '2013-07-05 12:29:26.77', 'ADMIN_EMAIL', 'ADMIN_FIRSTNAME', 'ADMIN_LASTNAME', '2013-07-05 12:29:26.77');

	INSERT INTO account (id, created_at, email, expire, provider, psw, role, status, token, updated_at, validated_oauth_id, profile_id) VALUES (1, '2013-07-05 12:29:26.77', 'ADMIN_EMAIL', NULL, 'vme', '21232f297a57a5a743894a0e4a801fc3', 1, 2, NULL, '2014-11-20 12:04:09.483', NULL, 1);

replacing ``ADMIN_EMAIL``, ``ADMIN_FIRSTNAME`` and ``ADMIN_LASTNAME`` with the proper values. Then execute the whole script on the database created and the startup entries will be created.
