Authentication and Authorization
================================

Software requirements and software artefacts
--------------------------------------------

The Authentication and Authorization building block (AAC) is implemented as J2EE Web application. Its deployment, therefore, relies on the standard J2EE Web application container. The data managed by AAC is persisted using the relational database; the object-relational mapping exploited by the implementation allows for abstracting from the concrete database implementation. For the demonstration purposes the MySQL database server is used .
The following tables reports the software requirements and the software artefacts to be installed for AAC.

+--------------------------------------+---------------------------------------+---------+
| WeLive tool                          | Software requirement                  | Version |
+======================================+=======================================+=========+
| AAC                                  | Oracle Java                           | 7       |
|                                      | MySQL                                 | 5.7     |
+--------------------------------------+---------------------------------------+---------+

Table 1 – Software requirements of AAC

+------------------------------+-----------------------------------+---------------------+
| WeLive tool                  | Software artefact                 | Artefact type       |
+==============================+===================================+=====================+
| AAC                          | aac.war                           | war                 |
+------------------------------+-----------------------------------+---------------------+

Table 2 - Software artefact of AAC

Installation guide
------------------

Git clone the ‘aac’ project source from::

	https://bitbucket.org/welive/aac
	
Database creation
^^^^^^^^^^^^^^^^^

AAC relies on a relational database to store all the application and user data. So before deploying the application, it is necessary to create a new database schema and associate it to a user with sufficient privileges (e.g., for table creation and management for the schema). The tables and the data bootstrapping will be performed by the application upon startup.

AAC configuration
^^^^^^^^^^^^^^^^^

Application configuration
^^^^^^^^^^^^^^^^^^^^^^^^^

The relevant properties of the AAC instance are configured through the application properties at src/main/resources/commoncore.properties

+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
| Tool                     | Name                                   | Value                                                                                               |
+==========================+========================================+=====================================================================================================+
| AAC                      | jdbc.dialect                           | Database used (e.g., org.hibernate.dialect.MySQLDialect)                                            |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | jdbc.driver                            | Database driver (e.g., com.mysql.jdbc.Driver)                                                       |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | jdbc.url                               | Database url ( e.g., jdbc:mysql://localhost:3306/acprovider)                                        |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | jdbc.user                              | Database user                                                                                       |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | jdbc. Password                         | Database user password                                                                              |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | mode.restricted                        | Whether the access to the AAC is available only to restricted users (true) or to everybody (false)  |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | mode.collectInfo                       | Whether extended profile data should be collected from user                                         |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | application.url                        | Base url of the AAC as exposed externally (e.g., https://dev.welive.eu/dev/api/aac)                 |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | ac.admin.file                          | File with the list of admin accounts and authorized accounts (in case of restricted access)         |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | api.token                              | Basic Auth token for accessing the user creation API                                                |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | default.redirect.url                   | URL where to redirect upon various logout methods by default                                        |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | support.email                          | Support email to be visualized on web pages                                                         |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | swagger.server                         | Endpoint of Swagger API interface                                                                   |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | welive.lum                             | Endpoint of Liferay user manager 'add-new-user' API                                                 |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | liferay.service.to.avoid               | Endpoint to be avoided for the fix for liferay logout solution                                      |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | liferay.service.to.logout              | Endpoint to be used to perform liferay logout solution                                              |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | welive.cas.server                      | URL of the CAS server for WeLive internal identity provider                                         |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | google.clientId                        | Google API client ID for Google+ social authentication.                                             |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | google.clientSecret                    | Google API client secret for Google+ social authentication.                                         |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | google.callbackURI                     | Google API callback for Google+ social authentication.                                              |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | fb.clientId                            | Facebook API client id for Facebook social authentication.                                          |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | fb.clientSecret                        | Facebook API client secret for Facebook social authentication.                                      |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
|                          | fb.callbackURI                         | Facebook API client secret for Facebook social authentication.                                      |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+
| Logging                  | logging.endpoint                       | Endpoint of logging API                                                                             |
+--------------------------+----------------------------------------+-----------------------------------------------------------------------------------------------------+

Identity Provider configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

AAC handles different Identity Providers for the authentication purposes and potentially using different protocols. The integration of the Identity Providers may be done in two ways: internally (i.e., extending AAC with the necessary libraries and code that handles the protocol-specific interaction) or externally (i.e., using the Web server capabilities and extensions that serve as proxies to the protected address).
Regardless specific implementation, at the level of the specification configuration each identity provider is represented with the Identity Provider model that is declared in authorities.xml. The model defines the following attributes (authorityMapping section):
name (string attribute): unique name of the IdP within AAC
url (string attribute): the url path variable representing the IdP in AAC
public (Boolean attribute, default false): whether the IdP may be engaged by any client app or only by the authorized once (managed by AAC admin).
useParams (Boolean atribute, default false): whether the user information arrive to the protected authentication endpoint as parameters or attributes.
attributes (element): list of possible attribute names that the IdP exposes. It is possible to map some of the attributes on user given name or surname using the alias attribute.
identityAttributes (element): list of attributes with unique values across the IdP. At least one identity attribute should be defined.

Example for WeLive IdP::


	<authorityMapping name="welive" url="welive" public="true">
	<attributes alias="eu.trentorise.smartcampus.givenname">name</attributes>
	<attributes alias="eu.trentorise.smartcampus.surname">surname</attributes>
	<attributes>email</attributes>
	<identifyingAttributes>username</identifyingAttributes>
	</authorityMapping>

In case different IdP share common attributes (e.g., identifying phone number or email), it is possible to relate different Identity providers so that no distinct users are created (authorityMatching section). Each authority in the matching group is represented with:
name (string attribute): the name of the IdP as declared in authorityMapping
attribute (string attribute): the identity attribute as declared in authorityMapping

Example::

	<authorityMatching>
	<authority name="google" attribute="OIDC_CLAIM_email"/>
	<authority name="welive" attribute="username"/>
	<authority name="facebook" attribute="email"/>
	</authorityMatching>

Admin configuration
^^^^^^^^^^^^^^^^^^^

Some of the operations of AAC may be performed only by the AAC admin. This includes, in particular, the authorization of restricted identity providers, access requests to the permission scopes that require explicit authorization. The list of administrator accounts is stored in admin.txt (see the properties of the AAC configuration). The declaration of the account is as follows::

	<IdP name>;<identity attribute>;<identity attribute value>; <role (‘admin’ in case of administrator)>

Packaging and deployment
^^^^^^^^^^^^^^^^^^^^^^^^

To create an application archive (war file), the AAC implementation relies on Apache Maven packaging system. That is the following command should be executed from the root of the AAC source folder::

	> mvn clean package

The resulting aac.war file can be found at target folder and should be deployed in the J2EE Web application container. In case of Apache Tomcat, it is sufficient to copy the file to the webapps folder of the Tomcat installation.
