User Account (Citizen Data Vault)
==================

Requirements
-------------

Software
^^^^^^^^
Citizen Data Vault (CDV) is implemented as a `Liferay portlet <https://www.liferay.com/>`_ and requires the following softwares to be correctly installed and configured. Follow the instructions provided for your platform to install them before proceeding.

*	`Liferay Portal Community Edition - Bundled with Tomcat 6.2 - GA4  <https://www.liferay.com/>`_
* `PostgreSQL 8.1 <https://wiki.postgresql.org/wiki/Detailed_installation_guides>`_
* `Oracle Java 7 <https://www.oracle.com/it/java/index.html>`_

Artefacts
^^^^^^^^^
These are the artefacts that must be installed in order to run the CDV:

* *CitizenDataVault-portlet-6.2.0.1.war*

Installation
------------

Liferay Setup
^^^^^^^^^^^^^^^^^^^^^^
Once Java and PostgreSQL are installed, `install Liferay <https://www.liferay.com/it/documentation/liferay-portal/6.2/user-guide/-/ai/installation-and-setup-liferay-portal-6-2-user-guide-15-en>`_.

Bundled Tomcat configuration and startup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In order to increase the duration of the working sessions, modify the file **web.xml**, in **tomcat/conf**, inside the bundled Liferay.

.. code-block:: xml

	<!-- =========== Default Session Configuration ================= -->
	<!-- You can set the default session timeout (in minutes) for all newly   -->
	<!-- created sessions by modifying the value below.                       -->

	<session-config>
	    <session-timeout>30</session-timeout>
	</session-config>

increasing ``session-timeout`` to 90.

In order to increase the allocation of RAM memory, modify the file **setenv.sh**, in **tomcat/bin**. The file should contain the following lines:

.. code-block:: sh

	CATALINA_OPTS="$CATALINA_OPTS -Dfile.encoding=UTF8 -Djava.net.preferIPv4Stack=true  -Dorg.apache.catalina.loader.WebappClassLoader.ENABLE_CLEAR_REFERENCES=false -Duser.timezone=Europe/Rome -Xms2048m -Xmx2048m -XX:PermSize=512m

At this point, `startup the bundled Tomcat <https://www.liferay.com/it/documentation/liferay-portal/6.2/user-guide/-/ai/installing-a-bundle-liferay-portal-6-2-user-guide-15-en>`_.

Basic configuration
^^^^^^^^^^^^^^^^^^^
Now, make the first access to the portal, accessing the address and the port where the Tomcat is listening, i.e. http://localhost:8080/. In the panel *Basic configuration* set:

* Section *Portal*

	* **Portal name**: WeLive

* Section *Administrator User*

	* **First name**: WeLive
	* **Last name**: Administrator

* Section *Database*

	* Select *Change*
	* **Database Type**: PostgreSQL
	* **JDBC URL**: jdbc:postgresql://localhost:5432/dbname, where dbname  is the database name chosen in the phase of Postgres installation, i.e. welive
	* **Username**: the username chosen in the phase of PostgreSQL installation, i.e. postgres
	* **Password**: the password chosen in the phase of PostgreSQL installation, i.e. postgres

Follow the wizard to choose the administrator password and complete the installation.

Portal Settings
^^^^^^^^^^^^^^^
Once finished the installation procedure, login as administrator (user WeLive Administrator) and go to *Control Panel > Portal Setting*. Then:

* In *General panel* set:

	* **Mail Domain**: mail domain chosen, i.e. welive.eu
	* **Virtual Host**: address domain chosen, i.e. welive.eu

* In *Users* panel add to *Roles* field:

	* **Citizen**
	* **Social Office User**

* In *Email Notifications* panel set:

	* **Name**: WeLive Administrator
	* **Address**: the mail address chosen, i.e. admin@welive.eu

* In *Display Settings* panel set as *Current Available Languages*:

	* English (United Kingdom)
	* Finnish (Finland)
	* Serbian (Serbia)
	* Serbian (Serbia, latin)
	* Spanish (Spain)

Mail Configuration
^^^^^^^^^^^^^^^^^^
Go to *Server Administration > Mail* and set:

* **Outgoing SMTP Server**: the IP or address of SMTP service chosen
* **Outgoing Port**: the port of SMTP service chosen
* Check **Use a Secure Network Connection** if necessary
* **User Name**, **Password** if necessary

Portal-ext properties and restart
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The file named **portal-ext.properties** must be placed in the same directory where is located the self-generated file **portal-setup-wizard.properties**, that is at first level of Liferay directory installation.

Restart the Tomcat server.

Database Creation
^^^^^^^^^^^^^^^^^
Citizen Data Vault relies on a PostgreSQL database to store all the application and user data. So before deploying the application, it is necessary to create a new database and annotate its name and owner (username and password). To create a new database, you can follow the `official documentation <http://www.postgresql.org/docs/9.1/static/manage-ag-createdb.html>`_.

Deployment
^^^^^^^^^^^^^^^
After that, install the WAR archive called *CitizenDataVault-portlet-6.2.0.1.war* to complete the procedure.

The CDV functionalities will be provided through the *My Account* section of the platform. 

