Open Data Stack
===============

WeLive's Open Data Stack is based on the wide extended data management system CKAN. For this reason, the first step for deploying the ODS is installing CKAN 2.4.1 following its `documentation <http://docs.ckan.org/en/ckan-2.4.1/maintaining/installing/index.html>`_.

After installing CKAN, the WeLive project requires to install different extensions on the basic CKAN installation.

.. note::
    For properly installing these extensions, remember to activate the CKAN's virtualenv::

      . /usr/lib/ckan/default/bin/activate.

.. note::
    We recommend installing the extensions following the order shown by this documentation.

SPARQL integration
------------------

This extension allows integrating a SPARQL repository into CKAN. More information about this extension can be found at :doc:`../../user-guide/ods/dataset-query`.

.. code::

    git clone https://bitbucket.org/welive/ckanext-sparql
    cd ckanext-sparql
    python setup.py develop

Edit \*.ini configuration file of CKAN, and include the following section:

.. code::

    ckan.plugins = [...] sparql

    [plugin:sparql]
    welive_api = https://dev.welive.eu/dev/api/


Welive Query Mapper
-------------------

This extension integrates the Open Data Stack with the :ref:`query_mapper`.

.. code::

    git clone https://bitbucket.org/welive/ckanext-welive_qm
    cd ckanext-welive_qm
    python setup.py develop

Edit \*.ini configuration file of CKAN, and include the following section:

.. code::

    ckan.plugins = [...] welive


Welive utils
------------

This extension provides common functionalities for integrating the ODS into the WeLive Platform. This extension is mandatory for many of the other extensions.

.. code::

    git clone https://bitbucket.org/welive/ckanext-welive_utils
    cd ckanext-welive_utils/
    python setup.py develop

Edit \*.ini configuration file of CKAN, and include the following section:

.. code::

    ckan.plugins = [...] welive_utils

    [plugin:welive_utils]
    welive_api = https://dev.welive.eu/dev/api
    basic_user = WELIVE_BASIC_AUTH_USER
    basic_password = WELIVE_BASIC_AUTH_PASSWORD
    pilot_dict =  {"bilbao-city-council": "Bilbao", "novi-sad": "Novisad", "helsinki-uusimaa": "Uusimaa", "trento": "Trento"}


    [plugin:logging]
    LOGGING_URL = https://dev.welive.eu/dev/api/log
    APP_ID= ODS


Authentication and authorization extension
------------------------------------------

This extension allows authenticating users against Welive's Authentication and Authorization Core Building Block.

.. code::

    git clone https://bitbucket.org/welive/ckanext-welive_authentication
    cd ckanext-welive_authentication
    pip install -r requirements.txt
    python setup.py develop

Edit \*.ini configuration file of CKAN, and include welive_authentication in ckan.plugins, and the following section:

.. code::

    ckan.plugins = [...] welive_authentication

    [plugin:authentication]
    aac_url = https://dev.welive.eu/aac
    aac_api_url = https://dev.welive.eu/dev/api/aac
    client_id = APP_CLIENT_ID
    client_secret = APP_CLIENT_SECRET
    client_token = APP_CLIENT_TOKEN
    admin_cc_user_id = CKAN_ADMIN_CC_USER_ID
    refresh_token = CKAN_ADMIN_REFRESH_TOKEN
    welive_url = https://dev.welive.eu


CKAN harvester
--------------

This extension is a fork of the `original CKAN harvester extension <https://github.com/ckan/ckanext-harvest>`_, so it can be installed following the original instructions. The modified extension is located at: ``https://bitbucket.org/welive/ckanext-harvest``. More information about the harvesting process can be found at :doc:`../user-guide/ods/harvesting`.

.. code::

    git clone https://bitbucket.org/welive/ckanext-harvest

And follow the original install instructions.

DCAT harvester
--------------

Same as the CKAN harvester, this harvester is a fork of the `original DCAT harvester extension <https://github.com/ckan/ckanext-dcat>`_.

.. code::

    git clone https://bitbucket.org/welive/ckanext-dcat

And follow the original install instructions.

Dataset rating extension
------------------------

This extension allows users rating datasets.

.. code::

    git clone https://bitbucket.org/welive/ckanext-rating
    cd ckanext-rating
    python setup.py develop

Edit \*.ini configuration file of CKAN, and include the following section:

.. code::

    ckan.plugins = [...] rating

CKAN git extension
------------------

This extension allows proposing modifications on resource files. More info at :doc:`../user-guide/ods/change-proposal`.

.. code::

    git clone https://bitbucket.org/welive/ckanext-git
    cd ckanext-git
    pip install -r requirements.txt
    python setup.py develop

Edit \*.ini configuration file of CKAN, and include the following sections:

.. code::

    ckan.plugins = [...] git

    [plugin:git]
    repo_dir = <path to repos>
    api_key = <CKAN's API key>

    [redis]
    redis.host = localhost
    redis.port = 6379
    redis.db=1


In case of deploying CKAN using uWSGI, you must declare the location of git in the \*.ini configuration file:

.. code::

    [uwsgi]
    ...
    env=GIT_PYTHON_GIT_EXECUTABLE=<git path>
    ...

Resource validation extension
-----------------------------

This extension validates CSV, XML, JSON and RDf files against their schema. More info at :doc:`../user-guide/ods/validation`.

.. code::

    git clone https://bitbucket.org/welive/ckanext-validation
    cd ckanext-validation
    pip install -r requirements.txt
    python setup.py develop

Edit \*.ini configuration file of CKAN, and include the following sections:

.. code::

    ckan.plugins = [...] validation

    [plugin:validation]
    welive_api = https://dev.welive.eu/dev/api/

    [redis]
    redis.host = localhost
    redis.port = 6379
    redis.db=1


WeLive Theme
------------

The Welive Theme for CKAN has been developed as another extension.

.. code::

    git clone https://bitbucket.org/welive/ckanext-welive_theme
    cd ckanext-welive_theme
    python setup.py develop

Edit \*.ini configuration file of CKAN, and include the following sections:

.. code::

    ckan.plugins = [...] welive_theme


Web Scrapper
------------

This extension allows scraping webs using XPath expressions or regular expressions and scraping tweets from Twitter API. More info at :doc:`../user-guide/ods/harvesting`.

.. code::

    git clone https://bitbucket.org/welive/ckanext-web_scraper
    cd ckanext-web_scraper
    python setup.py develop

Edit \*.ini configuration file of CKAN, and include the following sections:

.. code::

    ckan.plugins = [...] xpath_scraper regex_scraper tweet_scraper
