Decision Engine
===============

WeLive Decision Engine generates recommendations about artefacts, ideas and idea
collaborators to be used in different WeLive components. The Decision Engine is
divided into two different software components:

* Decision Engine: it recommends collaborators for a given idea.
* UDEUSTO Decision Engine: it recommends, on one hand, related artefacts and on the other hand, apps for a given user based on its preferences and context.


Decision Engine installation
----------------------------

For installing the Decision Engine, the following steps must be completed:


.. code::

  git clone https://bitbucket.org/welive/welive-de
  pip install -r welive-de/requirements.txt
  python welive-de/recommender_engine.py

The Decision Engine will be available at http://localhost:5000.


UDEUSTO Decision Engine installation
------------------------------------

UDEUSTO Decision Engine could be easily deployed using docker-compose:

.. code::

  git clone https://bitbucket.org/welive/decision_engine
  cd decision_engine/docker
  docker-compose up

The UDEUSTO Decision Engine will be available at http://localhost:8000.
