Liferay User Manager
====================
Following methods allow the tools to interact with the Liferay User Manager, in order to create/retrieve information about users.

Add new user
------------
Add a new user on the system.

* **Endpoint**: /lum/add-new-user
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: application/json
* **Produces**: application/json

**Input schema**

.. code-block:: json

	{
	  "ccUserId": 0,
	  "pilot": "string",
	  "firstName": "string",
	  "surname": "string",
	  "email": "string",
	  "isMale": true,
	  "birthdayDay": 0,
	  "birthdayMonth": 0,
	  "birthdayYear": 0,
	  "developer": false,
	 
	  "address": "string",
	  "zipCode": "string",
	  "city": "string",
	  "country": "string",
	  "languages": [
	    "string"
	  ],
	  "role": "string",
	  "employment": "string",
	  "tags": [
	    "string"
	  ]

	}

where:

* ``ccUserId`` is the cross component userId created by AAC;
* ``pilot`` is the pilot referred to the user (**Uusima**, **Bilbao**, **Trento** or **Novisad**);
* ``firstName`` is the first name of the user;
* ``surname`` is the surname of the user;
* ``email`` is the email address of the user;
* ``isMale`` is true if the user is male, false otherwise;
* ``birthdayDay`` is the day when the user was born;
* ``birthdayMonth`` is the month when the user was born;
* ``birthdayYear`` is the year when the user was born;
* ``developer`` is true if the user has the Developer role, false otherwise;

* ``address`` is the address of the user;
* ``zipCode`` is the zip code of the user;
* ``city`` is the city of the user;
* ``country`` is the Country of the user;
* ``languages`` are the languages of which the user is confident;
* ``role`` is the role of the user (**Academy**, **Business**,  **Citizen**, **Entrepreneur**);
* ``employement`` is the work status of the user (**Student**, **Unemployed**, **Employedbythirdparty**, **Selfemployedentrepreneur**, **Retired**, **Other**);
* ``tags`` are some keyword about the user;

**Output schema**

Previous method returns a JSON with the following schema:

.. code-block:: json

  {
    "error": false,
    "message": "string"
  }


Get User roles
--------------
Get the roles associated with the user identified by ``ccuserid``.

* **Endpoint**: /lum/get-user-roles/{ccuserid}
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Input**

* ``ccuserid`` is the cross component userId created by AAC;

**Output schema**

Previous method returns a JSON with the following schema:

.. code-block:: json

	{
	  "isDeveloper": false,
	  "error": false,
	  "role": "Academy",
	  "message": "string"
	}

where:

* ``developer`` is **true** if the user has the Developer role, **false** otherwise;
* ``role`` is the role of the user (**Academy**, **Business**, **Citizen**, **Entrepreneur**).
