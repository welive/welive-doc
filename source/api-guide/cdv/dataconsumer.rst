Data consumer oriented
======================
Following methods allow the data consumers (Decision Engine, Visual Composer, Building Blocks etc) to retrieve information about WeLive User Profiles from CDV.

Get User Profile
----------------
This method returns the whole WeLive User Profile corresponding to the specified ``ccUserID`` (cross-components user ID).

* **Endpoint**: /cdv/getuserprofile
* **Method**: GET
* **Authorization Header**: Bearer
* **Produces**: application/json

**Output schema**

.. code-block:: json

   	{
		"ccUserID": 0,
		"referredPilot": "string",
		"name": "string",
		"surname": "string",
		"gender": "string",
		"birthdate": "string",
		"address": "string",
		"city": "string",
		"country": "string",
		"zipCode": "string",
		"email": "string",
		"isDeveloper": false,
		"languages": ["string"],
		"userTags": ["string"],
		"skills":
		[
		  {
		    "skillName": "string",
		    "endorsementCounter": 0
		  }
		],
		"usedApps":
		[
		  {
		    "appName": "string",
		    "appID": 0
		  }
		],
		"profileData":
		{
		  "numCreatedIdeas": 0,
		  "numCollaborationsInIdeas": 0,
		  "reputationScore": 0.0
		},
		"lastKnownLocation":
		{
		  "lat": "string",
		  "lng": "string"
		}
	}

Get User Metadata
-----------------
This method returns the metadata about the user corresponding to the specified ``ccUserID`` (cross-components user ID).

* **Endpoint**: /cdv/getusermetadata/{ccUserID}
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Output schema**

.. code-block:: json

  {
    "birthdate": "string",
    "address": "string",
    "city": "string",
    "userTags": ["string"],
    "skills":
    [
      {
        "skillName": "string",
        "endorsementCounter": 0
      }
    ],
    "usedApps":
    [
      {
        "appName": "string",
        "appID": 0
      }
    ],
    "lastKnownLocation":
    {
      "lat": "string",
      "lng": "string"
    }
  }

Get Users Skills
----------------
This method returns the list of users with their skills and known languages.

* **Endpoint**: /cdv/getusersskills
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Output schema**

.. code-block:: json

  [
    {
      "ccUserID": 0,
      "languages": ["string"],
      "skills":
      [
        {
          "skillName": "string",
          "endorsementCounter": 0
        }
      ]
    }
  ]

Error output
------------
If the ``ccUserID`` is invalid, *Get User Profile* and *Get User Metadata* return the following JSON:

.. code-block:: json

  {
    "response": 1,
    "message": 5,
    "text": "Error: No user with CC_UserID <ccUserID>"
  }
