User data
===================
Following methods allow to store, create, update, delete or search data coming user's applications.

UserData Get
---------------------
This method returns stored data coming from user's Application identified by a specified token.  The ``dataID`` parameter can be **all** or  a numeric value indicating a data identifier.

* **Endpoint**: /cdv/userdata/get/{dataID | all}
* **Method**: GET
* **Authorization Header**: Bearer
* **Produces**: application/json

**Output schema**

.. code-block:: json

    {
        "tags": [
          "string",
          ],
        "dataId": 0,
        "data": {
           "value": "string",
           "key": "string"
             },
        "type": "String"
      }

.. note::

    If  ``dataID`` is **all** , the output will contain all user data for the application. Else, the output will contain only user data by dataID.

UserData Update
-------------------------------
This method allows to create or update data coming from user's Application identified by a specified token. If input object contains **dataID** , update the corresponding user data; else, store a new user data and returns the **dataID** assigned to input object identified by index.

* **Endpoint**: /cdv/userdata/update
* **Method**: POST
* **Authorization Header**: Bearer
* **Consumes**: application/json
* **Produces**: application/json

**Input schema**

.. code-block:: json

    {
        "index": 0,
        "dataId": 0,
           "data":
             {
                   "key": "string",
                   "value": "string"
              },
           "type": "string",
           "tags": ["string"]
    }

**Output schema**

.. code-block:: json

  {
        "index": 0,
        "dataId": 0
  }

UserData Delete
-------------------------
This method allows to delete data coming from user's Application identified by a specified token. The ``dataID`` parameter can be **all** or a numeric value indicating a data identifier.

* **Endpoint**: /cdv/userdata/delete/{dataId | all}
* **Method**: DELETE
* **Authorization Header**: Bearer
* **Produces**: application/json

**Output schema**

.. code-block:: json

    {
        "dataId": 0,
        "data":
        {
            "key": "string",
            "value": "string"
        },
         "type": "string",
         "tags": ["string"]
     }

.. note::
    If  ``dataID`` is **all** , all user data for the application will be deleted. Else, only user data by **dataId** will be deleted.

UserData Search
----------------------------
This method allows to search all user data matching the given {tags}. It requires a specified token for the authorization.

* **Endpoint**: /cdv/userdata/search/{tags}
* **Method**: GET
* **Authorization Header**: Bearer
* **Produces**: application/json

**Output schema**

.. code-block:: json

    {
        "dataId": 0,
        "data":
         {
             "key": "string",
             "value": "string"
         },
        "type": "string",
        "tags": ["string"]
    }

Error output
-------------------

If the **authorization token** is not valid, the output will contain the following message:

    ``Invalid OAuth token``
