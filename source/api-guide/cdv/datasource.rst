Data source oriented
====================
Following methods allow the data sources (Open Innovation Area, Marketplace, Liferay User Manager etc) to create new WeLive User Profiles into CDV and update their information.

Create user
-----------
This method creates a new WeLive User Profile into the CDV.

* **Endpoint**: /cdv/adduser
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: application/json
* **Produces**: application/json

**Input schema**

.. code-block:: json

     {
      "ccUserID": 0,
      "id": 0,
      "referredPilot": ["Uusimaa",  "Bilbao",  "Trento",  "Novisad"],
      "firstName": "string",
      "lastName": "string",
      "isMale": false,
      "birthDate":
      {
        "day": 0,
        "month": 0,
        "year": 0
      },
      "email": "string",
      "isDeveloper": false,
      "city": "string",
      "address": "string",
      "country": "string",
      "zipCode": "string",
      "languages": ["string"],
      "userTags": ["string"],
      "role": ["Academy", "Business", "Citizen", "Entrepreneur"],
      "employement": [ "Student", "Unemployed", "Employedbythirdparty", "Selfemployedentrepreneur", "Retired", "Other" ],
      "liferayScreenName": "string"
    }

Update user
-----------
This method updates the WeLive User Profile identified by its cross-component user ID. If a property has been not specified, the corresponding info will not be updated.

* **Endpoint**: /cdv/updateuser
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: application/json
* **Produces**: application/json

**Input schema**

.. code-block:: json

  {
    "ccUserID": 0,
    "isMale": false,
    "birthDate":
    {
      "day": 0,
      "month": 0,
      "year": 0
    },
    "city": "string",
    "address": "string",
    "country": "string",
    "zipCode": "string",
    "languages": ["string"],
		"userTags": ["string"]
  }

Notify events
-------------
This method accepts a list of events and for each event it pushes information (preferences, skills, installed apps etc) about the the WeLive User Profile identified by its cross-component user ID.

* **Endpoint**: /cdv/push
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: application/json
* **Produces**: application/json

**Input schema**

.. code-block:: json

  [
    {
      "ccUserID": 0,
      "eventName": "string",
      "entries":
      [
        {
          "key": "string",
          "value": "any"
        }
      ]
    }
  ]

where ``eventName`` can be one of the following:

* **IdeaCreated**: the user created a new Idea; it doesn't require ``entries``
* **IdeaDeleted**: the user removed an idea; it doesn't require ``entries``
* **NewCollaboration**: the user collaborated to an idea; it doesn't require ``entries``
* **CollaborationRemoved**: the user has been removed from idea co-workers; it doesn't require ``entries``
* **SkillsUpdated**: the user updated her/his skills or she/he received an endorsement; it requires the following ``entries``

  .. code-block:: json

    [
      {
        "ccUserID": 1234,
        "eventName": "SkillsUpdated",
        "entries":
        [
          {
            "key": "PHP",
            "value": 5
          },
          {
            "key": "Java",
            "value": 8
          }
        ]
      }
    ]

* **NewAppInstalled**: the user installed a new app through the WeLive Player; it requires the following ``entries``

  .. code-block:: json

    [
      {
        "ccUserID": 1234,
        "eventName": "NewAppInstalled",
        "entries":
        [
          {
            "key": "appName",
            "value": "Bikesharing"
          },
          {
            "key": "appID",
            "value": 102
          }
        ]
      }
    ]

* **NewKnownLocation**: the last known location of the user has changed; it requires the following ``entries``

  .. code-block:: json

    [
      {
        "ccUserID": 1234,
        "eventName": "NewKnownLocation",
        "entries":
        [
          {
            "key": "lat",
            "value": "38.115688"
          },
          {
            "key": "lng",
            "value": "13.361267"
          }
        ]
      }
    ]

* **ReputationUpdated**: the user reputation has been updated; it requires the following ``entries``

  .. code-block:: json

    [
      {
        "ccUserID": 1234,
        "eventName": "ReputationUpdated",
        "entries":
        [
          {
            "key": "newReputation",
            "value": 15.2
          }
        ]
      }
    ]

Output
------

The previous APIs return the following JSON:

.. code-block:: json

  {
    "response": 0,
    "message": 0,
    "text": "string"
  }

where:

+--------------+----------------+--------------------+
| ``response`` | ``message``    | Description        |
+==============+================+====================+
| 0            | 0              | OPERATION_SUCCESS  |
+--------------+----------------+--------------------+
| 1            | 1              | USER_EXISTS        |
+--------------+----------------+--------------------+
| 1            | 2              | MISSING_PARAMETER  |
+--------------+----------------+--------------------+
| 1            | 3              | TYPE_MISMATCH      |
+--------------+----------------+--------------------+
| 1            | 4              | INTERNAL_ERROR     |
+--------------+----------------+--------------------+
| 1            | 5              | WRONG_CCUSERID     |
+--------------+----------------+--------------------+
| 1            | 6              | INVALID_INPUT      |
+--------------+----------------+--------------------+

and ``text`` represents a textual description of the message.
