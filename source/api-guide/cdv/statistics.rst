Statistics oriented
===================
Following methods allow the Analytics Dashboard to retrieve statistics about WeLive platform usage by the users.

Get Stakeholders Number
-----------------------
This method returns the number of WeLive users registered in the period from ``start-date`` to ``end-date`` who created or collaborated any need/idea (per city). The ``pilotID`` parameter can be one of the following values: **all**, **bilbao**, **helsinki**, **novisad** or **trento**. ``start-date`` and ``end-date`` must have the following format **YYYY-mm-dd** (e.g. 2016-05-11).

* **Endpoint**: /cdv/getKPI_1.1/{pilotID}/from/{start-date}/to/{end-date}
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Output schema**

.. code-block:: json

  {
    "Uusimaa": 0,
    "Bilbao": 0,
    "Trento": 0,
    "Novisad": 0
  }

.. note::

	If ``pilotID`` is **Uusimaa**, **Bilbao**, **Trento** or **Novisad**, the output will contain only the corresponding field.
	Moreover, the API can be invoked without specifying ``start-date`` and ``end-date`` (all registered users) or without specifying ``end-date`` (all users registered from ``start-date`` to the current date).

Get Users Number
----------------
This method returns the number of WeLive users registered in the period from ``start-date`` to ``end-date`` (per city). The ``pilotID`` parameter can be one of the following values: **all**, **Uusimaa**, **Bilbao**, **Trento** or **Novisad**. ``start-date`` and ``end-date`` must have the following format **YYYY-mm-dd** (e.g. 2016-05-11).

* **Endpoint**: /cdv/getKPI_4.3/{pilotID}/from/{start-date}/to/{end-date}
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Output schema**

.. code-block:: json

  {
    "Uusimaa": 0,
    "Bilbao": 0,
    "Trento": 0,
    "Novisad": 0
  }

.. note::

	If ``pilotID`` is **Uusimaa**, **Bilbao**, **Trento** or **Novisad**, the output will contain only the corresponding field.
	Moreover, the API can be invoked without specifying ``start-date`` and ``end-date`` (all registered users) or without specifying ``end-date`` (all users registered from ``start-date`` to the current date).

Get Users per Age
-----------------
This method returns the classification of WeLive users per age range (per city). The ``pilotID`` parameter can be one of the following values: **all**, **Uusimaa**, **Bilbao**, **Trento** or **Novisad**.

* **Endpoint**: /cdv/getKPI_11.1/{pilotID}
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Output schema**

.. code-block:: json

  {
    "Uusimaa":
    {
      "<20": 0,
      "20-40": 0,
      "40-60": 0,
      ">60": 0
    },
    "Bilbao":
    {
      "<20": 0,
      "20-40": 0,
      "40-60": 0,
      ">60": 0
    },
    "Trento":
    {
      "<20": 0,
      "20-40": 0,
      "40-60": 0,
      ">60": 0
    },
    "Novisad":
    {
      "<20": 0,
      "20-40": 0,
      "40-60": 0,
      ">60": 0
    }
  }

.. note::

  If ``pilotID`` is **Uusimaa**, **Bilbao**, **Trento** or **Novisad**, the output will contain only the corresponding field.

Get Users per Gender
--------------------

This method returns the classification of WeLive users per gender (per city). The ``pilotID`` parameter can be one of the following values: **all**, **Uusimaa**, **Bilbao**, **Trento** or **Novisad**.

* **Endpoint**: /cdv/getKPI_11.2/{pilotID}
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Output schema**

.. code-block:: json

  {
    "Uusimaa":
    {
      "male": 0,
      "female": 0
    },
    "Bilbao":
    {
      "male": 0,
      "female": 0
    },
    "Trento":
    {
      "male": 0,
      "female": 0
    },
    "Novisad":
    {
      "male": 0,
      "female": 0
    }
  }

.. note::

  If ``pilotID`` is **Uusimaa**, **Bilbao**, **Trento** or **Novisad**, the output will contain only the corresponding field.

Get Users per Work Status
--------------------------------------------------------------------

This method returns the classification of WeLive users per work status (per city). The options shown are: **No answer**, **Student**, **Unemployed**, **Employed by third party**, **Self-employed / Enterpreneur**, **Retired**, **Other**. The ``pilotID`` parameter can be one of the following values: **all**, **Uusimaa**, **Bilbao**, **Trento** or **Novisad**. ``start-date`` and ``end-date`` must have the following format **YYYY-mm-dd** (e.g. 2016-05-11).

* **Endpoint**: /cdv/getKPI_11.3/{pilotID}/from/{start-date}/to/{end-date}0
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Output schema**

.. code-block:: json

  {
    "Bilbao": {
      "None": 0,
      "Student": 0,
      "Unemployed": 0,
      "Employed by third party": 0,
      "Self-employed / Entrepreneur": 0,
      "Retired": 0,
      "Other": 0
    },
    "Novisad": {
      "None": 0,
      "Student": 0,
      "Unemployed": 0,
      "Employed by third party": 0,
      "Self-employed / Entrepreneur": 0,
      "Retired": 0,
      "Other": 0
    },
    "Uusimaa": {
      "None": 0,
      "Student": 0,
      "Unemployed": 0,
      "Employed by third party": 0,
      "Self-employed / Entrepreneur": 0,
      "Retired": 0,
      "Other": 0
    },
    "Trento": {
      "None": 0,
      "Student": 0,
      "Unemployed": 0,
      "Employed by third party": 0,
      "Self-employed / Entrepreneur": 0,
      "Retired": 0,
      "Other": 0
    }
  }

.. note::

    If ``pilotID`` is **Uusimaa**, **Bilbao**, **Trento** or **Novisad**, the output will contain only the corresponding field.
	Moreover, the API can be invoked without specifying ``start-date`` and ``end-date``.

Get Users per role in the platform
-------------------------------------------------------------------------------
This method returns the classification of WeLive users per role in the platform (per city). The option shown are: **Citizen**, **Academy**, **Business**, **Enterpreneur**.  The ``pilotID`` parameter can be one of the following values: **all**, **Uusimaa**, **Bilbao**, **Trento** or **Novisad**. ``start-date`` and ``end-date`` must have the following format **YYYY-mm-dd** (e.g. 2016-05-11).

* **Endpoint**: /cdv/getKPI_11.4/{pilotID}/from/{start-date}/to/{end-date}0
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Output schema**

.. code-block:: json

  {
    "Bilbao": {
      "Citizen": 0,
      "Academy": 0,
      "Business": 0,
      "Entrepreneur": 0
    },
    "Novisad": {
      "Citizen": 0,
      "Academy": 0,
      "Business": 0,
      "Entrepreneur": 0
    },
    "Uusimaa": {
      "Citizen": 0,
      "Academy": 0,
      "Business": 0,
      "Entrepreneur": 0
    },
    "Trento": {
      "Citizen": 0,
      "Academy": 0,
      "Business": 0,
      "Entrepreneur": 0
    }
  }

.. note::

    If ``pilotID`` is **Uusimaa**, **Bilbao**, **Trento** or **Novisad**, the output will contain only the corresponding field.
	Moreover, the API can be invoked without specifying ``start-date`` and ``end-date``.

Get Number of Developers
----------------------------------------------------------------------
This method returns the number of users registered as developers in the platform.
The ``pilotID`` parameter can be one of the following values: **all**, **Uusimaa**, **Bilbao**, **Trento** or **Novisad**. ``start-date`` and ``end-date`` must have the following format **YYYY-mm-dd** (e.g. 2016-05-11).

* **Endpoint**: /cdv/getKPI_11.4/{pilotID}/from/{start-date}/to/{end-date}0
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Output schema**

.. code-block:: json

  {
    "Bilbao": {
      "Yes": 0,
      "No": 0
    },
    "Novisad": {
      "Yes": 0,
      "No": 0
    },
    "Uusimaa": {
      "Yes": 0,
      "No": 0
    },
    "Trento": {
      "Yes": 0,
      "No": 0
    }
  }

.. note::

    If ``pilotID`` is **Uusimaa**, **Bilbao**, **Trento** or **Novisad**, the output will contain only the corresponding field.
	Moreover, the API can be invoked without specifying ``start-date`` and ``end-date``.

Error output
------------
If the ``pilotID`` is invalid, the previous APIs return the following JSON:

.. code-block:: json

  {
    "response": 1,
    "message": 6,
    "text": "Error: Invalid pilotID"
  }
