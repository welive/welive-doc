.. _Logging_API:

Logging API guide
=================

The service permits to store and search a log. A log is composed from a set of optional values
(text message, a duration of an operation) plus some metainformation (application Id, session Id, datetime of operation).

REST API
--------

**Endpoints**

+ *log_server_URL/welive.logging* for the log server
+ *rest-api_server_URL/welive/api* for the rest-api server


Push log message
----------------

Save a log message on the service

**Permission**

No permission needed


**Parameters**

+ appId (path variable) : application identifier [mandatory | STRING].
+ msg (JSON request body) : log message [mandatory | STRING].
+ appId (JSON request body) : application identifier [optional | STRING].
+ Value is set always override by appId path variable.
+ type (JSON request body): type of log [mandatory | STRING]. Some examples are AppStart, AppStop, AppLogin, AppConsume, AppProsume, AppODConsume, AppCollaborate, AppDataQueryInitiate, AppDataQueryComplete, AppDataQueryError, AppQuestionnaire or you can use a your custom value.
+ duration (JSON request body) : duration of application operation [optional | DOUBLE].
+ session (JSON request body): identifier of application session [optional | STRING].
+ timestamp (JSON request body): timestamp of operation [optional | NUMBER]. Value is setted to current if field is missing.
+ custom_attr (JSON request body): map of key/value representing custom attribute to current log message. Accepted values can be numbers or strings.

**Request**::

  POST *Endpoint*/log/{appId}
  Content-type: application/json
  Authorization: Bearer <ACCESS_TOKEN> or Basic <TOKEN>
  Body:
  {
    "msg":"logging test",
    "appId":"9001",
    "type":"AppCustom",
    "duration":0.1,
    "session":"avbfsr3k4",
    "timestamp":34839043490890482390
    "custom_attr": {"rating":1.2, "deviceId":"aaacckfkk334"}
  }

**Response**

+ HTTP 200 if call goes ok
+ HTTP 500 in case of error.


Query
-----

Returns the paginate list of result matching query criteria.

**Permission**

No permission needed

**Parameters**

+ appId (path variable) : application identifier [mandatory | STRING].
+ from (query param) : Timerange start. Express it in millis [optional | INTEGER]
+ to (query param) : Timerange end. Express it in millis [optional | INTEGER]
+ type (query param) : log type to search [optional | STRING]
+ pattern (query param) : search criteria on custom fields using Lucene syntax [optional | STRING]. Put in logical AND clause with msgPattern if present.
+ msgPattern (query param): search the pattern in log text [optional | STRING]. Put in logical AND clause with pattern if present.
+ limit (query param) : maximum number of messages to return [optional | INTEGER]. Default value is 150
+ offset (query param) : index of first message to return [optional | INTEGER]. Default value is 0


**Request**::

  GET *Endpoint*/log/{appId}
  Accept: application/json
  Authorization: Bearer <ACCESS_TOKEN> or Basic <TOKEN>

**Response**

+ HTTP 200 and the result if call goes ok

  .. code-block:: json

    {
      "offset": 0,
      "limit": 150,
      "data": [
        {
          "appId": "wer123",
          "type": "AppStart",
          "msg": "appstart logging",
          "timestamp": 1444732794782,
          "custom_attr":{}
        },
        {
          "appId": "wer123",
          "type": "AppCustom",
          "msg": "log with invalid type",
          "timestamp": 1444732732318,
          "custom_attr":{}
        }
      ],
      "total_results": 2
    }

+ HTTP 500 in case of error.

Count Query
-----------

Returns the number of result matching the query criteria

**Permission**

No permission needed

**Parameters**

+ appId (path variable) : application identifier [mandatory | STRING].
+ from (query param) : Timerange start. Express it in millis [optional | INTEGER]
+ to (query param) : Timerange end. Express it in millis [optional | INTEGER]
+ type (query param) : log type to search [optional | STRING]
+ pattern (query param) : search criteria on custom fields using Lucene syntax [optional | STRING]. Put in logical AND clause with msgPattern if present.
+ msgPattern (query param): search the pattern in log text [optional | STRING]. Put in logical AND clause with pattern if present.
+ limit (query param) : maximum number of messages to return [optional | INTEGER]. Default value is 150
+ offset (query param) : index of first message to return [optional | INTEGER]. Default value is 0


**Request**::

  GET *Endpoint*/log/{appId}
  Accept: application/json
  Authorization: Bearer <ACCESS_TOKEN> or Basic <TOKEN>

**Response**

+ HTTP 200 and the result if call goes ok

  .. code-block:: json

    {
      "total_results": 2
    }

+ HTTP 500 in case of error.


Log Aggregate
-------------

Returns one or more elasticsearch ( 1.7.3 ) aggregation(s). See https://www.elastic.co/guide/en/elasticsearch/reference/current/search-search.html and https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations.html

**Permission**

No permission needed

**Parameters**

+ request (JSON request body) : An elasticsearch search query [mandatory | STRING].


**Request**::

  POST *Endpoint*/log/aggregate
  Content-type: application/json
  Authorization: Bearer <ACCESS_TOKEN> or Basic <TOKEN>
  Body:
  {
      "_source" : false,
      "size" : 0,
      "aggs" : {
          "agg1" : {
              "sampler" : {
                  "field" : "message"
              }
          }
      }
  }

**Response**

+ HTTP 200 and the result if call goes ok

  .. code-block:: json

    {
      "took": 2,
      "timed_out": false,
      "_shards": {
        "total": 4,
        "successful": 4,
        "failed": 0
      },
      "hits": {
        "total": 7202,
        "max_score": 0,
        "hits": []
      },
      "aggregations": {
        "agg1": {
          "doc_count_error_upper_bound": 0,
          "sum_other_doc_count": 102,
          "buckets": [
            {
              "key": "dataset",
              "doc_count": 4884
            },
            {
              "key": "metadata",
              "doc_count": 3434
            },
            {
              "key": "accessed",
              "doc_count": 3384
            },
            {
              "key": "created",
              "doc_count": 2395
            },
            {
              "key": "resource",
              "doc_count": 2278
            },
            {
              "key": "updated",
              "doc_count": 1040
            },
            {
              "key": "removed",
              "doc_count": 328
            },
            {
              "key": "its",
              "doc_count": 37
            },
            {
              "key": "profile",
              "doc_count": 37
            },
            {
              "key": "request",
              "doc_count": 37
            }
          ]
        }
      }
    }

+ HTTP 500 in case of error.

Update Log Event Schema
-----------------------

This operation updates json schema for log event. As a result of this operation, the json schema will be stored on server side. Any log event matching the type of saved schema is validated before getting saved at server side.

**Request**::

  POST *Endpoint*/log/update/schema/{appId}/{type}
  Host: dev.welive.eu
  Authorization: Bearer <ACCESS_TOKEN> or Basic <TOKEN>
  Body: 
  {
	"ComponentSelected":{
	"$schema": "http://json-schema.org/draft-04/schema#",
	"type": "object",
	"properties": {
		"componentname": {
		"type": "string"
	}
	},
	"required": [
		"componentname"
	]
	}
  }

**Response**

+ HTTP 200 with no content.
+ HTTP 500 in case of error.

Read Log Event Schema
---------------------

This operation provides list of json schemas for all the log events of an application.

**Request**::

  GET *Endpoint*/log/read/schema/{appId}
  Host: dev.welive.eu
  Authorization: Bearer <ACCESS_TOKEN> or Basic <TOKEN>
  

**Response**

+ HTTP 200 with content.
 .. code-block:: json

    [
    {
        "schema": "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"properties\":{\"challengeid\":{\"type\":\"number\"},\"pilot\":{\"type\":\"string\"},\"authorityname\":{\"type\":\"string\"}},\"required\":[\"challengeid\",\"pilot\"],\"type\":\"object\"}",
        "appId": "oia",
        "type": "ChallengePublished"
    },
    {
        "schema": "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"properties\":{\"challengeid\":{\"type\":\"number\"},\"ideaid\":{\"type\":\"number\"},\"usersid\":{\"items\":{\"type\":\"number\"},\"type\":\"array\"},\"pilot\":{\"type\":\"string\"}},\"required\":[\"ideaid\",\"pilot\",\"usersid\"],\"type\":\"object\"}",
        "appId": "oia",
        "type": "IdeaRemoved"
    }
    ]

+ HTTP 500 in case of error.