Dataset Operations
==================

A dataset represents a data package formed by one or more data resources. A dataset contains two main fields:

  * Metadata about the data represented by this dataset.
  * Resources containing the data itself.

For accessing datasets from the Open Data Stack, two ``GET`` methods have been allowed:

/ods/dataset/all
  This method returns a JSON document with the ids of all the datasets hosted in the ODS:

    .. code-block:: json

      {
        "result": [
          "1987ko-udal-hauteskundeetako-emaitzak",
          "1991ko-udal-hauteskundeetako-emaitzak",
          "1995ko-udal-hauteskundeetako-emaitzak",
          "1999ko-udal-hauteskundeetako-emaitzak",
          "1-barrutiko-albisteak-deustu",
          "2006an-esleitutako-kontratuak",
          "2007an-esleitutako-kontratuak",
          "2008ko-designazio-libreko-langileen-ordainsariak",
          "2009ko-kontratazio-mahaiko-deialdiak",
          "2010ean-esleitutako-kontratuak",
          "2010eko-designazio-libreko-langileen-ordainsariak",
          "2010eko-kontratazio-mahaiko-deialdiak",
          "2011an-esleitutako-kontratuak",
          "2011ko-designazio-libreko-langileen-ordainsariak",
          "2011ko-funtzionarioen-ordainsariak",
          "2011ko-kontratazio-mahaiko-deialdiak"
        ]
      }

/ods/dataset/{datasetID}
  Returns a JSON document with the metadata of a dataset identified by ``{datasetID}``:

    .. code-block:: json

      {
        "result": {
          "maintainer_email": null,
          "resources": [
            {
              "position": 0,
              "revision_id": "a2aad6bf-0e76-4b94-8abf-33b41c016fdd",
              "hash": "",
              "webstore_url": null,
              "state": "active",
              "format": "CSV",
              "uri": "None",
              "url": "http://www.bilbao.net/bilbaoopendata/sector_publico/resultado_elecciones_municipales_1987.csv",
              "size": null,
              "id": "4913b2ed-f05f-4175-a97e-a81253a381a8",
              "last_modified": null,
              "mimetype": "text/csv",
              "cache_url": null,
              "created": "2015-10-05T09:32:15.939146",
              "cache_last_updated": null,
              "description": "",
              "url_type": null,
              "resource_type": null,
              "name": "CSV",
              "webstore_last_updated": null,
              "datastore_active": true,
              "package_id": "1f9d400e-7131-452a-928f-a11b4d95f04c",
              "mimetype_inner": null
            }
          ],
          "revision_id": "a2aad6bf-0e76-4b94-8abf-33b41c016fdd",
          "state": "active",
          "type": "dataset",
          "private": false,
          "version": null,
          "owner_org": "3e49079a-14d7-4d69-9e71-fc700c4761e4",
          "id": "1f9d400e-7131-452a-928f-a11b4d95f04c",
          "author": null,
          "relationships_as_subject": [],
          "title": "Resultado de las Elecciones Municipales del año 1987",
          "organization": {
            "id": "3e49079a-14d7-4d69-9e71-fc700c4761e4",
            "revision_id": "464cd582-29d9-41ee-8357-9b3370bf9750",
            "title": "Bilbao",
            "is_organization": true,
            "image_url": "2015-09-30-123527.952561bilbao.jpg",
            "created": "2015-09-17T09:58:39.599248",
            "description": "Datasets from Bilbao City Council.",
            "approval_status": "approved",
            "name": "bilbao-city-council",
            "state": "active",
            "type": "organization"
          },
          "license_title": null,
          "name": "1987ko-udal-hauteskundeetako-emaitzak",
          "license_id": null,
          "creator_user_id": "30943c59-975a-463d-a3dd-2b7fef477056",
          "groups": [],
          "num_resources": 1,
          "tags": [
            {
              "id": "8f89d691-f879-4671-9817-69c569480053",
              "display_name": "1987",
              "vocabulary_id": null,
              "name": "1987",
              "state": "active"
            },
            {
              "id": "497ccbb9-2aaa-472e-87df-4e847dee6637",
              "display_name": "elecciones municipales",
              "vocabulary_id": null,
              "name": "elecciones municipales",
              "state": "active"
            }
          ],
          "isopen": false,
          "metadata_created": "2015-10-02T12:25:29.175920",
          "relationships_as_object": [],
          "url": null,
          "maintainer": null,
          "extras": [
            {
              "value": "http://www.bilbao.net/opendata/catalogo/dato-resultados-elecciones-municipales-1987",
              "key": "guid"
            },
            {
              "value": "http://www.bilbao.net/opendata/catalogo/dato-resultados-elecciones-municipales-1987",
              "key": "identifier"
            },
            {
              "value": "2014-10-30",
              "key": "issued"
            },
            {
              "value": "2014-10-30",
              "key": "modified"
            },
            {
              "value": "http://datos.gob.es/recurso/sector-publico/org/Organismo/L01480209",
              "key": "publisher_uri"
            },
            {
              "value": "http://datos.gob.es/recurso/sector-publico/territorio/pais/Espana",
              "key": "spatial_uri"
            },
            {
              "value": "[\"http://datos.gob.es/kos/sector-publico/sector/sector-publico\"]",
              "key": "theme"
            },
            {
              "value": "http://www.bilbao.net/opendata/catalogo/dato-resultados-elecciones-municipales-1987",
              "key": "uri"
            }
          ],
          "author_email": null,
          "metadata_modified": "2015-10-05T09:32:15.892840",
          "rating": null,
          "notes": "Detalle del resultado de las elecciones municipales del a&#241;o 1987, de todo Bilbao y de sus 8 Distritos.",
          "num_tags": 2
        }
      }

  As can be seen, among the metadata of the dataset, there are four fields storing another JSON object or a list of JSON objects:

    * **Resources**: as explained in the next section, resources represent the data itself. A dataset can store more than one resource.
    * **Organization**: this JSON object contains metadata about the organization to which belongs the dataset.
    * **Tags**: a list of tags for this dataset.
    * **Extras**: in this list, custom key-value pairs can be stored.

For creating datasets a ``POST`` method has been allowed:

/ods/dataset
  The body of the request to this method is formed by a JSON object similar to the given by the **/ods/dataset/{datasetID}**. The fields of this JSON object are described in `CKAN's Action API Reference <http://docs.ckan.org/en/ckan-2.4.1/api/index.html#ckan.logic.action.create.package_create>`_. For example, for creating the previously shown dataset, the following JSON have to be send through the ``POST`` request:

    .. code-block:: json

      {
        "maintainer_email": null,
        "state": "active",
        "type": "dataset",
        "private": false,
        "version": null,
        "owner_org": "3e49079a-14d7-4d69-9e71-fc700c4761e4",
        "author": null,
        "relationships_as_subject": [],
        "title": "Resultado de las Elecciones Municipales del año 1987",
        "license_title": null,
        "name": "1987ko-udal-hauteskundeetako-emaitzak",
        "license_id": null,
        "groups": [],
        "tags": [
          {
            "vocabulary_id": null,
            "name": "1987",
          },
          {
            "vocabulary_id": null,
            "name": "elecciones municipales",
          }
        ],
        "relationships_as_object": [],
        "url": null,
        "maintainer": null,
        "extras": [
          {
            "value": "http://www.bilbao.net/opendata/catalogo/dato-resultados-elecciones-municipales-1987",
            "key": "guid"
          },
          {
            "value": "http://www.bilbao.net/opendata/catalogo/dato-resultados-elecciones-municipales-1987",
            "key": "identifier"
          },
          {
            "value": "2014-10-30",
            "key": "issued"
          },
          {
            "value": "2014-10-30",
            "key": "modified"
          },
          {
            "value": "http://datos.gob.es/recurso/sector-publico/org/Organismo/L01480209",
            "key": "publisher_uri"
          },
          {
            "value": "http://datos.gob.es/recurso/sector-publico/territorio/pais/Espana",
            "key": "spatial_uri"
          },
          {
            "value": "[\"http://datos.gob.es/kos/sector-publico/sector/sector-publico\"]",
            "key": "theme"
          },
          {
            "value": "http://www.bilbao.net/opendata/catalogo/dato-resultados-elecciones-municipales-1987",
            "key": "uri"
          }
        ],
        "author_email": null,
        "notes": "Detalle del resultado de las elecciones municipales del a&#241;o 1987, de todo Bilbao y de sus 8 Distritos.",
      }

  For creating a dataset only fields ``name``  and ``owner_org`` (bilbao-city-council, helsinki-uusimaa, novi-sad or trento) are mandatory. The rest of the parameters are optional, being some of them automatically created by the ODS.

For modifying datasets, a method allowing a ``PUT`` operation has been allowed. The path for this method is the same of the method for getting datasets:

  /ods/dataset/{datasetID}
    This method modifies the metadata about the dataset identified by ``{datasetID}``. A JSON similar to the given when getting the metadata about the dataset has to be sent in the body of the ``PUT`` request. For this reason it is convenient to first make a ``GET`` request to the URL of this method, apply modifications to the given JSON, and finally return back to the Open Data Stack through a ``PUT``.

At last, for deleting datasets a ``DELETE`` call can be done to the same URL:

  /ods/dataset/{datasetID}
    Where the ``{datasetID}`` represents the dataset to be deleted.
