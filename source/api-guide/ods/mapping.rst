Mapping Operations
------------------

These methods manage the mapping for the :ref:`query_mapper`. For creating a mapping for a resource, a ``POST`` request can be done to the following method:

  /ods/dataset/{datasetID}/resource/{resourceID}/mapping
    This methods requires a JSON object in the body of the request. The format of this JSON can be checked at :ref:`mappings`.

For retrieving, modifying and deleting a mapping, ``GET``, ``PUT`` and ``POST`` methods have been allowed for the same URL. The ``PUT`` operation requires the same JSON object than the ``POST`` operation in the body of the request.
