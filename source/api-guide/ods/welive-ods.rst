.. _welive-ods:

Open Data Stack
===============

The following methods allow interacting with the Open Data Stack. These methods
execute CRUD operations over dataset, resource, mapping and validation objects.
While the following sections widely explains the API methods, a brief API
description can be found at :ref:`ods-api`.

.. note::
  For more information about the data model used by CKAN, we encourage the reading
  of `CKAN documentation <http://docs.ckan.org/en/ckan-2.4.1/user-guide.html>`_
  and `CKAN API reference <http://docs.ckan.org/en/ckan-2.4.1/api/index.html>`_.

.. note::
  For all ``POST``, ``PUT`` and ``DELETE`` operations a valid token provided by
  WeLive's Authorization and Authentication Component (AAC) is required. The token
  has to be granted to access to user's **profile.basicprofile.me** and
  **profile.accountprofile.me** profiles.  When calling to API methods directly,
  the token has to be included in the ``Authorization`` header of the request:
  ``Authorization: Bearer <token>``. On the other hand, when calling to API
  methods through the Swagger interface, the interface itself can log in the user,
  as explained at :ref:`swagger-authentication`.

.. toctree::
  dataset
  resource
  mapping
  data
  validation
  api-reference
