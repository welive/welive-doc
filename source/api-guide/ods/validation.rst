Validation
----------
These methods allow managing the schema for validation of resources. For validating resources, four validation schemas are supported:

  * `CSV Schema <http://digital-preservation.github.io/csv-schema/csv-schema-1.0.html>`_.
  * `XML Schema <http://www.w3schools.com/schema/>`_.
  * `JSON Schema <http://json-schema.org/>`_.
  * RDF files: RDF files are validated against their ontologies, so **no validation schema is required**.

A unique URL is provided for dealing with the validation schemas of the resources. This URL accepts ``GET`` operations for retrieving the mapping, ``POST`` for creating a mapping, ``PUT`` for modifying a mapping and ``DELETE`` for deleting mappings:

  /ods/dataset/{datasetID}/resource/{resourceID}/validation
    Where ``{datasetID}`` and ``{resourceID}`` are ids of dataset and resource respectively. In the case of ``POST`` and ``PUT`` methods, the mapping schema has to be posted/putted in the body of the request as a ``text/plain`` context.
