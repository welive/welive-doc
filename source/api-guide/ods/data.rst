.. _data_api:

Data Operations
===============

This part of the Open Data Stack API contains operations to query and update
data on registered datasets.

.. note::

  It should be taken into account that each dataset can have different access
  restrictions that specified using the mechanism explained in section
  :ref:`permissions`. Therefore, some methods require to provide a valid user
  token to access or update some data.

.. note::

  See `jQuery <http://jquery.com>`_  for more information about this library and how to
  use the methods it provides. Examples using jQuery can be tested with the
  Chrome Developer Console by loading the library before executing each example.:

    .. code-block:: javascript

      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = 'http://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js';
      document.head.appendChild(script);

Obtaining data structure
------------------------

Before starting to execute queries or updates in a dataset, it is required to
know its structure. The Open Data Stack API provides a method to obtain the
description of a datasource (tables and columns).

/ods/{dataset}/resource/{resource}/mapping/info
  This method retrieves the structure of the dataset. It can be directly opened
  in a browser or accessed programatically. For example, opening the following
  URL returns the description of an example dataset.

  https://dev.welive.eu/dev/api/ods/test-dataset/resource/ef408806-d7d7-4e3e-bf91-41f27018f412/mapping/info

  The platform will return the tables and columns that describe the following.

  .. code-block:: json

      {
        "tables": [
          {
            "columns": [
              {
                "key": true,
                "name": "id",
                "type": "INTEGER"
              },
              {
                "key": false,
                "name": "place",
                "type": "VARCHAR"
              },
              {
                "key": false,
                "name": "rating",
                "type": "INTEGER"
              },
              {
                "key": false,
                "name": "timestamp",
                "type": "DATE"
              },
              {
                "key": false,
                "name": "_row_user_",
                "type": "VARCHAR"
              }
            ],
            "name": "POI"
          }
        ],
        "type": "json_schema"
      }

.. _querying_data:

Querying data
-------------

This methods are used to query the data contained in the registered datasets.
All methods require to provide the identifier of the accessed dataset and
resource, which can be obtained using the dataset methods from the
:ref:`datasets`.

/ods/{dataset}/resource/{resource}/query
    This method enables to execute an SQL query on a registered dataset/resource.
    The Javascript code shows howto query a dataset and obtain a JSON response. The
    code first gets the id of the dataset identified by ``sql_dataset2`` and then
    executes the ``SELECT * FROM country;`` query. Query must be sent in
    ``text/plain`` format.

    .. note::

      When selecting information users can select the source of the information retrieved
      information using the ``?origin=`` query parameter of the method, which supports
      three different values: ``original``, ``user``, ``any``, allowing to retrieve
      datasource original values only, user provided values only, or a response containing
      a merge between the two data origins.

    .. warning::

        This method can be executed on any dataset compatible with a SQL query. All
        dataset backed by a ``database`` or a ``sparql`` endpoint are compatible with this
        kind of query. Use the ``/ods/{dataset}/resource/{resource}/info`` method to obtain
        information about the dataset.

    .. code-block:: javascript

        var API_BASE = 'https://dev.welive.eu/dev/api';

        var query_url = API_BASE + '/ods/portales-de-bilbao/resource/73b6103b-0c12-4b2c-98ae-71ed33e55e8c/query'
        var query = 'SELECT TDIS_DES_DISEST_A, TBAR_DES_BARRIO_A from results limit 5';

        $.ajax ({
            url: query_url,
            type: "POST",
            data: query,
            contentType: 'text/plain',
            }
        ).done(function(json) {
            console.log(JSON.stringify(json));
        });

    An example of the JSON response is shown in the next figure. The JSON response always
    contains the number of retrieved rows in ``count`` and an array of rows with an entry
    for each retrieved row, which contain an entry for each retrieved column.

    For example, the previous query has returned 5 rows with the ``TDIS_DES_DISEST_A``,
    ``TBAR_DES_BARRIO_A``, which contain information about some neighbourhoods of Bilbao.

    .. code-block:: json

        {
          "count":5,
          "rows":[
            {
               "TDIS_DES_DISEST_A":"DEUSTU",
               "TBAR_DES_BARRIO_A":"SAN IGNACIO"
            },
            {
               "TDIS_DES_DISEST_A":"DEUSTU",
               "TBAR_DES_BARRIO_A":"SAN IGNACIO"
            },
            {
               "TDIS_DES_DISEST_A":"DEUSTU",
               "TBAR_DES_BARRIO_A":"SAN IGNACIO"
            },
            {
               "TDIS_DES_DISEST_A":"DEUSTU",
               "TBAR_DES_BARRIO_A":"SAN IGNACIO"
            },
            {
               "TDIS_DES_DISEST_A":"DEUSTU",
               "TBAR_DES_BARRIO_A":"SAN IGNACIO"
            }
          ]
        }

Updating data
-------------

The data contained in the registered datasets can be updated using the following methods.
In the case of datasets mapped to external JSON (:ref:`json-mapping`) or CSV
(:ref:`csv-mapping`), as the source is usually read-only, the data inserted
by the users is stored locally by the platform in a automatically created database. For
more information about how to filter the information depending on its origin see
:ref:`querying_data`.

/ods/{dataset}/resource/{resource}/update
    This method enables to perform a data update on a registered dataset.
    The following code shows an example that executes an update on the an
    example datasource.

    .. note::

        Remember to replace ``XXXX-XXXX-XXXX-XXXXX`` with a valid token.

    This example uses the dataset structure created in the example showed in
    :ref:`user-dataset-example`. This dataset is registered with the dataset
    name ``test-dataset`` and resource id ``ef408806-d7d7-4e3e-bf91-41f27018f412``.

    .. code-block:: javascript

      var API_BASE = 'https://dev.welive.eu/dev/api';

      var query_url = API_BASE + '/ods/test-dataset/resource/ef408806-d7d7-4e3e-bf91-41f27018f412/update'
      var query = 'INSERT INTO POI (id, place, rating, timestamp) VALUES (null, "someplace", 5, "2016-05-16")';

      var token = 'Bearer XXXX-XXXX-XXXX-XXXXX';

      $.ajax ({
          url: query_url,
          type: "POST",
          data: query,
          contentType: 'text/plain',
          headers: {"Authorization": token }
          }
      ).done(function(json) {
          console.log(JSON.stringify(json));
      });

    The result of the query is a JSON object containing the number of modified rows.

    .. code-block:: json

        {
            "rows": 1
        }

    In addition the method supports the execution of multiple update statements in a single
    transaction. In order to enable transaction support the update method must be invoked
    with the ``?transaction=true`` parameter. This will enable the execution of multiple
    update statements passed in the following form as the method data.

    .. code-block:: none

      INSERT INTO POI (id, place, rating, timestamp) VALUES (null, "someplaceA", 5, "2016-05-16");
      INSERT INTO POI (id, place, rating, timestamp) VALUES (null, "someplaceB", 5, "2016-05-16");

    If there is a error during the execution of some of the passed update statements, the
    dataset will perform and automatic rollback to the previous state. If update statements
    are correctly executed, the method will return the number of total modifed rows.
