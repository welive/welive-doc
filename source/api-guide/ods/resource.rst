.. _resource-operations:

Resource Operations
-------------------

A resource is a file or a link related to a dataset. For example, given the dataset `2008an esleitutako kontratuak <http://apps.morelab.deusto.es/ods/dataset/contratos-adjudicados-durante-el-ano-2008>`_, three resources belonging to this dataset can be seen:

  * A resource representing data described by the dataset in JSON format.
  * Another resource representing the same data in CSV format.
  * A last resource representing the same data in XML format.

Any kind of file can be used to create a resource for a dataset, but structured files like CSV, XLS, XML, JSON or RDF are preferred for interoperation purposes.

As seen in :doc:`dataset`, similar methods have been deployed for dealing with resources. Given a dataset, all its resources are retrieved when calling to previously described **/ods/dataset/{datasetID}**. For retrieving data from a concrete resource, a ``GET`` request can be done to the following method:

  /ods/dataset/{datasetID}/resource/{resourceID}
    This method returns the following JSON document:

      .. code-block:: json

        {
          "result": {
            "position": 0,
            "revision_id": "11925ac1-1a57-4b56-af54-508c50f72d99",
            "hash": "",
            "webstore_url": null,
            "state": "active",
            "format": "RSS",
            "uri": "None",
            "url": "http://www.bilbao.net/cs/Satellite?language=es&pageid=3000075248&pagename=Bilbaonet/Page/BIO_suscripcionRSS&tipoSus=Noticias&idSec=3000014162",
            "size": null,
            "id": "633949a7-f7ff-4dcc-8775-56f7bfb5e631",
            "last_modified": null,
            "mimetype": "application/rss+xml",
            "cache_url": null,
            "created": "2015-10-05T09:31:22.193808",
            "cache_last_updated": null,
            "description": "",
            "url_type": null,
            "resource_type": null,
            "name": "RSS",
            "webstore_last_updated": null,
            "datastore_active": false,
            "package_id": "733213ad-97c2-4c78-b632-3617af6267d9",
            "mimetype_inner": null
          }
        }

For creating a new resource a ``POST`` request can be done to the following URL:

  /ods/dataset/{datasetID}/resource
    Where the ``{datasetID}`` is the id of the dataset to which the resource belongs. This ``POST`` request consumes a ``multipart/form-data`` content. This form contains three fields:

      * **packageDict**: A JSON object similar to the given by **/ods/dataset/{datasetID}/resource/{resourceID}**:

        .. code-block:: json

          {
            "revision_id": "11925ac1-1a57-4b56-af54-508c50f72d99",
            "hash": "",
            "webstore_url": null,
            "state": "active",
            "format": "RSS",
            "url": "http://www.bilbao.net/cs/Satellite?language=es&pageid=3000075248&pagename=Bilbaonet/Page/BIO_suscripcionRSS&tipoSus=Noticias&idSec=3000014162",
            "size": null,
            "id": "633949a7-f7ff-4dcc-8775-56f7bfb5e631",
            "last_modified": null,
            "mimetype": "application/rss+xml",
            "cache_url": null,
            "created": "2015-10-05T09:31:22.193808",
            "cache_last_updated": null,
            "description": "",
            "url_type": null,
            "resource_type": null,
            "name": "RSS",
            "webstore_last_updated": null,
            "datastore_active": false,
            "mimetype_inner": null,
          }

        For creating a resource, only the ``name`` field is required. The rest of the parameters are optional, being some of them automatically created by the ODS. More information about this JSON object can be found at `CKAN's Action API Reference <http://docs.ckan.org/en/ckan-2.4.1/api/index.html#ckan.logic.action.create.resource_create>`_.

      * **upload**: a FileStorage field. This field contains the data file to be attached to the resource. **This field is mandatory only if a** *url* **field is not declared in packageDict JSON object**. The *url* field in *packageDict* JSON object represents the URL where the resource is hosted, if it is hosted outside the ODS. The ODS will index and load the file linked by this URL into its database if the file format is compatible.

      * **fileInfo**: metadata about the file included in *upload* field. Most of HTTP clients and libraries generates this parameter automatically without the interaction of the user. If not, almost the parameter ``fileName`` is required.

      Next, an example of creating a resource using Python's `requests <http://docs.python-requests.org/en/latest/>`_ library is shown:

      .. code-block:: python

        import requests

        API_KEY = '<API_KEY>'
        URL = 'http://apps.morelab.deusto.es/welive/api/ods/dataset/example-dataset/resource'

        payload = {'packageDict': "{'name': 'example-resource'}"}
        files = {'upload': ('example.csv', open('/tmp/example.csv', 'r'))}
        headers = {'Authorization': API_KEY}

        response = requests.post(URL, data=payload, files=files, headers=headers)


For modifying a resource, a ``PUT`` request can be done to the same URL used to get metadata from a resource:

  /ods/dataset/{datasetID}/resource/{resourceID}
    This method receives the same parameters than the method for creating a dataset. For correctly fulfilling the ``packageDict`` JSON object, we recommend to get it through making a ``GET`` over **/ods/dataset/{datasetID}/resource/{resourceID}**, modify it and return back inside the body of this ``PUT`` request.

At last, to delete a resource, a ``DELETE`` request can be done to the same URL:

  /ods/dataset/{datasetID}/resource/{resourceID}
    Where ``{datasetID}`` and ``{resourceID}`` represents the ids of dataset and resource respectively.
