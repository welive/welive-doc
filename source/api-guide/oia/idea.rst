Idea implementation
===================
Following methods allow the Visual Composer to interact with the Open Innovation Area for the idea implementation.

Add mashup/mockup project
-------------------------
Add a new mashup/mockup project associated with an Idea.

* **Endpoint**: /oia/addVcProject
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: application/json
* **Produces**: application/json

**Input schema**

.. code-block:: json

  {
    "vcProjectId": "string",
    "vcProjectName": "string",
    "ideaId": "string",
    "mockup": false
  }

where:

* ``vcProjectId`` is the project identifier in the Visual Composer;
* ``vcProjectName`` is the project name;
* ``ideaId`` is the idea ID;
* ``mockup`` is true if the project is of mockup type, false otherwise.

Publish Screenshot
------------------
Associates a new mockup screenshot to the Idea identified by ``idea-id``.

* **Endpoint**: /oia/new-screenshot/ideaId/{idea-id}
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: application/json
* **Produces**: application/json

**Input schema**

.. code-block:: json

  {
    "encodedScreenShot": "string",
    "description": "string",
    "imgMimeType": "string",
    "username": "string"
  }

where:

* ``encodedScreenShot`` is the screenshot file (Base64 encoded);
* ``description`` is a short description of the screenshot;
* ``imgMimeType`` is the Mime-Type of the image (image/png, image/jpeg etc);
* ``username`` is the username (email) of the user who made the screenshot.

Output
------
Previous methods returns a JSON with the following schema:

.. code-block:: json

  {
    "error": false,
    "message": "string"
  }
