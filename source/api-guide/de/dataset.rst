Dataset Operations
==================

Following methods allow performing CRUD operations over the metadata of the datasets published by the Welive Open Data Stack.

  PUT /de/dataset/{datasetID}
    This method allows creating or updating metadata about a dataset. The ``{datasetID}`` parameter refers to the ID of the dataset in the Open Data Stack. This method requests the following JSON containing the information of the dataset:

    .. code-block:: json

      {
      	"lang": "spanish",
      	"tags": ["tag1", "tag2"],
      }

    * **lang:** the language of the dataset. Languages supported: danish, dutch, english, finnish, french, german,
      hungarian, italian, norwegian, porter, portuguese, romanian, russian,
      spanish and swedish.
    * **tags:** tags of the app.

  DELETE /de/dataset/{datasetID}
    This method allows deleting metadata about a dataset. The ``{datasetID}`` parameter refers to the ID of the dataset in the Open Data Stack.

  GET /de/dataset/{datasetID}/recommend/apps
    This method returns recommended apps based on given dataset tags. The ``{datasetID}`` parameter refers to the ID of the dataset in the Open Data Stack.

  GET /de/dataset/{datasetID}/recommend/building-blocks
    This method returns recommended building blocks based on given dataset tags. The ``{datasetID}`` parameter refers to the ID of the dataset in the Open Data Stack.

  GET /de/dataset/{datasetID}/recommend/datasets
    This method returns recommended datasets based on given dataset tags. The ``{datasetID}`` parameter refers to the ID of the dataset in the Open Data Stack.
