Building Block Operations
=========================

Following methods allow performing CRUD operations over the metadata of the building blocks published by the Welive Marketplace.

  PUT /de/building-block/{buildingBlockID}
    This method allows creating or updating metadata about a building block. The ``{buildingBlockID}`` parameter refers to the ID of the building block. This method requests the following JSON containing the information of the building block:

    .. code-block:: json

      {
      	"lang": "spanish",
      	"tags": ["tag1", "tag2"],
      }

    * **lang:** the language of the building block. Languages supported: danish, dutch, english, finnish, french, german,
      hungarian, italian, norwegian, porter, portuguese, romanian, russian,
      spanish and swedish.
    * **tags:** tags of the building block.

  DELETE /de/building-block/{buildingBlockID}
    This method allows creating or updating a metadata about a building block. The ``{buildingBlockID}`` parameter refers to the ID of the building block.

  GET /de/building-block/{buildingBlockID}/recommend/apps
    This method returns recommended apps based on given building block tags. The ``{buildingBlockID}`` parameter refers to the ID of the app in the marketplace.

  GET /de/building-block/{buildingBlockID}/recommend/building-blocks
    This method returns recommended building blocks based on given building block tags. The ``{buildingBlockID}`` parameter refers to the ID of the app in the marketplace.

  GET /de/building-block/{buildingBlockID}/recommend/datasets
    This method returns recommended datasets based on given building block tags. The ``{buildingBlockID}`` parameter refers to the ID of the app in the marketplace.
