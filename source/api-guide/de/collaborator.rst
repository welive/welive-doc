User Recommendation Operations
==============================

Following methods allow querying for users suitable as collaborators for the consecution of an idea as needed by the Open Innovation Area.

    POST /de/idea/recommend/users_by_skills
        This methods returns a list of users whose skills match the tags and/or categories specified in the response. These tags and categories are related to an idea to be implemented.
        
        Although logically a GET operation, it has been designed as a POST operation due to de complexity of the format of the request parameters. The request accepts *application/json* Content-Types according to the format of the following example:
        
        .. code-block:: json
            
            {
                "lang": "english",
                "tag_list": [
                    "java",
                    "php"
                ],
                "category_list": [
                    "security"
                ]
            }
        
        * **lang:** The language of the tags and categories specified. When different from *english*, tags and categories are translated into English via Microsoft Translator API before any matching. Languages currently supported are *danish*, *dutch*, *english*, *finnish*, *french*, *german*, *hungarian*, *italian*, *norwegian*, *porter*, *portuguese*, *romanian*, *russian*, *spanish* and *swedish*.
        * **tag_list:** A list of tags the users' skills must match. The tags are supposed to match the specified language.
        * **category_list:** A list of idea categories the users' skills must match. The categories are supposed to match the specified language.
        
        The returned list of recommended collaborators includes those whose skills cover at least 50% of the tags and categories specified. The matching between the tags/categories and skills is performed by approximation.
        
        The response is formatted as a JSON list (*Content-Type: application/json*) as can be observed in the following example:
        
        .. code-block:: json
            
            [
                {
                    "user_id": 123,
                    "matching_rate": 0.67,
                    "matching_skill_list": [
                        "java",
                        "php"
                    ]
                }
            ]
        
        * **user_id:** Unique user ID (also known as *CCUserID*) of the matching collaborator.
        * **matching_rate:** Number of user's skills that matched the tags and categories as a fraction of unity.
        * **matching_skill_list:** Skills that matched the tags and categories (post-processed in lower case).
        
        On error, any HTTP response code different from 200 is returned.
        
        .. note::
            
            This call requires **HTTP Basic Authentication**.
        
        .. note::
            
            Users and their skills lists are cached during an hour, so that last updates related to this information may not be reflected in the response.
        
        
