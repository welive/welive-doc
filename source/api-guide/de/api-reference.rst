.. _de-api:

API methods
===========

For a wider explanation of the methods listed below, see documentation at :doc:`welive-de`.

.. swaggerdoc:: https://dev.welive.eu/dev/api/api-docs/de
