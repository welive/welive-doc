Idea Operations
==================

Following methods allow performing CRUD operations over the metadata of the ideas published by the Open Innovation Area.

  PUT /de/idea/{ideaID}
    This method allows creating or updating metadata about an idea. The ``{ideaID}`` parameter refers to the ID of the idea in the Open Innovation Area. This method requests the following JSON containing the information of the dataset:

    .. code-block:: json

      {
      	"lang": "spanish",
      	"text": "Lorem ipsum dolosor",
      }

    * **lang:** the language of the idea. Languages supported: danish, dutch, english, finnish, french, german,
      hungarian, italian, norwegian, porter, portuguese, romanian, russian,
      spanish and swedish.
    * **text:** the text of the idea.

  DELETE /de/idea/{ideaID}
    This method allows deleting metadata about a idea. The ``{ideaID}`` parameter refers to the ID of the idea in the Open Innovation Area.

  GET /de/idea/{ideaID}/recommend/ideas
    This method returns recommended ideas based on given idea text. The ``{ideaID}`` parameter refers to the ID of the idea in the Open Innovation Area.
