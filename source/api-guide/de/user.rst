User Operations
==================

Following method allows getting recommended apps for a user from the Citizen Data Vault.

  GET /de/user/{userID}/apps?lat=latitude&lon=longitude&radius=radius
    This method returns recommended apps based on given user profile. The ``{userID}`` parameter refers to the ID of user in the Citizen Data Vault. The parameters of the query represent the following values:

    * **lat:** current latitude of the location of the user.
    * **lon:** current longitude of the location of the user.
    * **radius:** a radius in which apps have to be searched, expressed in kilometers.
