.. _welive-de:

Welive Decision Engine
======================

Following methods allow interacting with the Decision Engine.

These methods execute CRUD operations over dataset, building block, app and idea metadata, and allow retrieving different recommendations for each different type of item. While the following sections widely explains the API methods, a brief API description can be found at :ref:`de-api`.

.. toctree::
  app
  bb
  dataset
  idea
  user
  collaborator
  api-reference
