App Operations
==============

Following methods allow performing CRUD operations over the metadata of the apps published by the Welive Marketplace.

  PUT /de/app/{appID}
    This method allows creating or updating metadata about an app. The ``{appID}`` parameter refers to the ID of the app in the marketplace. This method requests the following JSON containing the information of the app:

    .. code-block:: json

      {
      	"lang": "spanish",
      	"tags": ["tag1", "tag2"],
      	"scope": "bilbao",
      	"minimum_age": 13
      }

    * **lang:** the language of the app. Languages supported: danish, dutch, english, finnish, french, german,
      hungarian, italian, norwegian, porter, portuguese, romanian, russian,
      spanish and swedish.
    * **tags:** tags of the app.
    * **scope:** the city or location in which is scoped the app.
    * **minimum_age:** the minimum age required to use the app.

  DELETE /de/app/{appID}
    This method allows deleting metadata about an app. The ``{appID}`` parameter refers to the ID of the app in the marketplace.

  GET /de/app/{appID}/recommend/apps
    This method returns recommended apps based on given app tags. The ``{appID}`` parameter refers to the ID of the app in the marketplace.

  GET /de/app/{appID}/recommend/building-blocks
    This method returns recommended building blocks based on given app tags. The ``{appID}`` parameter refers to the ID of the app in the marketplace.

  GET /de/app/{appID}/recommend/datasets
    This method returns recommended datasets based on given app tags. The ``{appID}`` parameter refers to the ID of the app in the marketplace.
