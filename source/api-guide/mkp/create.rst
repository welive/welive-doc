Publish Artefacts
=================
Following methods allow to publish new artefacts in the WeLive Marketplace.

Publish Service
---------------
Publishes a service into the Marketplace.

* **Endpoint**: /mkp/create-service
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: application/json
* **Produces**: application/json

**Input schema**

.. code-block:: json

	{
		"title": "string",
		"description": "string",
		"resourceRdf": "string",
		"webpage": "string",
		"ccUserId": 0,
		"providerName": "string",
		"endpoint": "string",
		"category": "REST"
	}

Publish Dataset
---------------
Publishes a dataset into the Marketplace.

* **Endpoint**: /mkp/create-dataset
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: application/json
* **Produces**: application/json

**Input schema**

.. code-block:: json

	{
		"title": "string",
		"description": "string",
		"resourceRdf": "string",
		"webpage": "string",
		"ccUserId": 0,
		"providerName": "string",
		"datasetId": "string"
	}

Output
------
Previous methods returns a JSON with the following schema:

.. code-block:: json

  {
		"error": false,
		"message": "string",
		"artefactid": 0
  }
