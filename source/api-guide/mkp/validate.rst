Validation APIs
===============
Following methods allow to validate RDF files or service descriptors such as WADL and/or WSDL. The following are the referred schema file:

* `WADL schema <https://www.w3.org/Submission/wadl/wadl.xsd>`_ (for REST)
* `WSDL schema <http://schemas.xmlsoap.org/wsdl/>`_ (for SOAP)

Validate RDF
------------
Returns a validation message and possible issues about mandatory or recommended fields.

* **Endpoint**: /mkp/validate
* **Method**: POST
* **Consumes**: application/json
* **Produces**: application/json

**Input**

.. code-block:: json

	{
		"resourcerdf": "string"
	}

The ``resourcerdf`` input parameter can contain both an url to a remote RDF file or a resource context name within the WeLive platform Sesame repository.

Validate Service Descriptor
---------------------------
Validate a Web Service descriptor file by its URL.

* **Endpoint**: /mkp/validateService
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: application/json
* **Produces**: application/json

**Input schema**

.. code-block:: json

	{
		"url": "string",
		"type": "string"
	}

where ``url`` is the URL of the WADL/WSDL and ``type`` can be **rest** or **soap**.

**Output schema**

.. code-block:: json

	{
		"valid": true,
		"code": "string",
		"message": "string"
	}

where ``code`` can be:

* valid-wadl or vc-valid-wsdl
* error-internal
* error-retrieving-wadl or vc-error-retrieving-wsdl
* error-validating-wadl or vc-error-validating-wsdl

and ``message`` contains additional information about the operation result.

Example
^^^^^^^

**Input**

.. code-block:: json

	{
		"url": "http://mycityportal.com/services/events.wadl",
		"type": "rest"
	}

**Output**

.. code-block:: json

	{
		"valid":false,
		"code":"error-validating-wadl",
		"message":"[Error] line: 5; column: 45; cause: cvc-id.2: There are multiple occurrences of ID value 'Events'."
	}
