Get artefacts
=============
Following methods allow to retrieve a list of artefacts published in the WeLive Marketplace.

Get Artefact By ID
------------------
Returns the single artefact identified by ``artefact-id``.

* **Endpoint**: /mkp/get-artefact/artefactid/{artefact-id}
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

Get Artefacts By IDs
--------------------
Returns the list of artefacts identified by the given ``ids-list`` (comma-separated).

* **Endpoint**: /mkp/get-artefacts-by-ids/ids/{ids-list}
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

Get Artefacts By Keywords
-------------------------
Returns a list of artefacts that match the given ``keywords`` (comma-separated).

* **Endpoint**: /mkp/get-artefacts-by-keywords/keywords/{keywords}/pilot-id/{pilot-id}/artefact-types/{artefact-types}
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Input**

* **pilot-id**: All, Bilbao, Novisad, Trento, Uusimaa;
* **artefact-types**: All, BuildingBlock, Dataset, PSA.

Get All Artefacts
-----------------
Returns all the artefacts filtered by ``pilot-id`` and/or ``artefact-types`` (comma-separated).

* **Endpoint**: /mkp/get-all-artefacts/pilot-id/{pilot-id}/artefact-types/{artefact-types}
* **Method**: GET
* **Authorization Header**: Basic
* **Produces**: application/json

**Input**

* **pilot-id**: All, Bilbao, Novisad, Trento, Uusimaa;
* **artefact-types**: All, BuildingBlock, Dataset, PSA.

Output
------
Previous methods returns a JSON with the following schema:

.. code-block:: json

  {
	  "artefacts": [
	    {
	      "artefactId": 0,
	      "name": "string",
	      "description": "string",
	      "interfaceOperation": "string",
	      "type": "string",
	      "typeId": 0,
	      "rating": 0,
	      "url": "string",
	      "tags": [
	        "string"
	      ],
	      "comments": [
	        {
	          "text": "string",
	          "author": "string",
	          "creation_date": "2016-05-16T12:52:07.181Z"
	        }
	      ],
	      "eid": 0
	    }
	  ]
	}
