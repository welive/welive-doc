﻿.. _weliveplayer:

WeLivePlayer API guide
======================

The WeLivePlayer acts as intermediary between weliveplayer mobile client and server APIs. The application provide control over the
data required by client. It makes additional REST calls in PRE and POST stages of operations to match requirement and to ensure data
consistency. It help improves overall security by validating access token from  client and at the same time improves the performance
of access by maintaining internal cache.

WeLivePlayer exposes a set of APIs methods:

+ Get Applications By City and Type.
+ Read User Profile Using Acess Token.
+ Get Single Application Comments.

REST API
--------

Get Applications By City and Type
---------------------------------

This operation returns list of Applications for requested city and type after validating access token.

**Permission**

**Parameters**

+ pilotId (path param) : determines the region/city of interest [mandatory | STRING] (for e.g. /Trento, /Bilbao, /Novisad, /All)
+ appType (path param) : Indicates the type of applications [mandatory | STRING] (for e.g. /BuildingBlock, /Dataset, /All)
+ start (query param) : indicates the page number [optional | INTEGER]
+ count (query param) : indicates number of applications per page [optional | INTEGER]

**Request**::

  GET dev/api/weliveplayer/api/apps/{pilotId}/{appType} HTTPS/1.1
  Host: dev.welive.eu
  Authorization: Bearer <ACCESS_TOKEN>

**Response**

.. code-block:: json

  {
	"data": [{
		"id": "100",
		"description": "Traffic in City fo Novi Sad",
		"linkImage": null,
		"eId": "",
		"name": "Traffic",
		"city": "All",
		"interfaceOperation": "http://services.nsinfo.co.rs/WeLive/odata/Traffic",
		"rating": 0,
		"type": "RESTful Web Service",
		"typeId": 4,
		"comments": [],
		"recommendation": null,
		"tags": null
	},
	{
		"id": "62",
		"description": "This dataset allows the consumer to have a complete view about the scheduling of the street washing day by day in the Trento city.",
		"linkImage": "https://dev.welive.eu/documents/10181/ba79e932-9b3a-4134-b0e6-b112d3660e07",
		"eId": "",
		"name": "StreetWashing",
		"city": "All",
		"interfaceOperation": "http://www.testservice.com/testservice/services/testservice?wsdl",
		"rating": 3,
		"type": "Dataset",
		"typeId": 101,
		"comments": [{
			"comment": "Very useful!",
			"authorNode": "83",
			"publishDate": "2015-10-20 18:30:24.609",
			"rating": 0
		}],
		"recommendation": null,
		"tags": null
	}],
	"errorMessage": null,
	"errorCode": 0
   }

SampleCall: https://dev.welive.eu/dev/api/weliveplayer/api/apps/Bilbao/PSA?start=0&count=20



Read User Profile Using Acess Token
-----------------------------------

This operation returns associated user profile after validating access token.

**Permission**

**Parameters**

+ artifactId (path param) : indicates the id of application [mandatory | STRING]
+ start (query param) : indicates the page number [optional | INTEGER].
+ count (query param) : indicates number of applications per page [optional | INTEGER].

**Request**::

  GET /dev/api/weliveplayer/api/userProfile
  Host: dev.welive.eu
  Authorization: Bearer <ACCESS_TOKEN>

**Response**

.. code-block:: json

  {
    "data": [
        {
            "ccUserID": "0",
			"name": "WeLive",
			"surname": "Tools",
			"gender": "Male",
			"birthdate": "null",
			"address": "Italy",
			"city": "Palermo",
			"country": "Italy",
			"zipCode": "null",
			"email": "welive@welive.eu",
			"languages": [
				"Italian",
				"Spanish",
				"Serbian",
				"Finnish"
			],
			"skills": [],
			"usedApps": [],
			"profileData": {
				"numCreatedIdeas": 0,
				"reputationScore": 0,
				"numCollaborationsInIdeas": 0
			},
			"lastKnownLocation": {
			"lng": null,
			"lat": null
			},
			"thirdParties": [],
			"developer": true
		}
    ],
    "errorMessage": null,
    "errorCode": 0
   }

SampleCall: https://dev.welive.eu/dev/api/weliveplayer/api/userProfile

Update User Profile Using Acess Token
-------------------------------------

This operation updates profile of user corresponding to the access token. The server authenticates post body against user profile before making an update on profile data.

**Permission**

**Parameters**

**Request**::

  POST /dev/api/weliveplayer/api/update/userProfile
  Host: dev.welive.eu
  Authorization: Bearer <ACCESS_TOKEN>
  Body:
  {
	"ccUserID": "29",
	"birthdate": "1989-04-25",
	"address": "Street XYZ",
	"city": "TRENTO",
	"country": "IT",
	"zipCode": "00001",
	"referredPilot": "Trento",
	"languages": ["English"],
	"userTags": ["Tag1"],
	"developer": true
  }

**Response**

.. code-block:: json

  {
    "data": [
        {
            "ccUserID": "0",
			"name": "WeLive",
			"surname": "Tools",
			"gender": "Male",
			"birthdate": "null",
			"address": "Italy",
			"city": "Palermo",
			"country": "Italy",
			"zipCode": "null",
			"email": "welive@welive.eu",
			"languages": [
				"Italian",
				"Spanish",
				"Serbian",
				"Finnish"
			],
			"skills": [],
			"usedApps": [],
			"profileData": {
				"numCreatedIdeas": 0,
				"reputationScore": 0,
				"numCollaborationsInIdeas": 0
			},
			"lastKnownLocation": {
			"lng": null,
			"lat": null
			},
			"thirdParties": [],
			"developer": true
		}
    ],
    "errorMessage": null,
    "errorCode": 0
   }



Get Single Application Comments
-------------------------------

This operation returns list of comments for requested application after validating access token.

**Permission**

**Parameters**

**Request**::

  GET /dev/api/weliveplayer/api/appComments/62 HTTPS/1.1
  Host: dev.welive.eu
  Authorization: Bearer <ACCESS_TOKEN>

**Response**

.. code-block:: json

  {
    "data": [
        {
            "comment": "Very useful!",
            "authorNode": "83",
            "publishDate": "Tue Oct 20 18:30:24 GMT 2015",
            "rating": 0
        }
    ],
    "errorMessage": null,
    "errorCode": 0
   }

SampleCall: https://dev.welive.eu/dev/api/weliveplayer/api/appComments/1418?start=0&count=20
