API guide
=========

The Welive API offers a centralized way for accessing all functionalities from the Welive platform in a RESTful way.
The documentation for this API is classified by each different component of the Welive platform.

.. toctree::

		de/welive-de
		ods/welive-ods
		logging/api-reference.rst
		aac/api-reference
		cdv/welive-cdv
		lum/welive-lum
		mkp/welive-mkp
		oia/welive-oia
		vc/welive-vc
		weliveplayer/weliveplayer.rst
		swagger/swagger-example.rst
		reference/api-reference.rst
