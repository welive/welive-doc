.. _AAC_API:

AAC API guide
=============

AAC exposes a set of APIs used in different phases of the authentication process:

+ Token generation (within different flows)
+ Token verification
+ User login profile

REST API
--------

Token generation
----------------

Access the token data within the client credentials flow and within the authorization code flow.

**Permission**


**Parameters**

+ grant_type (query param) : determines the authorization flow used. Possible values are authorization_code or client_credentials [mandatory | STRING]. 
+ client_id (query param) : Indicates the client that is making the request. The value passed in this parameter must exactly match the value shown in the console [mandatory | STRING]. 
+ client_secret (query param) : The client secret obtained during application registration [mandatory | STRING]. 
+ redirect_uri (query param) : Determines where the response is sent. Required in case of authorization code flow. The value of this parameter must exactly match one of the values registered in the APIs Console (including the http or https schemes, case, and trailing '/') [mandatory | STRING].
+ code (query param) : The (URL encoded) authorization code returned from the initial request. Ignored in case of client_credentials flow [mandatory | STRING].

**Request**::

  POST /aac/oauth/token HTTPS/1.1
  Host: dev.welive.eu
  client_id=23123121sdsdfasdf3242&client_secret=3rwrwsdgs4sergfdsgfsaf&code=1234567&redirect_uri=http://www.example.com/protected&grant_type=authorization_code

**Response**

.. code-block:: json

  {
    "access_token": "025a90d4-d4dd-4d90-8354-779415c0c6d8",
    "token_type": "bearer",
    "refresh_token": "618415da-dcfc-48b6-b775-7d0f04a71105",
    "expires_in": 38937,
    "scope": "profile.basicprofile.me"
  }


Token validation
----------------

Check if a token is valid for a specified scope.

**Permission**


**Parameters**

+ scope (query param) : A scope  [mandatory | STRING].

**Request**::

  GET /aac/resources/access HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer 025a90d4-d4dd-4d90-8354-779415c0c6d8

**Response**::

  true

Check Token
-----------

Implemented in accordance with Spring Security RemoteTokenService for OAuth, returns resource information for e.g authorities, scopes etc, that are associated with access token.

**Permission**

**Parameters**

+ token (query param) : Acess token [mandatory | STRING] 

**Request**::

  GET /aac/eauth/check_token HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json

**Response**

.. code-block:: json

  {
	"exp": 1489451452,
	"user_name": "90",
	"scope": ["profile.basicprofile.me"],
	"authorities": ["ROLE_USER"],
	"aud": ["2"],
	"client_id": "15c840c1-f00d-4fdf-8ccd-f94342f87e47"
  }
	
Read current user profile
-------------------------

Access the basic profile of the current user.

**Permission**

profile.basicprofile.me

**Parameters**

**Request**::

  GET /aac/basicprofile/me HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {user access token}

**Response**

.. code-block:: json

  {
    "name": "Mario",
    "surname": "Rossi",
    "socialId": "12345",
    "userId": "6789"
  }
    
    
Read current user account profile
---------------------------------

Access the account profile of the current user.

**Permission**

profile.accountprofile.me

**Parameters**

**Request**::

  GET /aac/accountprofile/me HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {user access token}

**Response**

.. code-block:: json

  {
    "accounts": {
        "fbk": {
        "eppn": "rossi@fbk.eu",
        "eu.trentorise.smartcampus.givenname": "Mario",
      "eu.trentorise.smartcampus.surname": "Rossi"
      }
    }
  }
    
    
Read account profile of a set of users
--------------------------------------

Access the account profiles of the specified users.

**Permission**

profile.accountprofile.all

**Parameters**

+ userIds (query param) : list of user IDs [mandatory | STRING].

**Request**::

  GET /aac/accountprofile/profiles?userIds=1,2 HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client access token}

**Response**

.. code-block:: json

  {
  "profiles":[
	"accounts":{
		"fbk":{
		"eppn": "rossi@fbk.eu",
		"eu.trentorise.smartcampus.givenname": "Mario",
		"eu.trentorise.smartcampus.surname": "Rossi"
		}
	}
	]
  }
    

Read specific user profile
--------------------------

Access the basic profile of the specified user. The user ID is passed as the path element.

**Permission**

profile.basicprofile.all

**Parameters**

+ userId (path variable) : a user Id [mandatory | STRING].

**Request**::

  GET /aac/basicprofile/all/{userId} HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}

**Response**

.. code-block:: json

    {
      "name": "Mario",
      "surname": "Rossi",
      "socialId": "12345",
      "userId": "6789"
    }
    
    
Read profile of a set of users
------------------------------

Access the basic profiles of the specified users.

**Permission**

profile.basicprofile.all

**Parameters**

+ userIds (query param) : list of user IDs [mandatory | STRING].

**Request**::

  GET /aac/basicprofile/profiles?userIds=1,2 HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}

**Response**

.. code-block:: json

    {
     "profiles": [
        {
           "name": "Mario",
           "surname": "Rossi",
           "socialId": "1",
           "userId": "1"
        },
        {
           "name": "Mario",
           "surname": "Rossi",
           "socialId": "2",
           "userId": "2"
        },
        {
           "name": "Mario",
           "surname": "Rossi",
           "socialId": "3",
           "userId": "3"
        }
      ]
    }    
    
    
Read profile of users with name filter
--------------------------------------

Access the basic profiles of the users whose name/surname matches the specified string (i.e., name/surname contains the string).

**Permission**

profile.basicprofile.all

**Parameters**

+ filter (query param) : case-insensitive substring to be searched. If not specified, all users returned [optional | STRING].

**Request**::

  GET /aac/basicprofile/all?filter=mario HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}

**Response**

.. code-block:: json

    {
     "profiles": [
        {
           "name": "Mario",
           "surname": "Rossi",
           "socialId": "1",
           "userId": "1"
        },
        {
           "name": "Mario",
           "surname": "Rossi",
           "socialId": "2",
           "userId": "2"
        },
        {
           "name": "Mario",
           "surname": "Rossi",
           "socialId": "3",
           "userId": "3"
        }
      ]
    }

Read client information
-----------------------

Get information about the client handling the specified token.

**Permission**

profile.basicprofile.all

**Request**::

  GET /aac/resources/clientinfo HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}

**Response**

.. code-block:: json

  {
    "clientId": "1783734e-8545-4aee-be0e-23523c46ce80",
    "clientName": "App1"
  }


Read all client information associated to specified token
---------------------------------------------------------

Get all client information handling the specified token.

**Permission**

profile.basicprofile.all

**Request**::

  GET /aac/resources/clientinfo/oauth HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}

**Response**

.. code-block:: json

  {
    "clientId": "1783734e-8545-4aee-be0e-23523c46ce80",
    "clientName": "App1"
  }
	

Read all client information associated to user id
-------------------------------------------------

Get all client information handling the specified user id.

**Permission**

profile.basicprofile.all

**Parameters**

+ userId (path variable) : a user Id [mandatory | STRING].

**Request**::

  GET /aac/resources/clientinfo/oauth/{userId} HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Basic {client or user access token}

**Response**

.. code-block:: json

  {
    "clientId": "1783734e-8545-4aee-be0e-23523c46ce80",
    "clientName": "App1"
  }


Revoke authorization token
--------------------------

Revoke the access token and the associated refresh token

**Permission**

profile.basicprofile.all

**Parameters**

+ token (path variable) : an access token [mandatory | STRING].

**Request**::

  DELETE /aac/eauth/revoke/{token} HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}

**Response**

200 OK


Revoke authorization token provided user id and client id
---------------------------------------------------------

Revoke the access token against user id and client id.

**Permission**

profile.basicprofile.all

**Parameters**

+ userId (path variable) : a user Id [mandatory | STRING].
+ clientId (path variable) : an access token [mandatory | STRING].

**Request**::

  DELETE /aac/eauth/revoke/{clientId}/{userId} HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Basic {client or user access token}

**Response**

200 OK

Create User
-----------
Create a new user.

**Request**::

  POST /aac/extuser/create
  Host: dev.welive.eu
  Authorization: Bearer <CLIENT_ACCESS_TOKEN> or Basic <TOKEN>
  Body: 
  {
  "username": "string",
  "name": "string",
  "surname": "string",
  "email": "string"
  }

**Response**
+ HTTP 200 with no content.
+ HTTP 500 in case of error.

Create Client Specification
---------------------------
Create client specification for user

**Permission**

**Parameters**

**Request**::

  POST /aac/resources/clientspec HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}
  Body:
  {
	"clientId": "string",
	"clientSecret": "string",
	"clientSecretMobile": "string",
	"name": "string",
	"redirectUris": [
		"string"
	],
	"grantedTypes": [
		"string"
	],
	"nativeAppsAccess": false,
	"nativeAppSignatures": "string",
	"browserAccess": false,
	"serverSideAccess": false,
	"identityProviders": [
		"string"
	],
	"scopes": [
		"string"
	],
	"ownParameters": [
		{
		"name": "string",
		"value": "string",
		"service": "string",
		"visibility": "CLIENT_APP"
		}
	]
  }

**Response**

.. code-block:: json

  {
	"clientId": "15c840c1-f00d-4fdf-8ccd-f94342f87e47",
	"clientSecret": "30840262-4469-4c5b-b5f0-670c638b9ffd",
	"clientSecretMobile": "da834f50-8c75-45b1-97a2-d3fdff142f82",
	"name": "test1",
	"redirectUris": ["http://localhost"],
	"sloUrl": null,
	"grantedTypes": ["implicit", "client_credentials"],
	"nativeAppsAccess": false,
	"nativeAppSignatures": null,
	"browserAccess": true,
	"serverSideAccess": false,
	"identityProviders": ["google", "facebook"],
	"scopes": ["profile.basicprofile.me"],
	"ownParameters": []
 }

+ HTTP 500 in case of error.

Read Client Specification
-------------------------
Read client specification for user

**Permission**

**Parameters**

**Request**::

  GET /aac/resources/clientspec HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}

**Response**

.. code-block:: json

  {
	"clientId": "15c840c1-f00d-4fdf-8ccd-f94342f87e47",
	"clientSecret": "30840262-4469-4c5b-b5f0-670c638b9ffd",
	"clientSecretMobile": "da834f50-8c75-45b1-97a2-d3fdff142f82",
	"name": "test1",
	"redirectUris": ["http://localhost"],
	"sloUrl": null,
	"grantedTypes": ["implicit", "client_credentials"],
	"nativeAppsAccess": false,
	"nativeAppSignatures": null,
	"browserAccess": true,
	"serverSideAccess": false,
	"identityProviders": ["google", "facebook"],
	"scopes": ["profile.basicprofile.me"],
	"ownParameters": []
 }

+ HTTP 500 in case of error.

Update Client Specification
---------------------------
Update client specification for user

**Permission**

**Parameters**

**Request**::

  PUT /aac/resources/clientspec HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}
  Body:
  {
	"clientId": "string",
	"clientSecret": "string",
	"clientSecretMobile": "string",
	"name": "string",
	"redirectUris": [
		"string"
	],
	"grantedTypes": [
		"string"
	],
	"nativeAppsAccess": false,
	"nativeAppSignatures": "string",
	"browserAccess": false,
	"serverSideAccess": false,
	"identityProviders": [
		"string"
	],
	"scopes": [
		"string"
	],
	"ownParameters": [
		{
		"name": "string",
		"value": "string",
		"service": "string",
		"visibility": "CLIENT_APP"
		}
	]
  }

**Response**

.. code-block:: json

  {
	"clientId": "15c840c1-f00d-4fdf-8ccd-f94342f87e47",
	"clientSecret": "30840262-4469-4c5b-b5f0-670c638b9ffd",
	"clientSecretMobile": "da834f50-8c75-45b1-97a2-d3fdff142f82",
	"name": "test1",
	"redirectUris": ["http://localhost"],
	"sloUrl": null,
	"grantedTypes": ["implicit", "client_credentials"],
	"nativeAppsAccess": false,
	"nativeAppSignatures": null,
	"browserAccess": true,
	"serverSideAccess": false,
	"identityProviders": ["google", "facebook"],
	"scopes": ["profile.basicprofile.me"],
	"ownParameters": []
  }

+ HTTP 500 in case of error.

Read Service Permissions
------------------------
Read service permissions.

**Permission**

**Parameters**

**Request**::

  GET /aac/resources/permissions HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  
**Response**

.. code-block:: json

  {
	"permissions": [
	{
	"name": "Basic profile service",
	"description": "Core AAC service for managing basic user profiles.",
	"scopes": [
	{
	"id": "profile.basicprofile.me",
	"description": "Basic profile of the current platform user. Read access only.",
	"access_type": "U"
	},
	{
	"id": "profile.basicprofile.all",
	"description": "Basic profile of the platform users. Read access only.",
	"access_type": "UC"
	},
	{
	"id": "profile.accountprofile.me",
	"description": "Account profile of the current platform user. Read access only.",
	"access_type": "U"
	},
	{
	"id": "profile.accountprofile.all",
	"description": "Account profile of the platform users. Read access only.",
	"access_type": "C"
	}
	]
	},
	{
	"name": "CDV Basic profile service",
	"description": "CDV service for managing user profiles.",
	"scopes": [
	{
	"id": "cdv.profile.me",
	"description": "Profile of the current platform user. Read access only.",
	"access_type": "U"
	}
	]
	},
	{
	"name": "Decision Engine",
	"description": "Service for Decision Engine",
	"scopes": [
	{
	"id": "de.artifact.add",
	"description": "This mapping allows adding artifacts to authorized clientes",
	"access_type": "C"
	}
	]
	}
	]
  }

+ HTTP 500 in case of error.

Delete User Using OAuth
-----------------------
Delete user using OAuth.

**Permission**

**Parameters**

**Request**::

  DELETE /aac/user/me HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}

**Response**

200 OK

Delete User Using Basic Auth
----------------------------
Revoke user using user id.

**Permission**

**Parameters**

+ ccUserId (path variable) : a user Id [mandatory | STRING].

**Request**::

  DELETE /aac/user/{ccUserId} HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Basic {client or user access token}

**Response**

200 OK