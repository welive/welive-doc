User and Workgroup Creation
===========================
The following method allow the Liferay User Manager and the Open Innovation Area to communicate with the Visual Composer.

Register User
-------------
Creates a new user into the CDV.

* **Endpoint**: /vc/registeruser
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: text/plain
* **Produces**: text/plain

**Input schema**

.. code-block:: json

  {
		"ccUserID": "string",
		"pilotID": "string",
		"firstName": "string",
		"lastName": "string",
		"email": "string"
  }

.. note::
	All fields are mandatory.

Create Workgroup
----------------
Creates a new workgroup into VC. If the ``ideaid`` is already associated with a workgroup, the VC updates the list of members.

* **Endpoint**: /vc/ideaworkgroup
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: text/plain
* **Produces**: text/plain

**Input schema**

.. code-block:: json

  {
    "ideaid": 0,
    "ideaname": "string",
    "authority": "string",
    "members":
    [
      {
        "username": "string"
      }
    ]
  }

where:

* ``ideaid`` is the OIA idea ID;
* ``ideaname`` is the name of the idea;
* ``authority`` is the email of the authority;
* ``members`` is the list of workgroup members and ``username`` is the email of the member.

.. note::
	All fields are mandatory.

Output
------
Previous methods return a string with the following schema:

.. code-block:: json

  {
    "type": 0,
    "message": 0
  }
