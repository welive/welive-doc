Building Block Validation
=========================
The following method allow you to validate a Web Service descriptor file according to the Visual Composer specification. The following are the referred schema file:

* `WADL schema <https://www.w3.org/Submission/wadl/wadl.xsd>`_ (for REST)
* `WSDL schema <http://schemas.xmlsoap.org/wsdl/>`_ (for SOAP)

Validate Service
----------------
Validate a Web Service descriptor file by its URL.

* **Endpoint**: /visualcomposer/webservice/validateService
* **Method**: POST
* **Authorization Header**: Basic
* **Consumes**: application/json
* **Produces**: application/json

**Input schema**

.. code-block:: json

	{
		"url": "string",
		"type": "string"
	}

where ``url`` is the URL of the WADL/WSDL and ``type`` can be **rest** or **soap**.

**Output schema**

.. code-block:: json

	{
		"valid": true,
		"code": "string",
		"message": "string"
	}

where ``code`` can be:

* vc-valid-wadl or vc-valid-wsdl
* vc-error-internal
* vc-error-retrieving-wadl or vc-error-retrieving-wsdl
* vc-error-validating-wadl or vc-error-validating-wsdl

and ``message`` contains additional information about the operation result.

Example
^^^^^^^

**Input**

.. code-block:: json

	{
		"url": "http://mycityportal.com/services/events.wadl",
		"type": "rest"
	}

**Output**

.. code-block:: json

	{
		"valid":false,
		"code":"vc-error-validating-wadl",
		"message":"[Error] line: 5; column: 45; cause: cvc-id.2: There are multiple occurrences of ID value 'Events'."
	}
