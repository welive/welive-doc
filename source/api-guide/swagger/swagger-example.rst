Swagger interface
=================

In addition to the typical API calls, Welive's API provides a `Swagger interface <https://dev.welive.eu/dev/swagger>`_. Through this interface, the different API calls can be tested easily. Next, two examples on using the Swagger interfaces are described.

.. _swagger-authentication:

Authentication into swagger interface
-------------------------------------

For calling some API methods, an authenticated user is needed. WeLive's Swagger interface offers this functionality for allowing making these calls through the web interface. The Log in button placed at the upper right corner starts the log in process:

.. image:: ../images/swagger-login.png

This button redirects to the Authentication and Authorization Component:

.. image:: ../images/swagger-aac.png

Once the user is authenticated, the workflow returns to the Swagger interface, showing the expiration time of the token:

.. image:: ../images/swagger-expiration.png

While the token remains active, the user can make API calls to methods that require authentication.

Mapping creation
----------------

In this example, the creation of a mapping through a POST call to the method **/ods/dataset/{datasetID}/resource/{resourceID}/mapping** is explained. As can be seen in the next image, two path parameters (datasetID and resourceID) and a JSON object in the body of the request are required. The IDs of datasets can be retrieved through the method **/ods/dataset/all**, and the IDs of the resources of a dataset can be retrieved through the method **/ods/dataset/{datasetID}**. The specification of the mapping is detailed in :ref:`mappings`. In the following image, the usage of the Swagger interface can be seen.

.. image:: ../images/mapping-example.png

An the response:

.. image:: ../images/mapping-result.png


Resource creation
-----------------

.. note::
  At the time of writing this documentation, a bug was discovered in Swagger that didn't allow uploading both files and text together with ``multipart/form-data`` content type. While this bug is solved, we offer an example of submitting files through the application `Àdvanced REST Client <https://chrome.google.com/webstore/detail/advanced-rest-client/hgmloofddffdnphfgcellkdfbfbjeloo?utm_source=chrome-ntp-icon>`_ for Google Chrome.

For creating a resource, the method **/ods/dataset/{datasetID}/resource** is provided. This method requires the ID of the dataset in which is going to be created in the path of the URL, a form with three fields in its body (packageDict, upload and fileInfo), and the Authorization header, as can be seen in the following image:

.. image:: ../images/resource-example.png
      :scale: 20%

In the "File" tab, the file to be submitted can be selected:

.. image:: ../images/resource-file.png
      :scale: 20%

At last, the results given from the call can be seen:

.. image:: ../images/resource-result.png
      :scale: 20%

In this case, the client fills the fileInfo JSON automatically. If you are using a client that requires the explicit declaration of this field, the structure of the JSON is the following one:

.. code-block:: json

  {
    "name": "fileName.csv",
    "modificationDate": "2015-10-20T07:42:10.184Z",
    "readDate": "2015-10-20T07:42:10.184Z",
    "creationDate": "2015-10-20T07:42:10.184Z",
    "type": "string",
    "size": 0,
    "fileName": "fileName.csv"
  }

Only the fields ``name`` and ``fileName`` are mandatory.

..
  For creating a resource, the method **/ods/dataset/{datasetID}/resource** is provided. This method requires the ID of the dataset in which is going to be created in the path of the URL, a form with three fields in its body (packageDict, upload and fileInfo), and the Authorization header, as can be seen in the following image:

  .. image:: ../images/swagger-example.png

  The **datasetID** could be retrieved through an API call to **/ods/dataset/all**. The JSON dictionary for **packageDict** is described at :ref:`resource-operations`. At the **upload** field, the resource file to be uploaded is inserted. At last, at the **fileInfo** field, the information about the uploaded file is described. Regarding to the Authorization header, this fields requires the API KEY of the user from the ODS. In the following image, an example of a resource creations can be seen:
