﻿7. SINGLE LOGOUT
================

Single Logout (SLO) protocol mechanism is implemented to ensure logout integrity across all platforms. The idea evolved to tackle the disparity between log out actions being carried out at different levels. Web applications and AAC use session cookies to identify the user “login” state. Logout is performed either locally (i.e., on the component’s side) or locally + AAC (component-side and on AAC), but not across all the components involved.
As a result, when a user is signed in components A and B 
-	there exists session cookie of the component A, B, and AAC.
-	Logout locally from A invalidates the session of A, but not B and not AAC. The user can still work with component B and perform SSO with AAC.
-	Logout locally + AAC invalidates the session of A and AAC, but not B. The user can still work with the component B
With SLO in place, it offers mechanism for transparently signing out from all the components currently active for the user. In case user is signed in components A and B, the single logout will terminate sessions of A, B, and AAC.

6.1 PROTOCOL
------------
In the context of WeLive project, the implemented single logout mechanism is inspired with SAML Single Logout Profile. It uses asynchronous HTTP redirect binding and is compliant with CAS protocol SLO front-channel implementation

6.1.1. Sign In Phase
^^^^^^^^^^^^^^^^^^^^
Upon request, AAC adds the authenticating component to the user session storage. The component’s logout URL is stored.

6.1.2. Logout Phase
^^^^^^^^^^^^^^^^^^^
User performs a global logout operation

- A component makes a request to the AAC SLO endpoint specifying the redirect URL to be used upon completion::
	
	/cas/logout?service=<final-redirect-url>


- AAC check the list of SSO components. If it is not empty, it takes the logout URL of one of the component, and issues a logout request via HTTP redirect with the RelayState key associated to the component and the encoded and compressed logout request XML message (see nex slide)::
	
	HTTP 302  <componentLogoutURL>?SAMLRequest=<request-message>&RelayState=<state-key>


- Component handles the request, invalidating (if not yet done) the session and sending logout response back to AAC with the same RelayState via HTTP redirect::
	
	HTTP 302 /cas/logout?RelayState=<state-key>


- AAC removes the component from the list of SSO components and proceeds with step 3

- If the list is empty, the AAC invalidates the current session and redirect to the URL specified in the initial request::
	
	HTTP 302 <final-redirect-url>


.. image:: images/6.1.1.png

The SLO protocol relies on the Logout URL of the components. In case of CAS protocol, the logout URL is taken from the “service” parameter provided on CAS signin. For CAS-based clients, the implementation of the logout is mandatory.
In case of OAuth2 protocol, the logout URL of the component has to be configured through the AAC Developer console. The implementation of the logout is optional. If not implemented, the SLO flow does not apply to the component.



