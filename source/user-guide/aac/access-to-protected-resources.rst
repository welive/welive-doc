﻿3. ACCESS TO PROTECTED RESOURCES
================================

When an external app besides the authentication requires also the access to the protected WeLive building blocks on behalf of the user, the CAS protocol is not sufficient, as the BB access requires that the OAuth2.0 token is provided and the token is associated with the required permissions. According to the OAuth2.0 information flow, the authenticated user is asked to grant those permissions to the application and after that the corresponding token is emitted.

To accomplish the OAuth2.0 flow, the application that will access the protected BBs and resources should be explicitly registered in the AAC.

3.1. REGISTERING THE CLIENT APP
-------------------------------

The registration of the client apps is performed through the AAC Developer Console::

	https://dev.welive.eu/aac/dev

Before accessing the console, the developer should be authenticated. Once signed in, the user can see the empty console where it is possible to register an app:

.. image:: images/1.1.1.png

To create an app, it is enough to select the application name. Note that the application name should be unique, otherwise the application will not be created. A newly created app is provided with the set of generated attributes (client_id, client_secret, client_secret_mobile) present on the overview tab. On the settings page it is necessary to select which methods will be used by the client for user authorization.

Specifically, it is necessary to select

- **server-side access** checkbox and a valid redirect Web Server URL in case of authorization code flow;
- **browser access** checkbox and a valid redirect Web Server URL in case of implicit flow;
- **native app** access in case of native mobile apps.

It is necessary to define the set (comma-separated) of the callback URL, to which the authorization result will be redirected. 
It is also necessary to define which identity providers may be used for the user authentication within the app (e.g., WeLive users and or the users having Google account).

.. image:: images/1.1.2.png

Once the client is configured, it is necessary to define which scopes the client will target. That is, what is the maximum set of permissions that the app may require from the user.

In AAC, the permissions are grouped into “services” that form logically related sets of operation and resource permissions. These model of permissions is extendible, the new permissions and services may be added for the purpose of the more accurate and controlled resource access.

An important aspect is that the resources are divided into the ones owned by (or available to) the user (i.e., user resources) and those that are available to an app without involving a user (i.e., client resources). For example, the user files are of the first type, while the file storage configuration resource is of the second type.
It is also important to know that some of the resources are not immediately available to the application once registered. Their access may be subject of the external approval by the data providers through some additional procedure. Once such a procedure is accomplished, the administrator will unblock the access to the resource by the application.

As it is shown in the following picture, for the services of interest of a specific application, the app developer should select the required resources and save the model. The user-related scopes are marked with the ‘U’ label, the non-related resources are marked with ‘C’, while the resources and operations available both to a single user or to the application without user grants are marked with ‘*’ label.

.. image:: images/1.1.3.png

Once the client app is registered and configured, it is possible to perform authorization and access the services. To facilitate the testing phase, the developer console provides means to generate both the user and the client access tokens: in the 'Overview' tab the 'Get client credentials flow token' link allows for generating the client access token and the 'Get implicit flow token' link allows for generating the user access token associated to the developer's account.

.. image:: images/1.1.4.png

3.2. OAUTH2.0 USER-RELATED PROTOCOL FLOW
----------------------------------------

Once the app is registered in the console, the OAuth2.0 protocol may be engaged in order to authenticate the users and to obtain the necessary tokens on their behalf. Depending on the timing and the origin of access, it is possible to involve different types of the information flow, namely the implicit flow and authorization code flow.

3.2.1. Implicit Flow
^^^^^^^^^^^^^^^^^^^^

This flow is suitable for the scenarios, where the client application (e.g., client part of a Web app or a mobile app) makes the authentication and then direct access to the API without passing through its own Web server backend. This allows for generating only a token for a short time period, so the next time the API access is required, the authentication should be performed again.
In a nutshell, the flow is realized as follows:

1. The client app, when there is a need for the token, emits an authorization request to AAC in a browser window (or in a WebView in case of mobile app)::

	https://dev.welive.eu/aac/eauth/authorize

.. image:: images/1.2.1.png

The request accepts the following set of parameters:

+-------------------------+------------------------------+------------------------------------------------------------+
|          Parameters     |         Values               |          Description                                       |
+=========================+==============================+============================================================+
| client_id               | The client_id obtained in    |Indicates the client that is making the request. The value  |
|                         | developer console            |passed in this parameter must exactly match the value       |
|                         |                              |in the console.                                             |
+-------------------------+------------------------------+------------------------------------------------------------+
| response_type           | token                        |determines if the SC OAuth 2.0 endpoint returns a token. For|
|                         |                              |client applications, a value of token should be used.       |
+-------------------------+------------------------------+------------------------------------------------------------+
| redirect_uri            |one of there direct_uri values|Determines where the response is sent. The value of this    |
|                         |registered at the developer   |parameter must exactly match one of the values registered in|
|                         |console.                      |the APIs Constole (including the http or https schemes, case|
|                         |                              |, and trailing '/').                                        |
+-------------------------+------------------------------+------------------------------------------------------------+
| scope                   |space delimited set of        |Indicates the access your application is requesting. The    |
|                         |permissions the application   |value passed in this parameter inform the consent page shown|
|                         |requests                      |to the user. There is an inverse relationship between the   |
|                         |                              |number of permissions requested and the likelihood of       |
|                         |                              |obtaining user consent.                                     |
+-------------------------+------------------------------+------------------------------------------------------------+
| state (optional)        | any string                   |Indicates any state which may be useful to your application |
|                         |                              |upon receipt of the response. The Authorization Server      |
|                         |                              |roundtrips this parameter, so your application receives the |
|                         |                              |same value it sent.                                         |
+-------------------------+------------------------------+------------------------------------------------------------+

2. AAC redirects the user to the authentication page, where the user selects one of the identity providers to perform the sign in.

3. Once authenticated, AAC asks the user whether the permissions for the requested operations may be granted. 

4. If the user accepts, the browser is redirected to the specified redirect URL, attaching the token data in the url hash part. Note that this part is available client side only; it is not accessible server-side::

	http://www.example.com#access_token=025a90d4-d4dd-4d90-8354-779415c0c6d8&token_type=Bearer&expires_in=3600

5. The obtained token data may be used on the client to perform the requests. For instance, in order to obtain the basic profile of the user managed by the AAC, the following request may be emitted::

	GET /dev/api/aac/basicprofile/me HTTPS/1.1
	Host: dev.welive.eu
	Accept: application/json
	Authorization: Bearer 025a90d4-d4dd-4d90-8354-779415c0c6d8

3.2.2. Authorization Code Flow
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This flow is used when there is a need to access protected resources, without requesting the user to authenticate again and again, even when the initially emitted access token has already been expired. This scenario corresponds to

- the access the resources in a server-to-server modality when the user is offline
- mobile app that has a goal not to ask the user to authenticate each time.

For this purpose, the emitted access token is accompanied with the ‘refresh’ token, that allows to re-generate a new access token without involving the user. 

.. image:: images/1.2.2.png

This protocol flow is realized as follows:

1. The client app, when there is a need for the token, emits an authorization request to AAC in a browser window (or in a WebView in case of mobile app)::

	https://dev.welive.eu/aac/eauth/authorize

The request accepts the following set of parameters:

+-------------+---------------------------------------------+---------------------------------------------------------+
|Parameter    |     Value                                   |  Description                                            |
+=============+=============================================+=========================================================+
| client_id   |The client_id obtained in developer console  |Indicates the client that is making the request. The     |
|             |                                             |value passed in this parameter must exactly match the    |
|             |                                             |value shown in the console.                              |
+-------------+---------------------------------------------+---------------------------------------------------------+
|response_type|code                                         |determines if the SC OAuth2.0 endpoint returns an        |
|             |                                             |authorization code. For web server applications, a value |
|             |                                             |of code should be used                                   |
+-------------+---------------------------------------------+---------------------------------------------------------+
|redirect_uri |one of the redirect_urivalues registered at  |determines where the response is sent. The value of this |
|             |the developer console                        |parameter must exactly match one of the valuesregistered |
|             |                                             |in the APIs Console including the http or https schemes, |
|             |                                             |case,and trailing '/')                                   |
+-------------+---------------------------------------------+---------------------------------------------------------+
|scope        |space delimited set of permissions the       |Indicates the access your application is requesting. The |
|             |application requests                         |values passed in this parameter inform the consent page  |
|             |                                             |shown to the user. There is an inverse relationship      |
|             |                                             |between the number of permissions requested and the      |
|             |                                             |likelihood of obtaining user consent.                    |
+-------------+---------------------------------------------+---------------------------------------------------------+
|state        |any string                                   |Indicates any state which may be useful to your          |
|(optional)   |                                             |application upon receipt of the response. The            |
|             |                                             |Authorization Server roundtrips this parameter, so your  |
|             |                                             |application receives the same value it                   |
+-------------+---------------------------------------------+---------------------------------------------------------+
															 
															 
2. AAC redirects the user to the authentication page, where the user selects one of the identity providers to perform the sign in.

3. Once authenticated, AAC asks the user whether the permissions for the requested operations may be granted. 

4. If the user accepts, the browser is redirected to the specified redirect URL with the authorization code in query parameters::

	http://example.com/protected?code=1234567

5. The receiving endpoint should exchange the provided code for the token information. This request is an HTTPS post, and includes the following parameters:

+-------------------+-------------------------------------------------------------------------------------------------+
| Parameter         |   Description                                                                                   |
+===================+=================================================================================================+
|grant_type         |determines the authorization flow used. For web server applications, a value of                  |
|                   |authorization_code should be used                                                                |
+-------------------+-------------------------------------------------------------------------------------------------+
|client_id          |Indicates the client that is making the request. The value passed in this parameter must exactly |
|                   |match the value shown in the console.                                                            |
+-------------------+-------------------------------------------------------------------------------------------------+
|client_secret      |The client secret obtained during application registration                                       |
+-------------------+-------------------------------------------------------------------------------------------------+
|redirect_uri       |Determines where the response is sent. The value of this parameter must exactly match one of the |
|                   |values registered in the APIs Console (including the http or https schemes,                      |
|                   |case, and trailing '/').                                                                         |
+-------------------+-------------------------------------------------------------------------------------------------+
|code               |The authorization code returned from the initial request                                         |
+-------------------+-------------------------------------------------------------------------------------------------+

Example::

	POST /dev/api/aac/oauth/token HTTPS/1.1
	Host: dev.welive.eu
	Content-Type: application/x-www-form-urlencoded
	client_id=23123121sdsdfasdf3242&
	client_secret=3rwrwsdgs4sergfdsgfsaf&
	code=1234567&
	redirect_uri=http://www.example.com/protected&
	grant_type=authorization_code

A successful response to this request contains the following fields:

+----------------+----------------------------------------------------------------------------------------------+
|Field           | Description                                                                                  |
+================+==============================================================================================+
|access_token    | The token that can be used in resource requests                                              |
+----------------+----------------------------------------------------------------------------------------------+
|refresh_token   |A token that may be used to obtain a new access token. Refresh tokens are valid until the user|
|                |evokes access. This field is only present if is included in the authorization code request.   |
+----------------+----------------------------------------------------------------------------------------------+
|expires_in      |The remaining lifetime on the access token.                                                   |
+----------------+----------------------------------------------------------------------------------------------+
|scope           |                                                                                              |
+----------------+----------------------------------------------------------------------------------------------+

A successful response is returned as a JSON object, similar to the following::

	"access_token": "025a90d4-d4dd-4d90-8354-779415c0c6d8",
	"token_type": "bearer",
	"refresh_token": "618415da-dcfc-48b6-b775-7d0f04a71105",
	"expires_in": 38937,
	"scope": "profile.basicprofile.me"


Store the refresh token associated to the user for future uses.

6. The obtained token data may be used on the client to perform the requests. For instance, in order to obtain the basic profile of the user managed by the AAC, the following request may be emitted::

	GET /dev/api/aac/basicprofile/me HTTPS/1.1
	Host: dev.welive.eu
	Accept: application/json
	Authorization: Bearer 025a90d4-d4dd-4d90-8354-779415c0c6d8

7. When the access token is expired, it is possible to generate a new one, using the AAC token endpoint::

	https://dev.welive.eu/dev/api/aac/oauth/token

The request accepts the following set of parameters:

+-------------------+-----------------------------------------------------------------------------------------+
| Field             | Description                                                                             |
+===================+=========================================================================================+
|refresh_token      |The refresh token returned from the authorization code exchange                          |
+-------------------+-----------------------------------------------------------------------------------------+
|client_id          |The client_id obtained during application registration                                   |
+-------------------+-----------------------------------------------------------------------------------------+
|client_secret      |The client secret obtained during application registration                               |
+-------------------+-----------------------------------------------------------------------------------------+
|grant_type         |As defined in the OAuth2 specification, this field must contain a value of refresh_token |
+-------------------+-----------------------------------------------------------------------------------------+

For example::

	POST /dev/api/aac/oauth/token HTTPS/1.1
	Host: dev.welive.eu
	Content-Type: application/x-www-form-urlencoded
	client_id=23123121sdsdfasdf3242&
	client_secret=3rwrwsdgs4sergfdsgfsaf&
	code=1234567&
	refresh_token=618415da-dcfc-48b6-b775-7d0f04a71105&
	grant_type=refresh_token

3.2.3. Mobile Client Access
^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the situation, where the authentication is performed directly on the mobile app and the app requires access to the protected WeLive resources, it is possible to exploit the authorization code flow described in Authorization Code Flow. In this case, the following considerations apply:

- Use some redirect URL that can be intercepted directly on the mobile (more precisely, in the WebView of the app), such as http://localhost; 
- Implement the exchange of the code for token directly on the client;
- Implement the token refresh directly on the client;
- Use the value of client secret mobile value provided by the developer console instead of the client secret to refresh token. The client_secret_mobile may be used only for generating the refresh
  token and cannot be exploited to access the protected WeLive resources through the client credentials flow.
  
  
3.3. OAUTH2.0 NON USER-RELATED PROTOCOL FLOW
--------------------------------------------

In case the exposed protected resource is not related to the user data or actions, it is possible to engage the OAuth2.0 client credentials flow. In this flow, the access token is associated to the client app, not to a single user. 

The client credentials flow is realized as follows.

1. The client app requests the access token from AAC. This request is an HTTPS post::

	https://dev.welive.eu/dev/api/aac/oauth/token

and includes the following parameters:

+------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Parameter        |    Description                                                                                                                       |
+==================+======================================================================================================================================+
|grant_type        |determines the authorization flow used. For web server applications, a value of client_credentials should be used                     |
+------------------+--------------------------------------------------------------------------------------------------------------------------------------+
| client_id        |Indicates the client that is making the request. The value passed in this parameter must exactly match the value shown in the console.|
+------------------+--------------------------------------------------------------------------------------------------------------------------------------+
|client_secret     |The client secret obtained during application registration                                                                            |
+------------------+--------------------------------------------------------------------------------------------------------------------------------------+

Example::

	POST /dev/api/aac/oauth/token HTTPS/1.1
	Host: dev.welive.eu
	Content-Type: application/x-www-form-urlencoded
	client_id=23123121sdsdfasdf3242&
	client_secret=3rwrwsdgs4sergfdsgfsaf&
	grant_type=client_credentials

A successful response to this request contains the following fields:

A successful response is returned as a JSON object, similar to the following::

	"access_token": "025a90d4-d4dd-4d90-8354-779415c0c6d8",
	"token_type": "bearer",
	"expires_in": 38937,
	"scope": "profile.basicprofile.all"

8. The obtained token data may be used on the client to perform the requests. For instance, in order to obtain the basic profile of all the users managed by the AAC, the following request may be 

emitted::

	GET /dev/api/aac/basicprofile/all HTTPS/1.1
	Host: dev.welive.eu
	Accept: application/json
	Authorization: Bearer 025a90d4-d4dd-4d90-8354-779415c0c6d8
