﻿6. RIGHT TO FORGET
==================

The right to forget feature of AAC allows for the deletion of user stored data. The data can be resources, access token, acccount information etc. This feature is in accordance with the requirement where users have the right to erase personal data and "to be forgotten". This enables subjects to require the removal,
without delay, of personal data collected or published. In AAC, it is possible in following ways

6.1. User Recovation
--------------------
With user revocation, AAC permit the deletion of user account from server by exposing the following REST APIs.

6.1.1. Delete User Using OAuth
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Allows for cancellation of user using OAuth.

**Permission**

**Parameters**

**Request**::

  DELETE /aac/user/me HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}

**Response**

200 OK

6.1.2. Delete User Using Basic Auth
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Allows for cancellation of user using user id.

**Permission**

**Parameters**

+ ccUserId (path variable) : a user Id [mandatory | STRING].

**Request**::

  DELETE /aac/user/{ccUserId} HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Basic {client or user access token}

**Response**

200 OK


6.3. Resource Rovocation.
-------------------------
With resource revocation, AAC allows for deletion of certain resources(client/user access token) from user account, providing key parameters like clientId, userId.
In this process, AAC expose the following REST APIs to do the same.

6.3.1. Revoke authorization token
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Revoke the access token and the associated refresh token

**Permission**

profile.basicprofile.all

**Parameters**

+ token (path variable) : an access token [mandatory | STRING].

**Request**::

  DELETE /aac/eauth/revoke/{token} HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Bearer {client or user access token}

**Response**

200 OK


6.3.2. Revoke authorization token provided user id and client id
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Revoke the access token against user id and client id.

**Permission**

profile.basicprofile.all

**Parameters**

+ userId (path variable) : a user Id [mandatory | STRING].
+ clientId (path variable) : an access token [mandatory | STRING].

**Request**::

  DELETE /aac/eauth/revoke/{clientId}/{userId} HTTPS/1.1
  Host: dev.welive.eu
  Accept: application/json
  Authorization: Basic {client or user access token}

**Response**

200 OK
