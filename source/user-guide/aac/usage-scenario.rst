1. USAGE SCENARIOS
==================

The Authorization and Authentication Control (AAC) system relies on the OAuth2.0 [OAuth] protocol for  the resource access authorization and external identity providers for the user authentication. Specifically, AAC supports integration with the identify providers made available through standard authentication protocols, such as Shibboleth, or Open ID. The specific configuration and use of the AAC depends on the scenario, in which it is exploited. The driving factors refer to

- the use of the information being accessed (i.e., whether the goal is only to authenticate the user or there is a need to access other protected APIs and services);

- the type of the information (i.e., the information or operations belonging to the user or the general information, such as aggregated data, batch data, etc.);

- the timing of the access (i.e., the access to the protected resources is performed only while the user is online or in the offline mode also);

- the origin of the information access (i.e., the access to the protected resources is done from a mobile client directly, through a proxy application, or merely in a server-to- server communication).

To supports these capabilities, the OAuth2.0 protocol defines the model of permissions and the corresponding data flow as follows.

- Each protected operation or resource is associated to a permission (scope in OAuth2.0 terminology). For instance, the user profile read and modify operations may be associated to ‘profile.read’ and ‘profile.write’ scopes correspondingly. To perform such an operation, the caller should have the corresponding permission granted by the user and associated to the access token.

- The scopes may refer to user operations (like change the profile of the currently logged user) or to generic operations (like read the profiles of all the users) or both. Depending on the type of the scope the client requires to access, different OAuth2.0 protocol data flows should be involved. When the user-related scopes are involved, the access should be explicitly granted by the authenticated user through implicit or authorization code flow. As for the generic operation scopes, the client credentials flow is engaged, which does not involve the user.

- The access tokens emitted by the OAuth2.0 protocols are short-living. If the access to the protected resource is required only while the user is online (i.e. for short period of time), the access token should be used. If there is a need to access the resources on behalf of the user even if the user is offline or when the authentication is persisted (e.g., remember-me authentication), it is possible to use previously obtained refresh token to generate a new access token without involving the user.

In the following sections, we consider a set of typical usage scenarios and their implementation with the AAC

component.

- **Authentication only.** The 3 rd party web app or mobile app require the user identification only, without requiring the access to the protected WeLive building blocks and resources. In this case, the AAC is used only as the user identity provider.

- **Access to the user-related protected operations and resources.** In these scenarios, the protected BBs are accessed on behalf of the user from the 3 rd party application (mobile or web).

- **Access to the non user-related operations and resources.** In these scenarios, the protected BBs are accessed without involving the user.

- **Protect BB with AAC scopes.** In this scenario, there is a need to protect a resource using AAC OAuth2.0 authorisation mechanism.