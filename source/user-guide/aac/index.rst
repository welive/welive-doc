AAC User Guide
==============

The document presents the instructions on the use of the Authentication and Authorization Control System.

In this user guide all the user scenarios and ineractions related to the AAC are described.

.. toctree::

    usage-scenario
    auth-only-scenario
    access-to-protected-resources
    using-aac-to-protect-resources
    registering-new-permissions
    right-to-forget
    slo
    references
    annex