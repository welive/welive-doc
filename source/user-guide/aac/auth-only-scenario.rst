2. AUTHENTICATION ONLY SCENARIO
===============================

This scenario exploits the AAC only as the way to uniquely identify the user, without requiring the access to the protected REST APIs. In this scenario, the app interacts with the AAC component to authenticate the user and to obtain the user information (cross-platform user Id, name, surname, etc). AAC enables this flow using either CAS [cas].

CAS protocol provides a way for the user authentication without explicitly passing the user credentials through the 3rd party app. The detailed description of the protocol specification may be found here::

	https://apereo.github.io/cas/4.2.x/protocol/CAS-Protocol.html

2.1. CAS PROTOCOL
-----------------

In a nutshell the protocol is realized through the following steps:

1. The app makes a request to the Web app, in which the authentication is integrated. The app detects that the authentication data is missing (i.e., the corresponding cookie is missing) and redirects the request to the AAC CAS login page passing the callback URL as the “service” parameter::

	https://dev.welive.eu/aac/cas/login?service=<callback-url>

2. The user performs the sign-in operation, and AAC authenticates the user. The response is redirected to the URL provided at the previous step. The CAS authentication “ticket” is provided as the parameter::

	<callback-url>?ticket=<ticket>

3. The Web app responds to this callback, invoking the AAC API to exchange the ticket for the user assertion document::

	GET https://dev.welive.eu/aac/cas/serviceValidate?ticket=<ticket>&service=<callback-url>

4. The provided assertion document represents the user data and may be handled correspondingly (stored in the DB, in the session only, etc.).
::

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<cas:serviceResponse xmlns:cas="http://www.yale.edu/tp/cas">
<cas:authenticationSuccess>
<cas:user>3</cas:user>
<cas:attributes>
<cas:longTermAuthenticationRequestTokenUsed>false</cas:longTermAuthenticationRequestTokenUsed>
<cas:isFromNewLogin>true</cas:isFromNewLogin>
<cas:name>Mario</cas:name>
<cas:surname>Rossi</cas:surname>
</cas:attributes>
</cas:authenticationSuccess>
</cas:serviceResponse>

5. The app responds to the client with the authentication result or redirects back to the resource requested originally.

2.2. CLIENT LIBRARIES
---------------------

The protocol is widely supported by a variety of the client implementations in Java and other languages. More details can be found here:

**https://apereo.github.io/cas/4.2.x/integration/CAS-Clients.html**

Specifically, when the application is implemented using the Java Spring framework [spring], the configuration may be implemented as follows (XML-based configuration):

1. Configure the properties for the CAS filter:
::

	<bean id="serviceProperties" class="org.springframework.security.cas.ServiceProperties">
	<property name="service" value="https://example.com/app/j_spring_cas_security_check" />
	<property name="sendRenew" value="false" />
	</bean>

The value of the service should match the protected application URL.

2. Configure the protected resources and the corresponding security filter. Note the reference to the WeLive CAS login entry point:
::

	<sec:http pattern="/protected" entry-point-ref="casEntryPoint">
	<sec:intercept-url pattern="/protected" access="IS_AUTHENTICATED_FULLY" />
	<sec:custom-filter position="CAS_FILTER" ref="casFilter" />
	</sec:http>
	<bean id="casFilter" class="org.springframework.security.cas.web.CasAuthenticationFilter">
	<property name="authenticationManager" ref="authenticationManager" /></bean>
	<bean id="casEntryPoint" class="org.springframework.security.cas.web.CasAuthenticationEntryPoint">
	<property name="loginUrl" value="https://dev.welive.eu/aac/cas/login" />
	<property name="serviceProperties" ref="serviceProperties" />
	</bean>

3. Configure the authentication provider to load authorization properties from the CAS assertion:
::

	<sec:authentication-manager alias="authenticationManager">
	<sec:authentication-provider ref="casAuthenticationProvider" /></sec:authentication-manager>
	<bean id="casAuthenticationProvider" class="org.springframework.security.cas.authentication.CasAuthenticationProvider">
		<property name="userDetailsService" ref="userService" />
		<property name="serviceProperties" ref="serviceProperties" />
		<property name="ticketValidator">
		<bean class="org.jasig.cas.client.validation.Cas20ServiceTicketValidator">
		<constructor-arg index="0" value="https://dev.welive.eu/aac/cas" />
	</bean>
	</property>
	<property name="key" value="welive"/>
	</bean>