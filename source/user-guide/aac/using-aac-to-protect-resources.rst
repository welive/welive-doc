4. USING AAC TO PROTECT RESOURCES AND TOKEN VALIDATION
======================================================

When there is a need to expose a new WeLive building block protected with the OAuth2.0 protocol capabilities, it is possible to engage the AAC capabilities. In particular, it is possible to use the OAuth access tokens and the model of AAC scopes. That is, the request to the exposed API should be accompanied with the access token (As bearer Authorization header). Since the token is merely an alpha-numeric secret key, in order to ensure that 
the token is valid (i.e., corresponds to a correct token and is not expired);the token is associated with the correct permission scope.In order to validate the token, it is necessary to perform HTTP get operation::

	https://dev.welive.eu/dev/api/aac/resources/access
	
The token is passed as Authorization header::

	Authorization: Bearer 025a90d4-d4dd-4d90-8354-779415c0c6d8

he permission scope to be verified against the token is passed as the scope query parameter::

	GET /dev/api/aac/basicprofile/all HTTPS/1.1
	Host: dev.welive.eu
	Accept: application/json
	Authorization: Bearer 025a90d4-d4dd-4d90-8354-779415c0c6d8
	scope=profile.basicprofile.me
	
The request return value of true (respectively, false) if the token is valid and is applicable for the specified scope.