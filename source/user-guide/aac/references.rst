7.REFERENCES
============
::

	oauth	OAuth 2.0 protocol. http://oauth.net/2/
	cas  	CAS protocol http://jasig.github.io/cas/4.0.x/protocol/CAS-Protocol.html
	spring	Spring Framework https://spring.io/ 
