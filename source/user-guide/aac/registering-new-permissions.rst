5. REGISTERING NEW PERMISSION SCOPES
====================================
When necessary, it is possible to define new permission scopes for use during the AAC OAuth2.0 authorization flows. The registration of new permissions takes place with registration of new permission groups, referred to as protected services. Each service may define more permission scopes corresponding to different usage (e.g., read and write) and different targets (i.e., user-related or non user-related operations).
To register a new service, it is necessary to select “My Services” menu option in the developer console:

.. image:: images/5.1.png

The service is defined with service ID (alpha-numeric identifier, ‘.’), service name and description:

.. image:: images/5.2.png

To add new permissions, it is necessary to add a new service permission mapping. For each permission it is necessary to define permission ID and URI (alpha-numeric identifier, ‘.’), name and description, define the target authority (user, client or both), whether the scope is public (default is false) and whether the client requires an explicit authorization from the platform administrator to request this scope (defaults false).

.. image:: images/5.3.png

Once registered, the scopes of the service may be accessed in the developer console client permissions tab.