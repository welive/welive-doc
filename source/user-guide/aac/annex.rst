8. ANNEX
========

7.1 AAC REST API
----------------

AAC exposes a set of APIs used in different phases of the authentication process:

- Token generation (within different flows)
- Token verification
- User login profile

7.1.1 TOKEN GENERATION API
^^^^^^^^^^^^^^^^^^^^^^^^^^

The token generation method allows for accessing the token data within the client credentials flow and within the authorization code flow. The method is realized a HTTP post at the following endpoint::

	https://dev.welive.eu/dev/api/aac/oauth/token
	
+----------------------+-------------------------------------------------------------------------+
| Parameter                |     Description                                                     |
+======================+=========================================================================+
| grant_type           | determines the authorization flow used. Possible values are             |
|                      | authorization_code or client_credentials.                               |
+----------------------+-------------------------------------------------------------------------+
| client_id            | Indicates the client that is making the request. The value passed in    |
|                      | this parameter must exactly match the value shown in the console.       |
+----------------------+-------------------------------------------------------------------------+
| client_secret        | The client secret obtained during application registration              |
+----------------------+-------------------------------------------------------------------------+
| redirect_uri         | Determines where the response is sent. Required in case of authorization|
|                      | code flow. The value of this parameter must exactly match one of the    |
|                      | values registered in the APIs Console (including the http or https      |
|                      | schemes, case, and trailing '/').                                       |
+----------------------+-------------------------------------------------------------------------+
|code                  |                                                                         |
+----------------------+-------------------------------------------------------------------------+

The authorization code returned from the initial request. Ignored in case of client_credentials flow.

Example for the authorization code flow request::

	POST /dev/api/aac/oauth/token HTTPS/1.1
	Host: dev.welive.eu
	Content-Type: application/x-www-form-urlencoded
	client_id=23123121sdsdfasdf3242&
	client_secret=3rwrwsdgs4sergfdsgfsaf&
	code=1234567&
	redirect_uri=http://www.example.com/protected&
	grant_type=authorization_code
	
A successful response to this request contains the following fields:

+----------------------+-------------------------------------------------------------------------+
|       Field          |Description                                                              |
+======================+=========================================================================+
|       access_token   |The token that can be used in resource requests                          |
+----------------------+-------------------------------------------------------------------------+
|  refresh_token       |Only in case of authorization code flow. A token that may be used to     |
|                      |obtain a new access token. Refresh tokens are valid until the user       |
|                      |revokes access. This field is only present if is included in the         |
|                      |authorization code request.                                              |
+----------------------+-------------------------------------------------------------------------+
| expires_in           |The remaining lifetime on the access token                               |
+----------------------+-------------------------------------------------------------------------+
| token_type           |Indicates the type of token returned. At this time, this field will      |
|                      |always have the value Bearer                                             |
+----------------------+-------------------------------------------------------------------------+
| scope                |Scopes associated to the token.                                          |
+----------------------+-------------------------------------------------------------------------+

A successful response is returned as a JSON object, similar to the following

.. code-block:: json

	{
	"access_token": "025a90d4-d4dd-4d90-8354-779415c0c6d8",
	"token_type": "bearer",
	"refresh_token": "618415da-dcfc-48b6-b775-7d0f04a71105",
	"expires_in": 38937,
	"scope": "profile.basicprofile.me"
	}

7.1.2. TOKEN VALIDATION API
^^^^^^^^^^^^^^^^^^^^^^^^^^^

In order to validate the token, it is necessary to perform HTTP get operation::

	https://dev.welive.eu/dev/api/aac/resources/access
	
The token is passed as Authorization header::

	Authorization: Bearer 025a90d4-d4dd-4d90-8354-779415c0c6d8
	
The permission scope to be verified against the token is passed as the scope query parameter::

	GET /dev/api/aac/basicprofile/all HTTPS/1.1
	Host: dev.welive.eu
	Accept: application/json
	Authorization: Bearer 025a90d4-d4dd-4d90-8354-779415c0c6d8
	scope=profile.basicprofile.me
	
The request return value of true (respectively, false) if the token is valid and is applicable for the specified scope.

7.1.3 PROFILE DATA API
^^^^^^^^^^^^^^^^^^^^^^

The API allows for accessing the information about the signed user. There are two types of profiles::

	Basic profile: contains cross-platform userId, name, surname.
	Account profile: contains information about the account information used to sign in (e.g., google email in case of google account);
	
The request is emitted to the following endpoint::

	https://dev.welive.eu/dev/api/aac/basicprofile
	
7.1.3.1. Read current user profile
""""""""""""""""""""""""""""""""""

The method allows for accessing the basic profile of the current user.

Permission::

	profile.basicprofile.me
	
Request::

	GET /dev/api/aac/basicprofile/me HTTPS/1.1
	Host: dev.welive.eu
	Accept: application/json
	Authorization: Bearer {user access token}
	
Response

.. code-block:: json

	{
	"name": "Mario",
	"surname": "Rossi",
	"userId": "6789"
	}

	
7.1.3.2. Read current user account profile
""""""""""""""""""""""""""""""""""""""""""

The method allows for accessing the account profile of the current user.

Permission::

	profile.accountprofile.me
	
Request::

	GET /dev/api/aac/accountprofile/me HTTPS/1.1
	Host: dev.welive.eu
	Accept: application/json
	Authorization: Bearer {user access token}
	
Response

.. code-block:: json

	{
	"accounts": {
		"google": {
				  "eu.trentorise.smartcampus.givenname": "Mario",
				  "eu.trentorise.smartcampus.surname": "Rossi",
				  "OIDC_CLAIM_email": "rossi@fbk.eu"
		          }
	           }
	}