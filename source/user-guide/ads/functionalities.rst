﻿2. Functionalities 
====================================================================

In this section, will be described functionalities of the Analytics Dashboard.
In order to login, you must have the credentials (username and password), obtained after registering on the portal. It takes only 2 minutes!
To login on the portal, click on button login  from WeLive home page. Then, you can choose a provider for your login (Google, WeLive or Facebook).

.. image:: images/oia7.png

In order to continue login, choose a provider (e.g. WeLive) and insert required information.

.. image:: images/oia8.png

After login, you can view your referred pilot main page.

.. image:: images/oia9.png

In order to access to the Analytics Dashboard, you can click on the chart.

.. image:: images/ads10.png

In the ADS the authenticated user can:

	* Create a new chart; 
	* Select a range of dates;
	* Remove charts;

2.1. Creating a new chart
--------------------------------------

To create a new chart:

	1.	Go to the ADS main page;
	2.	Open the section that you are going to explore;
	3.	Click on the KPI that you want to see;


.. image:: images/ads11.png

Select a section from the left menu of the ADS.

.. image:: images/ads12.png

When the section is displayed you can choose any of the KPIs of that section. you can collapse the section clicking again the title.

.. image:: images/ads13.png

Once you have clicked the KPI a chat will appear in the main section of the ADS tool


2.2. Select a range of dates
--------------------------------------

To create a new chart:

	1.	Select the start date of your new query;
	2.	Select the end date of your new query;
	3.	Refresh the data;


.. image:: images/ads14.png

Select the start date of your query using the calendar widget. The start date is mandatory if you want to query data in a time period

.. image:: images/ads15.png

Select the end date of your query using the calendar widget. The end date is an optional value, if there is not end date the system will use the current date for extract the data. 

.. image:: images/ads16.png

You can refresh the charts that are already opened if you click the refresh button.


2.3. Removing charts from the ADS
--------------------------------------

To create a new chart:

	1.	Remove a chart;
	2.	Remove all the charts;

.. image:: images/ads17.png

Click on the X of a chart for removing it from the central panel of the ADS.

.. image:: images/ads18.png

Click on the X of Header for removing all the charts rom the central panel of the ADS.