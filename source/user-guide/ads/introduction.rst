1. Introduction
===============

ADS (Analytics Dashboard) allows to gain insights and generate visualizations about the usage of the WeLive ecosystem in your city. You will be able to compare your city with other WeLive cities and to keep track of the ideas, challenges and urban apps you have created and published.

.. image:: images/ads1.png

It is necessary to perform the login to use the Analytics Dashboard (the registration is required. It takes only 2 minutes).
