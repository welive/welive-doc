Analytics Dashboard 
===============================

The document presents the instructions on the use of the Analytics Dashboard .

In this user guide all the user scenarios and interactions related to the Analytics Dashboard  are described.

.. toctree::

    introduction
    functionalities
    specific-user-type-kpis
