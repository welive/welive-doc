3. KPIS Dashboard  
====================================================================

Depending on the kind of users logged in, three different dashboards will be shown:

	* Citizen Dashboard;
	* City Dashboard;
	* Platform Dashboard: 

3.2. Citizen Dashboard
--------------------------------------

Set of statistics showing the current usage of the different components.


.. image:: images/ads20.png

Platform KPIs related to the usage of the logged user


3.3. Citizen Dashboard
--------------------------------------

It shows WeLive PAs those statistics directly linked to the city

.. image:: images/ads21.png

Header KPIs related to the statics of the city of the logged user

.. image:: images/ads22.png

Platform KPIs related to the statics of the city of the logged user

.. image:: images/ads23.png

Common APPs KPIs related to the statics of the questionnaire included in the apps of the city

.. image:: images/ads24.png

City APPs KPIs related to the statics of the apps of the city


3.4. Platform Dashboard
--------------------------------------

It includes all the statistics from the previous dashboards plus those related to the platform performance


It shows WeLive PAs those statistics directly linked to the city

.. image:: images/ads25.png

Header KPIs related to the statics of the platform

.. image:: images/ads26.png

Platform KPIs related to the statics of the platform

.. image:: images/ads27.png

Common APPs KPIs related to the statics of the questionnaire included in all the apps in the platform.

.. image:: images/ads28.png

City APPs KPIs related to the statics of all the apps in the platform.

