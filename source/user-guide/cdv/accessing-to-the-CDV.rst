Accessing to the Citizen Data Vault
===================================

In order to access to the Citizen Data Vault, click on user icon.

	1.	Perform the login into the platform,
	2.	Click on CDV icon provided into

			a. the WeLive platform main page,
			b. the platform toolbar,

	3.	View and manage your profile information into the CDV.

.. image:: images/cdv5.png
