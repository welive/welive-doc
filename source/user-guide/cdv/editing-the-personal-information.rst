Editing the personal information
================================

In order to edit your personal information:

	1.	Perform the login into the platform,
	2.	Go to the CDV main page,
	3.	Click on “edit” tab,
	4.	Fulfill the form to edit your personal information,
	5.	Click on “Save” button to complete the procedure and update your user profile.

.. image:: images/cdv6.png

.. image:: images/cdv7.png

.. image:: images/cdv8.png

.. image:: images/cdv9.png
