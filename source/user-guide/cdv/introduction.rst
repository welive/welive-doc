Introduction
============

The CDV access is allowed to the WeLive user registered as “citizen” only.
The entry point to the Marketplace is the WeLive platform public page.

.. image:: images/cdv1.png
