Citizen Data Vault User Guide
=============================

The document presents the instructions on the use of the Citizen Data Vault.

In this user guide all the user scenarios and interactions related to the Citizen Data Vault are described.

.. toctree::

    introduction
    login-to-the-WeLive-platform
    accessing-to-the-CDV
    editing-the-personal-information
