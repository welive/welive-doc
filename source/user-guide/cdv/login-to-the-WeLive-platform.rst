Login to the WeLive platform
============================

To login on the Welive platform:

	1.	Go to the WeLive home page,
	2.	Click on button “login”,
	3.	Select the provider to perform the login,
	4.	Provide the required information to complete the action.

.. image:: images/cdv2.png


.. image:: images/cdv3.png

After login, you can view your pilot main page.

.. image:: images/cdv4.png
