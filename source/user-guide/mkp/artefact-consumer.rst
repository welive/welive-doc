3. Logged in user (as an artefact consumer)
===========================================

You are a logged in user (citizen, business or academic representative, or authority), but you are not developer, into the marketplace if you can:

	* View the list of the artefacts related to your favourite pilot;
	* View the details page of an artefact;
	* Leave comments in an artefact;
	* Vote an artefact.

If you are a logged in user, you will view the Marketplace.

.. image:: images/mkp6.png

3.1 Leaving a comment in an artefact
------------------------------------------------------

In order to leave a comment in an artefact, click on the form at the bottom of the details page of the artefact.

.. image:: images/mkp7.png

3.2 Voting an artefact
-------------------------------

You can also vote an artefact from its details page, by placing your mouse on the number of stars that you want to assign to the artefact.

.. image:: images/mkp8.png

3.3 Suggested artefacts
----------------------------------

In addition to the public sections, if you are a logged in user, you are able to see into the catalogue also a section named “Suggested”, which contains a subset of all the recommended artefacts.

.. image:: images/mkp9.png
