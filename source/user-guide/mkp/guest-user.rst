2. Guest User
=============

If you are a guest user (not registered on the platform), into the Marketplace you can:

	* View the list of the artefacts related to your favourite pilot;
	* View the details page of an artefact.

To access to the Marketplace as guest:

	* Go to the WeLive home page;
	* Click on your favourite pilot;
	* Click on the cart icon of the Marketplace.
	* Enjoy!

.. image:: images/mkp2.png

.. image:: images/mkp3.png

You can filter all published artefacts by type, through a filter located at the top of the catalogue page, which allows you to select only the Building Blocks, Public Services or Datasets.

.. image:: images/mkp4.png

To view details page of an artefact, click on artefact badge.

.. image:: images/mkp5.png

On top of the page, you can view information such as the representative image, the title, the author, the provider, the publication date, the type, the tag list, the gallery of images.
At the bottom of the artefact details page, it is located a section where you can navigate through four subsections:

	*  	Details section reports general information about the artefact, e.g. the author, the abstract and detailed descriptions.
	*  	Interaction Points section exists only if the artefact is a building block and it lists all the online access point to the artefacts. Here you can find, for example, all the API exposed by the artefact (if it is a Web service or a complex Web application), or the URI where the artefact is available (if it is a dataset), or the URL of the artefact (if it is a Web application), and so on.
			* For each interaction point the title, the input arguments, the output type and a brief description are listed.
	*  	Offering pricing section lists all the pricing of the current artefact. This section constitutes a constraint for the usage of the artefact.
	*  	Terms and Conditions section lists the usage licenses to be applied to the artefact.
