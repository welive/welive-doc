4. Logged in user (as a diffusor)
=================================

To be a diffusor it is necessary that  you are logged in and you are registered as developer.
A diffusor/developer can access to all functionality offered by the Marketplace to the consumer and also can:

	* Create a new artefact;
	* View the artefacts you have created.

The main page of the Marketplace for the user developer is shown in the following figure.

.. image:: images/mkp10.png

4.1 Artefact Publication
----------------------------------
The Marketplace allows the user to easily publish new artefacts through its `Publication Wizard`.

.. note::

	If you are going to publish a new Web Service into the platform, you must describe it in compliance with the building block specification (see  `Building Block Model <https://dev.welive.eu/documentation/user-guide/bb/bb-model.html>`_ ) and provide a valid descriptor file in compliance with the following schema file: 

	* `WADL schema <https://www.w3.org/Submission/wadl/wadl.xsd>`_ (for REST)
	* `WSDL schema <http://schemas.xmlsoap.org/wsdl/>`_ (for SOAP)

	You can validate your descriptor file through the Marketplace `validation API <../../api-guide/mkp/validate.html>`_.




Step 1 - Artefact selection
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: images/wdp0.png

First the user has to select which kind of artefact she is going to publish. Possible choices are:

* **Building Block**
* **Public Service Application**
* **Dataset**

If the user selects `Building Block` or `Public Service Application` then she is redirected to the Publication Wizard she can continue with the publication. If the user select `Dataset` then she is redirected to the Open Data Stack (ODS) where she can continue with the dataset publication.

Step 2 - Artefact description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: images/wdp1.png

The Publication Wizard will ask the user to insert the information about the artefact to be published:

**Important note: the fields marked with an asterisk are required.**

* **Title (*)**: the title of the artefact
* **Description (*)**: a short description of the artefact
* **Type (*)**: the artefact category

	* **REST** or **SOAP** web services for `Building Blocks`
	* **Android App** or **Web App** for `Public Service Application`
* **WSDL/WADL descriptor URL (*)**: indicates one of the following

	* Homepage for `Web Apps`
	* Google Play page for `Android Apps`
	* WADL URL for `REST Web Services`
	* WSDL URL for `SOAP Web Services`

* **Documentation Page**: source code, documentation or Swagger page of the artefact
* **Tags**: list of terms that characterize the artefact separated by comma.

The user can also specify additional information by clicking on the `Advanced` button (only for expert users) or load a descriptor file (RDF) stored on her filesystem by clicking on the `Upload RDF File` button.



Step 3 - Images upload
^^^^^^^^^^^^^^^^^^^^^^

.. image:: images/wdp2.png

The `Gallery` tab of the wizard allows the user to upload one or more images related to the artefact. In order to upload a new image the user has to:

* click on `Add an image` and select an image file from her filesystem;
* click on `Submit` to upload the image;

Step 4 - Publishing
^^^^^^^^^^^^^^^^^^^

Finally the user can publish the new artefact by clicking on the `Publish` button on the bottom right corner.

4.2 Artefact publication (advanced editor)
-----------------------------------------------------------

In order to create a new artefact by using advanced editor, click on “new artefact” and then on "advanced" and the browser redirects you to a publication wizard, which consists of two steps:

	* **General**, where you can formally describe the general information about the artefact you are going to publish;
	* **Gallery**, where you can upload a set of images that will be shown in the artefact details page.



In the General section, you can:

	* Create a new model
	* or Upload RDF file.

 By clicking on Create new model, you should fill the fields required in six tabs:
 	General;
 	Dependencies;
 	Security Profile;
 	Service Offering;
 	Licenses;
 	Review and Confirm.

The general tab allows you to insert general information about new artefact you are going to create, such as title, creation date, author, service provider, etc.
Put required information related to the artefact and click on next to pass to the dependencies tab.

.. image:: images/mkp12.png

After the general tab, the artefact creation process passes to the dependencies tab, that will include information related to other artefacts useful to the artefact you are going to create.

 .. image:: images/mkp13.png

 After dependencies tab, your creation process passes to the Security Profile tab, where you can create a security profile for the usage of your new artefact.

.. image:: images/mkp14.png

After this, the next tab is “Service offering”, in which you can add your offers for your artefact.

.. image:: images/mkp15.png

Then, if you want, you can associate a license to your new artefact and you can specify the terms of use of the artefact.

.. image:: images/mkp16.png

The next tab is “Review and Confirm” tab, that contains the details of previous tabs. By clicking on finish, you confirm all information of tabs in general section.

.. image:: images/mkp17.png

When you complete the General section, you will view artefact details. In this page (Figure 18), you can:

	* edit the artefact details by clicking on “Edit”,
	* or click on “next” to pass to the Gallery section.

In Gallery section, you can add one or more images for your artefact.

Onto the Gallery, by using the button “Choose the image”, you  can upload the images that represents the artefact. Once uploaded at least one image, you can choose one of them as icon of the artefact by clicking the corresponding icon over the thumbnail.
Even the Gallery, such as the Application tab, is not mandatory; so you can skip it and directly publish the artefact.

You can also create a new artefact by clicking on Upload RDF file from the artefact creation page. In this way, you upload from your PC an RDF file that already contains artefact information.

When you upload an RDF file, in the artefact creation process you can:
	* Edit information in RDF file and then continue in the creation process, as described before.
	* Click on next to continue in the creation process.

4.3.1 View the artefacts created
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

From your Marketplace mainpage, you can be redirected to My artefacts page, by clicking on “my artefacts”.

.. image:: images/mkp23.png

The “My artefacts” page contains two main sections:

	*  The 'Approved artefacts' section on the top of the page lists all your published artefacts;
	*  The 'Drafts' section on the bottom of the page lists all the artefacts that are not published yet and, therefore, are still editable.

.. image:: images/mkp24.png

If you select an artefact published, you are redirected to the artefact details page, which will appear as we already have described it before. If you select artefact in 'Draft' status, the artefact details page contains also an additional couple of buttons ('Edit' and 'Delete').

.. image:: images/mkp25.png

As shown in the above picture, in the draft artefact details page, every authorized user can see the 'Edit' and 'Delete' buttons:

	* By clicking the “'Edit'” button you will be redirected to the publication wizard where you can edit the information contained in the two tabs.
	* By clicking the “'Delete'” button you will be redirected to his “'My artefacts'” page where a confirmation message will notify you about the success of the deletion and where you see that the deleted artefact is no longer listed.

.. image:: images/mkp26.png
