4. Logged in user (as a diffusor)
=================================




Artefact Publication
----------------------------

The Marketplace allows the user to easily publish new artefacts through its `Publication Wizard`.

.. note::

	**If you are going to publish a new Building Block, you must provide a valid descriptor file in compliance with the  Visual Composer specification.**

	 The following are the referred schema file:

	* `WADL schema <https://dev.welive.eu/visualcomposer/schema/xsd/wadl-schema.xsd>`_ (for REST)
	* `WSDL schema <https://dev.welive.eu/visualcomposer/schema/xsd/wsdl-schema.xsd>`_ (for SOAP)

	You can validate your descriptor file through the Marketplace `validation API <../../api-guide/mkp/validate.html>`_.




Step 1 - Artefact selection
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: images/wdp0.png

First the user has to select which kind of artefact she is going to publish. Possible choices are:

* **Building Block**
* **Public Service Application**
* **Dataset**

If the user selects `Building Block` or `Public Service Application` then she is redirected to the Publication Wizard she can continue with the publication. If the user select `Dataset` then she is redirected to the Open Data Stack (ODS) where she can continue with the dataset publication.

Step 2 - Artefact description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: images/wdp1.png

The Publication Wizard will ask the user to insert the information about the artefact to be published:

**Important note: the fields marked with an asterisk are required.**

* **Title (*)**: the title of the artefact
* **Description (*)**: a short description of the artefact
* **Type (*)**: the artefact category

	* **REST** or **SOAP** web services for `Building Blocks`
	* **Android App** or **Web App** for `Public Service Application`
* **WSDL/WADL descriptor URL (*)**: indicates one of the following

	* Homepage for `Web Apps`
	* Google Play page for `Android Apps`
	* WADL URL for `REST Web Services`
	* WSDL URL for `SOAP Web Services`

* **Documentation Page**: source code, documentation or Swagger page of the artefact
* **Tags**: list of terms that characterize the artefact separated by comma.

The user can also specify additional information by clicking on the `Advanced` button (only for expert users) or load a descriptor file (RDF) stored on her filesystem by clicking on the `Upload RDF File` button.



Step 3 - Images upload
^^^^^^^^^^^^^^^^^^^^^^

.. image:: images/wdp2.png

The `Gallery` tab of the wizard allows the user to upload one or more images related to the artefact. In order to upload a new image the user has to:

* click on `Add an image` and select an image file from her filesystem;
* click on `Submit` to upload the image;

Step 4 - Publishing
^^^^^^^^^^^^^^^^^^^

Finally the user can publish the new artefact by clicking on the `Publish` button on the bottom right corner.
