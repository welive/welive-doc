4. DEPLOYING SERVICE BB IN CLOUDFOUNDRY
=======================================

CloudFoundry is an open source Platform-as-a-Service that enables deployment and execution of generic web applications and services. It allows the applications developed with a variety of languages and frameworks to be executed and scaled in a cloud environment. More details about the CloudFoundry platform and its usage may be found at http://docs.cloudfoundry.org/.

Specifically, the CloudFoundry platform made available within the WeLive project supports the following languages and frameworks (referred to as Buildpacks)::

	Java (executable spring apps or web apps packed as war files);
	Ruby
	NodeJS
	Go
	Python
	PHP

Besides, the platform supports a set of infrastructural services that may exploited by the apps in order to make the app data persistent::

	PostgreSQL
	MongoDB

4.1. Using CloudFoundry
-----------------------

To start using the CloudFoundry, it is necessary to install the command-line interface. The installation instructions may be found here::

	https://docs.cloudfoundry.org/devguide/cf-cli/install-go-cli.html

Once installed, it is necessary to target the CloudFoundry endpoint::

	> cf api https://api.cloudfoundry.welive.eu

The endpoint may also be associated during the login phase. To perform the login, it is necessary to use the credentials obtained from the provider1::

	> cf login –a https://api.cloudfoundry.welive.eu -u username –p password

Once entered, the user is automatically associated to the organization (WeLive) and the space (corresponds to the project organization name). The users have the developer permissions and are allowed to deploy the applications within the space quota.

4.2. Deploying app to CloudFoundry
----------------------------------

To deploy an application to the cloud, the following steps are required:

1. Prepare the application artefacts. This may be done, e.g., by packaging the app (Java war or jar file) or by providing the information about source code, deployment manifest, etc. More details may be found here: https://docs.cloudfoundry.org/devguide/cf-cli/getting-started.html#push 
   For example, to prepare the artefact for the mavenized Java project, it is enough to build a jar or war file::
   
	> mvn clean package

2. Push the application to CloudFoundry. If the artefact folder contains the deployment descriptor (yml file), the following command may be used::

	> cf push APPNAME

Here APPNAME is the name of the application as it will be deployed and exposed in the CloudFoundry. 
Alternatively, to push the packaged artefact it is necessary to pass the reference to it::

	> cf push APPNAME –p target/welive.quickstart.war

Once deployed, the application will be exposed on the internet at the following address::

	https://APPNAME.cloudfoundry.welive.eu

Please note that by default the endpoint certificate may be not recognized as valid signed SSL certificate. 

4.3. Using CloudFoundry services
--------------------------------

CloudFoundry allows the application to exploit additional infrastructural components, such as message queues, databases, etc. These components, referred to as CloudFoundry services, should be instantiated and bind to the application during the application. More details about CloudFoundry services and their usage for the applications may be found here: http://docs.cloudfoundry.org/devguide/services/managing-services.html 
Currently, the following services are enabled for the developers::

	PostgreSQL
	MongoDB
	
To see the list of services available, it is necessary to access the CloudFoundry service marketplace::

	> cf marketplace

	Getting services from marketplace in org WeLive / space tecnalia as admin...
	OK

	service      plans                    description   
	Mongo DB     Default Mongo Plan*      A simple MongoDB service broker implementation   
	PostgreSQL   Basic PostgreSQL Plan*   PostgreSQL on shared instance.  

	The command returns the list of available service types and associated plans.

To create an instance of a service, the following command is used::

	> cf create-service SERVICE PLAN SERVICE_INSTANCE_NAME

Where SERVICE is the name of the service, Plan is the associated plan (use “” around the plan name) and SERVICE_INSTANCE_NAME is an alias to be used for the service instance.
To bind a service to an application, the following command is used::

	> cf bind-service APP_NAME SERVICE_INSTANCE

This allows the application to use the service instance within the application.
Note that the same service may bound to many applications if necessary.

Once you have a service instance created and bound to your application, you need to configure the application to dynamically fetch the credentials for your service instance. The VCAP_SERVICESenvironment variable contains credentials and additional metadata for all bound service instances. There are two methods developers can leverage to have their applications consume binding credentials.

- Parse the JSON yourself: See the documentation for VCAP_SERVICES. Helper libraries are available for some frameworks.
- Auto-configuration: Some buildpacks create a service connection for you by creating additional environment variables, updating config files, or passing system parameters to the JVM.

For details on consuming credentials specific to your development framework, refer to the Service Binding section in the documentation for your framework’s buildpack (for Java Spring apps see here: http://docs.cloudfoundry.org/buildpacks/java/spring-service-bindings.html). 

4.4. Using CloudFoundry Eclipse plugin
--------------------------------------

CloudFoundry provides an Eclipse IDE extension for Java developers. This allows to configure, deploy, and run the applications directly from the Eclipse IDE. More details may be found here::

	http://docs.cloudfoundry.org/buildpacks/java/sts.html 