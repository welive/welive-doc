Building Block User Guide
=========================

This document presents the process of the specification, implementation, and publication of the WeLive building blocks. Starting from the elements that are common for the building blocks of different nature (datasets, services, or service applications), the presented guide then makes a deep insight on the process for the building blocks that are realized as a RESTful Web Service.

.. toctree::

    bb-model
    bb-implementation-java
    bb-publishing-mkp
    bb-deploy-cf
    bb-create-use
    abbreviations
    references
    
    