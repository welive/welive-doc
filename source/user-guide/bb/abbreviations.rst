6. ABBREVIATIONS
================

+-------------+-------------------------------------+
| BB          | WeLive Building Block               |
+-------------+-------------------------------------+
| WADL        | Web Application Description Language|
+-------------+-------------------------------------+
| WSDL        | Web Service Definition Language     |
+-------------+-------------------------------------+
| REST        | Representation State Transfer       |
+-------------+-------------------------------------+
