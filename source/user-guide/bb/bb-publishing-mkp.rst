3. PUBLISHING BB IN WELIVE MARKETPLACE
======================================

In order to make the building block available to the other participants of the WeLive ecosystem, the BB should be published in the WeLive Marketplace. This may be achived in two ways:

- using the Marketplace online tool or

- programmatically, i.e., using the dedicated API exposed by the Marketplace component.

Publishing the BB using the online tool is described `here <https://dev.welive.eu/documentation/user-guide/mkp/index.html>`__. This user guides describes the steps necessary to publish a WeLive-compliant artifact, and in particular open service building block.

To publish the building block programmatically, one can use the “create-service” API method. As input the create service request requires the service description and the necessary metadata, in the form of reference to USDL and WADL/WSDL documents. Full details on the API access and usage can be found `here <https://dev.welive.eu/documentation/api-guide/mkp/create.html#publish-service>`__.
