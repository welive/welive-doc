1. BUILDING BLOCK MODEL
=======================

The building block (hereafter, BB) represents a standalone, self-contained component that is made available to the application developers and users within the WeLive ecosystem. The building blocks may expose different functionalities and may be of different types allowing for different usages and contexts. More specifically, the following building block types are supported:

- Open datasets representing an information resource available in a machine-readable format over the Internet. These building blocks may define the open data that is made available by the data provider being, for instance, the municipality.

- Service building blocks, i.e., the Web services exposed over the Internet in a standard form. More specifically, the service BB may be exposed as SOAP-based Web service or as a RESTful service and should be provided with the corresponding specification document.

- Public Service Application, i.e., an application built on top of other building blocks (services or datasets) and made available as a mobile application or a Web application.

1.1. EXPOSING BUILDING BLOCK SPECIFICATION
------------------------------------------

To become a WeLive building block, each component of these kinds should adhere to the following requirements:

- Should expose the corresponding specification metadata document [WELIVE-CORE]. The document should provide the BB description (information about type, provider/owners, license information, interaction and execution characteristics, etc.) as defined by the Building Block Specification Notation profiles.
- Should provide the necessary information about its usage through the WeLive logging core building block. This information may be provided directly by the BB component itself or by the execution platform, where the BB is hosted (if applicable).
- When the access to the BB is restricted or is a subject of the authentication/authorization policies, the component should be provided with the Security Profile specification metadata and rely on the WeLive AAC core building block.
- When required, should expose the additional metadata necessary for the BB integration and execution, according to the specific nature. For example, in case of Web services, the BB should expose the corresponding standard WSDL/WADL specification describing the service.

The resources defining the building block metadata should be exposed according to the following scheme:

+----------------------------------+--------------------+-----------------------------------------------+
|      URL                         |    HTTP Method     |         Description                           |
+==================================+====================+===============================================+
| {building_block}/spec/list       |GET                 |Gets the list of specification formats         |
|                                  |                    |supported by the building block(it should      |  
|                                  |                    |include at least xwadl and usdl)               |
+----------------------------------+--------------------+-----------------------------------------------+
| {building_block}/spec/xwadl      |GET                 |Gets the WADL standard specification           |
|                                  |                    |of the building block                          |
|                                  |                    |                                               |
+----------------------------------+--------------------+-----------------------------------------------+
| {building_block}/spec/wsdl       |GET                 |Gets the WSDL standard specification           |
|                                  |                    |of the building block exposed as SOAP web      |
|                                  |                    |service                                        |
+----------------------------------+--------------------+-----------------------------------------------+
| {building_block}/spec/usdl       |GET                 |Gets the Linked USDL specification of the      |
|                                  |                    |building block                                 |
+----------------------------------+--------------------+-----------------------------------------------+

1.2. BUILDING BLOCK SPECIFICATION EXAMPLE
-----------------------------------------

We consider a simple “echo” building block that provides the profile information of the current user (or a client application that acts on behalf of the user). This BB is implemented as a RESTful Web service and deployed elsewhere (e.g., in a CloudFoundry PaaS). More specifically, BB has the following characteristics:

- The service exposes a RESTful resource that returns the profile XML or JSON data structure;

- The resource is protected and relies on the OAuth2 protocol for the authentication. The corresponding token should be provided when the service is called.

The following document represents the Building Block USDL specification (see [WELIVE-CORE] for the specification details)::

 <rdf:RDF
    xmlns:j.0="http://www.holygoat.co.uk/owl/redwood/0.1/tags#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:j.1="http://xmlns.com/foaf/0.1/"
    xmlns:j.2="http://www.welive.eu/ns/welive-core#"
    xmlns:j.3="http://welive.eu/ns/security#"
    xmlns:j.4="http://purl.org/dc/terms/"
    xmlns:j.5="http://schema.org/" >
  <rdf:Description rdf:about="http://www.welive.eu/ns/welive-core#Service">
    <j.4:type>RESTful Web Service</j.4:type>
    <j.2:hasInteractionPoint rdf:resource="http://www.welive.eu/ns/welive-core#InteractionPoint0"/>
    <j.2:hasSecurityMeasure rdf:resource="http://www.welive.eu/ns/welive-core#Security0"/>
    <j.0:tag>Test</j.0:tag>
    <j.2:pilot>Trento</j.2:pilot>
    <j.2:hasBusinessRole rdf:resource="http://www.welive.eu/ns/welive-core#Author"/>
    <j.2:hasLegalCondition rdf:resource="http://www.welive.eu/ns/welive-core#License0"/>
    <j.4:abstract>Service abstract.</j.4:abstract>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#Artifact"/>
     <j.4:created>05/07/2016</j.4:created>
    <j.1:page></j.1:page>
    <j.4:title>WeLive Quickstart Example</j.4:title>
    <j.4:description>Service description.</j.4:description>
  </rdf:Description>
  <rdf:Description rdf:about="http://www.welive.eu/ns/welive-core#IdentityProvider0_0">
    <j.4:identifier>Welive</j.4:identifier>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#IdenitityProvider"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://www.welive.eu/ns/welive-core#Author">
    <j.1:businessRole>Author</j.1:businessRole>
    <j.1:mbox>asirchia@welive.eng.it</j.1:mbox>
    <j.4:title>Nino Sirchia</j.4:title>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#Author"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://www.welive.eu/ns/welive-core#Security0">
    <j.3:requires rdf:resource="http://www.welive.eu/ns/welive-core#Permission0_2"/>
    <j.3:requires rdf:resource="http://www.welive.eu/ns/welive-core#Permission0_1"/>
    <j.3:requires rdf:resource="http://www.welive.eu/ns/welive-core#Permission0_0"/>
    <j.2:withIdentityProvider rdf:resource="http://www.welive.eu/ns/welive-core#IdentityProvider0_0"/>
    <j.2:accessType>User</j.2:accessType>
    <j.2:protocolType>OAuth2</j.2:protocolType>
    <j.4:description></j.4:description>
    <j.4:title>OAuth authorization</j.4:title>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#Security"/>
   </rdf:Description>
  <rdf:Description rdf:about="http://www.welive.eu/ns/welive-core#Permission0_2">
    <j.4:description></j.4:description>
    <j.4:title>profile.basicprofile.all</j.4:title>
    <j.4:identifier>profile.basicprofile.all</j.4:identifier>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#Permission"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://www.welive.eu/ns/welive-core#License0">
    <j.4:description></j.4:description>
    <j.4:title>GNU-LGPL</j.4:title>
    <j.2:url>https://www.gnu.org/licenses/lgpl-3.0.en.html</j.2:url>
    <j.4:hasVersion></j.4:hasVersion>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#LegalCondition"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://www.welive.eu/ns/welive-core#Permission0_1">
    <j.4:description></j.4:description>
    <j.4:title>cdv.profile.me</j.4:title>
    <j.4:identifier>cdv.profile.me</j.4:identifier>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#Permission"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://www.welive.eu/ns/welive-core#InteractionPoint0">
    <j.5:url>http://test.com/welive.quickstart/spec/xwadl</j.5:url>
    <j.4:title>REST Interaction Point</j.4:title>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#InteractionPoint"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://www.welive.eu/ns/welive-core#Permission0_0">
    <j.4:description></j.4:description>
    <j.4:title>profile.accountprofile.me</j.4:title>
    <j.4:identifier>profile.accountprofile.me</j.4:identifier>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#Permission"/>
  </rdf:Description>
 </rdf:RDF>

In this example, the specification declares a “QuickStart” building block. The interaction with the building block is defined through the RESTful Web service interaction point. 
Finally, the specification declares that the service interaction relies on the OAuth2.0 authentication protocol.