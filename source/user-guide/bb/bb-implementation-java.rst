2. BUILDING BLOCK JAVA DEVELOPMENT TOOLS
========================================

Building block java development tools aimed at demonstrating to developers, the usage of building blocks and applications.

2.1 STARTER TEMPLATE APPLICATION
--------------------------------

The Starter template web application, based on spring is created to demonstrate for development purposes, the authorize and code flow usage of authentication using AAC building block together with the usage of WeLive Logging building block to log event entries. The project is realized on top of AAC and Logging building block libraries.
 
2.1.1. Functional Aspects
^^^^^^^^^^^^^^^^^^^^^^^^^

Starter app is a template web application based on spring. The purpose of this web application is to demonstrate for development purposes the authorize and code flow using AAC.

It demonstrate the following:

- allows to authenticate user using aac client library(welive-aac-client-bitbucket) and act as resource server to validate user access token against the offered resources.

- allows user to log to welive logging server using logging client library(welive-logging-client-bitbucket).

- acts as resource server and expose /echo endpoint with pre authorize scope (profile.basicprofile.me) so that user with corresponding scope can only access it. It demonstrate Spring OAuth features
  of Resource Servers to decode tokens through an HTTP resource on the Authorization Server (/oauth/check_token). It demonstrate with the security configuration of (/echo) operation, the usage of Spring
  Security's expression-based access control filter over operation using custom implementation of RemoteTokenService in aac client library (welive-aac-client-bitbucket).
  
- demonstrate single signOn/signoff.

- provides logging console that shows all the events logged to welive logging server.

- offer swagger console, the link to which is present on home page for signed in user.
  
- exposes WADL endpoint.

- exposes USDL specification document endpoint.


2.1.2 Source Structure
^^^^^^^^^^^^^^^^^^^^^^

One can download the source code of the application from::

	https://bitbucket.org/welive/bb-devtools
	
provided with credentials.

The project consists of three sub projects

- welive-aac-client-bitbucket <AAC Client Library>
- welive-logging-client-bitbucket <Logging Client Library>
- welive-aac-template-starter <STARTER TEMPLATE APPLICATION>
  
2.1.3. Configure
^^^^^^^^^^^^^^^^

The application.yml file in the resource folder of welive-aac-template-starter project, is the central point of configuration::

    aac:
    client:
    clientId: <CLIENT_ID>
    clientSecret: <CLIENT_SECRET>
    accessTokenUri: https://test.welive.eu/aac/oauth/token
    userAuthorizationUri: https://test.welive.eu/aac/eauth/authorize
	resource:
    userInfoUri: https://test.welive.eu/aac/basicprofile/me
    serviceId: http://localhost:5057/login/aac
	oauthServer:
    serverConfig:
        endpoint: https://test.welive.eu/dev/api
        logoutUrl: https://test.welive.eu/aac/cas/logout
	logging:
    endpoint: https://test.welive.eu/dev/api
	level:
    org.springframework.security: DEBUG

One can modify the urls as per the context. For the sake of demonstration all the AAC and Logging operation urls are pointed to Swagger API on Welive test server.

The client libraries are embedded in the starter template application projects as maven dependency::
	
	<dependency>
		<groupId>it.smartcommunitylab</groupId>
		<artifactId>welive.aac.client</artifactId>
		<version>1.0</version>
	</dependency>
	<dependency>
		<groupId>it.smartcommunitylab</groupId>
		<artifactId>welive.logging.client</artifactId>
		<version>1.0</version>
	</dependency>
	
The instructions to build/install the client libraries inside maven repository are given in corresponding sections below.

2.1.4. API Methods
^^^^^^^^^^^^^^^^^^

**2.1.4.1. echo**
  
 This operation is protected with pre authorize scope to demonstrate spring oauth feature of specifying fine grained control check over operation in a convenient way::
	
	@PreAuthorize("#oauth2.hasScope('profile.basicprofile.me')")
	@RequestMapping(method = RequestMethod.GET, value = "/api/echo")
	public @ResponseBody Response<String> echo(@RequestHeader(value = "Authorization") String token)
			throws WeliveException {

		// fetch basic profile.
		BasicProfile profile = profileService().getBasicProfile(userToken);
		pushLog("BASIC PROFILE");
		return new Response<String>("Hi " + profile.getName() + "!");
	}
	
 All the access to this operation will be validated against permitted scope using RemoteTokenService of AAC client library(welive-aac-client-bitbucket).
 
 The security configuration of this operation uses spring security's expression-based access control(#oauth2.hasScope('profile.basicprofile.me')) as shown below::
	
	@Bean
	RemoteTokenServices remoteTokenServices() {
	RemoteTokenServices rts = new RemoteTokenServices();
	rts.setClientId(env.getProperty("aac.client.clientId"));
	rts.setClientSecret(env.getProperty("aac.client.clientSecret"));
	rts.setCheckTokenEndpointUrl(
			env.getProperty("oauthServer.serverConfig.endpoint") + "/aac/eauth/check_token");
	return rts;
	}

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
 	resources.resourceId(null);
	resources.tokenServices(remoteTokenServices());
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
 	// @formatter:off
 	http.antMatcher("/me").authorizeRequests().anyRequest().authenticated();
 	// @formatter:on
 	http.requestMatchers().antMatchers("/echo").and().authorizeRequests().anyRequest()
 			.access("#oauth2.hasScope('profile.basicprofile.me')");
	}


2.1.5. Usage
^^^^^^^^^^^^

The project is realized on top of maven build tool. It can be build/install using::

	mvn clean install 

The project can be launched using StarterApp (SpringBootApplication) java class. To start the welive-aac-template-starter application, run from project root::

	...\welive-aac-template-starter>java -Dserver.port=5057 -jar target\welive-aac-template-starter-1.0.jar

This will start the starter template application on port 5057::

	http://localhost:5057


.. _bb-ui:

2.1.6. Application Interface
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The application console offers an interface where user can authenticate using AAC. 

.. image:: images/2.1.5.1.png

The interface provides link to download RFD based USDL specification about project, conforming to the WeLive BB metamodel specification standards. Besides, the interface provides link to WADL endpoint which is intended to simply the reuse of Starter Template application in other applications.

.. image:: images/2.3.6.png

The logged in user can view clients details and can invoke the echo operation if provided with scope(profile.basicprofile.me)

.. image:: images/2.1.5.2.png

The interface provides a log window which logs not only the events in the scope of starter template application but also the ones saved on Logging block using respective client library as described in section below.

The interface provides link to swagger rest API console for signed in user which is another place from where  user can test the invocation of /echo operation.

.. image:: images/2.1.5.3.png


2.2. LOGGING CLIENT LIBRARY
---------------------------

The client library allows for logging the relevant application events (e.g., service execution event) through the WeLive core logging building block. 

2.2.1. Configure
^^^^^^^^^^^^^^^^

The libary can be configured at the instantiation phase of LoggingService class object::
	
	LoggingService.logClient(<Endpoint>, <ACCESS_TOKEN>)
	for e.g.
	LoggingService.logClient("https://test.welive.eu/dev/api", "Bearer e22ed6d4-f86d-4ad1-9e74-88fd9cc4e6b5")

where Endpoint should be replaced with the URL of the logging service endpoint URL (e.g., https://dev.welive.eu/dev/api/log or https://139.162.156.192/dev/api/log). 
ACCESS_TOKEN is the user/client access token associated to user account.


2.2.1. Usage
^^^^^^^^^^^^

The project is realized on top of maven build tool. It can be build/install using::

	mvn clean install 

The project is then injected as maven dependency inside the welive starter template project as shown in configure section of Starter Template Application.

To log a message first create/update a logging schema for the message for e.g::

	privates String APPID = "testApp";
	private static final String SCHEMATYPE = "testSchema";
	String jsonSchema = readFile("test-schema.json");
	Boolean status = loggingClient.updateSchema(APPID, jsonSchema, SCHEMATYPE);
	
test-schema.json

.. code-block:: json

	{
	"testSchema":{
	"$schema": "http://json-schema.org/draft-04/schema#",
	"type": "object",
	"properties": {
		"pilot": {
		"type": "string"
		},
	"action": {
	"type": "string"
	}
	},
	"required": [
		"pilot"
	]
	}
	}
	
The logging schema will be stored at Logging BB server. Any message of corresponding type will be validated against the schema before getting saved at server side. The schema above specifies
a mandatory field "pilot" of type String to be present in any Log message arriving from the application "testApp" with type "testSchema"::

	LogMsg payload = new LogMsg();
	payload.setAppId(APPID);
	Map<String, Object> attrs = new HashMap<String, Object>();
	attrs.put("pilot", "Trento");
	attrs.put("action", "LOGIN");
	payload.setCustomAttributes(attrs);
	payload.setType(SCHEMATYPE);
	payload.setMsg("TestSchema");
	loggingClient.pushLog(APPID, payload);

In case of failed validation, the orginator will get response with error information.


2.3. AAC CLIENT LIBRARY
-----------------------

The AAC Client library provide interface to AAC server operations for accessing the users profile and resource based information, as well as to support the authorization/code flow cycle.
It’s a wrapper project on top of AAC remote APIs exposed using swagger. It makes remote http calls to AAC rest api fetch information.

The AAC core building block used in the WeLive architecture for the building block access authentication and authorization relies on the OAuth 2.0 protocol.  According to the protocol, the access to the protected resources (services) should be accompanied with the credentials that identify the accessing parties (client application and the user, on behalf of which the access is performed). To obtain these credentials, the calling application (client) should obtain the permission from the caller (the user) to access the protected resource. The AAC ensures the authentication and permission granting process and issues the corresponding credentials (access tokens) according one of the predefined authorization flows (implicit flow, authorization token flow, or client flow).
Once issued, the access token should be included into the request to the protected resource. Note that the tokens are normally short-living; this is a responsibility of the client application to maintain the credentials between the access sessions (e.g., using the OAuth 2.0 refresh tokens or the “remember me” authentication approach).
According to the OAuth 2.0 protocol specification, different resources may require different permissions (scopes) from the user. To ensure that the resource access is performed with the necessary permissions, the AAC API may be used. 

The information is accessible via services realized as java beans inside client library and are categorized as follows.

- Resource Service 
- Profile Service.
- Authentication Service
- RemoteToken Services

2.3.1. Configure
^^^^^^^^^^^^^^^^

The library services can be configured at the instantiation phase::

	ProfileService profileService = new ProfileService(<SERVER_URL>)
	ResourceService resourceService = new ResourceService(<SERVER_URL>);
	AuthService authService = new AuthService(<SERVER_URL>, <CLIENT_ID>, <CLIENT_SECRET>);

Where the SERVER_URL is the URL of the AAC service (https://test.welive.eu/aac) and CLIENT_ID/CLIENT_SECRET are the keys corresponding to the client of the AAC infrastructure. These keys are necessary if the library is used to instantiate the OAuth 2.0 sign-in procedure. To obtain the keys, the application should be registered through the AAC console as AAC OAuth 2.0 client.

The client library support the following services:

- Authentication Service
- Resource Service
- Profile Service
- Remote Token Service.

2.3.2. Usage
^^^^^^^^^^^^

The project is realized on top of maven build tool. It can be build/install using::

	mvn clean install 

The project is then injected as maven dependency inside the welive starter template project.


2.3.3. Authentication Service
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The service offers method to authenticate the users and to obtain the necessary tokens on their behalf. The service expose methods required in different types of information flow (implicit and authorization code flows) depending on the timing and origin of access.

**2.3.3.1 Token Validation**

It is the standard approach for passing the credentials is using the Authorization header::

	Authorization: Bearer alphaNumericToken
	curl -H “Authorization: Bearer <ACCESS_TOKEN>” -X GET "http://localhost:8080/myapp/private-resource"

To check that the provided credentials are valid, the AAC provides the following API::

	aacService.isTokenApplicable(accessToken, SCOPE)

Where SCOPE is a comma-separated list of permission identifiers accepted by the AAC. The method returns true if the token is not yet expired and is applicable for all the specified scopes.

**2.3.3.2 User authentication and permission for granting**

When the application that hosts the building block exposes its user authentication page based on the WeLive AAC single sign-on, the AAC client library may be used to support the authentication procedure. To trigger the login procedure, it is necessary to redirect the browser to the authentication page of AAC. The URL may be constructed with the following code::

	aacService.generateAuthorizationURIForCodeFlow(REDIRECT_URI, null, SCOPE, null)

Here REDIRECT_URI stands for the URL, where the AAC should callback upon successful authentication and permission granting, and SCOPE identifies the comma-separated permissions to be requested to the user. If the authentication should be done with a specific Identity Provider only (i.e., “welive”), it is necessary to pass that as a second parameter. Otherwise, all the registered IdPs of the AAC client application will be used.

To handle the authentication callback, the application should expose a resource (using GET method), where the authentication code generated by the AAC will be exchanged for the real user credentials. In case Spring MVC framework used, the following code may implement this functionality::

	@RequestMapping(method = RequestMethod.GET, value = "/check")
	public ModelAndView securePage(HttpServletRequest request, 
          @RequestParam(required = false) String code) throws Exception {

	TokenData accessToken = aacService.exchngeCodeForToken(code, REDIRECT_URI);

      ...
	}

The token data obtained represents the access token together with the information about token type, expiration, scope, and the refresh token. The latter may be stored within the app in order to generate new user tokens without re-requesting the authentication procedure again.

**2.3.3.3. Application authentication**

when the application requires to access some protected resource not associated to a particular user, the OAuth 2.0 client flow may be exposed. In this case the access tokens are generated through the AAC API directly provided the client ID and client secret::

	AACService aacService = new AACService(AAC_SERVICE_ENDPOINT,
			CLIENT_ID, CLIENT_SECRET);
	TokenData accessToken = aacService.generateClientToken();
	
2.3.4 Resource Service
^^^^^^^^^^^^^^^^^^^^^^

In AAC, the resources are divided in two categories. Ones owned by the user (i.e user resources) and those that are available to an application without involving a user (i.e. client resources).
The access to the resources are controlled by permission defined using scopes. the permissions are grouped into “services” that form logically related sets of operation and resource permissions.
All this information related to resources are accessible via ResourceService of AAC Client Library.

**2.3.4.1 Usage**

To check if user access token can access a resource with scope::

	private ResourceService resourceService = new ResourceService(Constants.SERVER_URL);;
	private final String scope = "profile.basicprofile.me";
	Boolean canAccess = resourceService.canAccessResource(Constants.USER_AUTH_TOKEN, scope);
	
For detailed sample usage, check the TestResourceService test case inside welive-aac-client-bitbucket project here::

	https://bitbucket.org/welive/bb-devtools/
	
2.3.5. Profile Service
^^^^^^^^^^^^^^^^^^^^^^

This service offers methods to fetch basic and account profile information for the signed in user.

**2.3.5.1 Usage**

To view account profile for user::

	ProfileService profileConnector = new ProfileService(Constants.SERVER_URL);
	AccountProfile accountProfile = profileConnector.getAccountProfile(Constants.USER_AUTH_TOKEN);
	Assert.assertNotNull(accountProfile);
	BasicProfile bp = profileConnector.getBasicProfile(Constants.USER_AUTH_TOKEN);
	List<AccountProfile> profiles = profileConnector.getAccountProfilesByUserId(Collections.singletonList(bp.getUserId()), Constants.CLIENT_AUTH_TOKEN);

For detailed sample usage, check the TestProfileService test case inside welive-aac-client-bitbucket project here::

	https://bitbucket.org/welive/bb-devtools/

2.3.6. RemoteToken service
^^^^^^^^^^^^^^^^^^^^^^^^^^

Th Remote Token Service is realized in the scenario where the Authorization Service (AAC) and ResoureceService are realized as separate applications, located on different servers. The purpose of this
service is to decode the token correctly using an HTTP resource on AAC (/eauth/check_token). With this service in place, the client library offers a convenient and efficient way to filter operations for e.g /api/echo on resource server
based on scope and authorities. The RemoteToken service can be configured in security configuration as follow::

	@Bean
	RemoteTokenServices remoteTokenServices() {
		RemoteTokenServices rts = new RemoteTokenServices();
		rts.setClientId(env.getProperty("aac.client.clientId"));
		rts.setClientSecret(env.getProperty("aac.client.clientSecret"));
		rts.setCheckTokenEndpointUrl(
				env.getProperty("oauthServer.serverConfig.endpoint") + "/aac/eauth/check_token");
		return rts;
	}

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		/** its a trick to avoid resourceId check. **/
		resources.resourceId(null);
		resources.tokenServices(remoteTokenServices());
	}

	/**REMOTE TOKEN **/
	@Override
	public void configure(HttpSecurity http) throws Exception {
		// @formatter:off
		http.antMatcher("/me").authorizeRequests().anyRequest().authenticated();
		// @formatter:on
		http.requestMatchers().antMatchers("/api/echo").and().authorizeRequests().anyRequest()
				.access("#oauth2.hasScope('profile.basicprofile.me')");
	}

2.4. Meta data
--------------

The building block specification consists of a set of profiles used to characterize different aspects of the BB representation and execution. The aim of the specification is to capture the BB characteristics, that may be required:

- when the users (BB clients, citizens, developers) search for relevant components to build new applications upon;
- during the application execution, when a BB has to be accessed and invoked providing the unambiguous definition of access policies and invocation protocols.

In this regard, WeLive BB metamodel is created with the Building Block class and the corresponding subclasses are provided for each of the specific BB types (service, dataset, or public applciation).
Together with the generic properties (such as title and description, provider, classification), the metamodel defines the Legal Conditions that characterize the licensing and usage terms aspects.
The Interaction Point class defines the description of BB execution and the corresponding technical properties, such as the access protocol in case of a service, the interface definition, and the 
data formats. Finally, the Security Measure and the Service Level Profile classes describe the BB access policies and the service offering characteristics of the building blocks. 

The specification notation is RDF-based and relies on a wide range of existing ontologies and the formal definitions of the metamodel elements.

The Starter template application meta model specification(RDF-based) is shown below::
	
  <rdf:RDF
    xmlns:tags="http://www.holygoat.co.uk/owl/redwood/0.1/tags#"
    xmlns:welive-sec="http://www.welive.eu/ns/welive-security#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:welive-core="http://www.welive.eu/ns/welive-core#"
    xmlns:dc="http://purl.org/dc/terms/"
    xmlns:schema="http://schema.org/" > 
  <rdf:Description rdf:about="http://platform.smartcommunitylab.it/openservice/services#dev-toolsAuthentication">
    <welive-sec:withIdentityProvider>facebook</welive-sec:withIdentityProvider>
    <welive-sec:withIdentityProvider>google</welive-sec:withIdentityProvider>
    <welive-sec:protocolType>cas</welive-sec:protocolType>
    <welive-sec:protocolType>oauth</welive-sec:protocolType>
    <welive-sec:accessType>user</welive-sec:accessType>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-security#AuthenticationMeasure"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://platform.smartcommunitylab.it/openservice/services#dev-toolsWeLive-Consortium">
    <welive-core:businessRole rdf:resource="http://www.welive.eu/ns/welive-core#Provider"/>
    <foaf:page>http://test.welive.eu</foaf:page>
    <dc:title>WeLive Consortium</dc:title>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#Entity"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://platform.smartcommunitylab.it/openservice/services#dev-toolsCommunication">
    <welive-sec:protocol>https</welive-sec:protocol>
    <welive-sec:protocol>http</welive-sec:protocol>
    <welive-sec:origin>cccc</welive-sec:origin>
    <welive-sec:origin>ssss</welive-sec:origin>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-security#CommunicationMeasure"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://platform.smartcommunitylab.it/openservice/services#dev-tools">
    <dc:description>Starter app is a template web application based on spring. The purpose of this web application is to demonstrate for development purposes the authorize and code flow using AAC.It demonstrate the following:allows to authenticate user using aac client library(welive-aac-client-bitbucket) and act as resource server to validate user access token against the offered resources.allows user to log to welive logging server using logging client library(welive-logging-client-bitbucket).acts as resource server and expose /echo endpoint with pre authorize scope (profile.basicprofile.me) so that user with corresponding scope can only access it. It demonstrate Spring OAuth features of Resource Servers to decode tokens through an HTTP resource on the Authorization Server (/oauth/check_token). It demonstrate with the security configuration of (/echo) operation, the usage of Spring Security’s expression-based access control filter over operation using custom implementation of RemoteTokenService in aac client library (welive-aac-client-bitbucket).demonstrate single signOn/signoff.provides logging console that shows all the events logged to welive logging server.offer swagger console, the link to which is present on home page for signed in user.</dc:description>
    <dc:created>16/03/2017</dc:created>
    <tags:tag>AAC</tags:tag>
    <welive-core:hasLegalCondition rdf:resource="http://platform.smartcommunitylab.it/openservice/services#dev-toolslicense"/>
    <dc:type rdf:resource="http://www.welive.eu/ns/welive-core#WebService"/>
    <dc:title>dev-tools</dc:title>
    <dc:abstract>The building block (hereafter, BB) represents a standalone, self-contained component that is made available to the application developers and users within the WeLive ecosystem. The building blocks may expose different functionalities and may be of different types allowing for different usages and contexts</dc:abstract>
    <welive-core:type rdf:resource="http://www.welive.eu/ns/welive-core#WebService"/>
    <welive-core:hasBusinessRole rdf:resource="http://platform.smartcommunitylab.it/openservice/services#dev-toolsWeLive-Consortium"/>
    <welive-core:pilot>Trento</welive-core:pilot>
    <welive-sec:hasSecurityMeasure rdf:resource="http://platform.smartcommunitylab.it/openservice/services#dev-toolsCommunication"/>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#BuildingBlock"/>
    <welive-core:hasInteractionPoint rdf:resource="http://platform.smartcommunitylab.it/openservice/services#dev-toolsendpoint"/>
    <welive-sec:hasSecurityMeasure rdf:resource="http://platform.smartcommunitylab.it/openservice/services#dev-toolsAuthentication"/>
    <tags:tag>LoggingBB</tags:tag>
    <foaf:page>https://test.welive.eu/test/oauth-clientservice/spec</foaf:page>
    <welive-core:hasBusinessRole rdf:resource="http://platform.smartcommunitylab.it/openservice/services#dev-toolsSmartCommunityLab"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://platform.smartcommunitylab.it/openservice/services#dev-toolsSmartCommunityLab">
    <foaf:page>http://www.smartcommunitylab.it</foaf:page>
    <welive-core:businessRole rdf:resource="http://www.welive.eu/ns/welive-core#Owner"/>
    <dc:title>SmartCommunityLab</dc:title>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#Entity"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://platform.smartcommunitylab.it/openservice/services#dev-toolslicense">
    <dc:title>CC-BY</dc:title>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#StandardLicense"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://platform.smartcommunitylab.it/openservice/services#dev-toolsendpoint">
    <welive-core:wadl>https://test.welive.eu/test/oauth-client/service/spec/xwadl</welive-core:wadl>
    <dc:title>REST Interaction Point</dc:title>
    <schema:url>https://test.welive.eu/test/oauth-client</schema:url>
    <rdf:type rdf:resource="http://www.welive.eu/ns/welive-core#RESTWebServiceIP"/>
  </rdf:Description>
  </rdf:RDF>


As can be seen in RDF-based specification of Starter Template application, the interaction profile defines the means and protocols to be used for the invocation and communication with the building blocks. The service endpoint declares the metadata and WADL endpoint as shown above in section :ref:`bb-ui`.
The WADL endpoint provides machine-readable XML description of HTTP-based web services exposed by Starter Template application and is intended to simplify their reuse in other applications.





