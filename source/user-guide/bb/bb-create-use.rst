5. Creating and using Building Blocks hosted in the CNS Marketplace
===================================================================

Building Blocks that process vast amounts of data may require complex data refining algorithms and significant computing capacity. The complete data processing solution may involve multiple algorithmic steps implemented by different vendors as separate general-purpose modules in various programming languages. This arises special requirements for the building block design and the hosting environment in order to simplify implementation and satisfy scalability and performance requirements. In this Section we will describe how the Cloud’N’Sci.fi Marketplace (CNS Marketplace) provided by Cloud’N’Sci Ltd (CNS) can be utilized in using, creating, hosting and commercializing building blocks.

5.1. About the CNS Marketplace
------------------------------

The CNS Marketplace is based on the Algorithms-as-a-Service concept and offers an easy way to create, utilize and commercialize algorithmic data refining solutions as web services. The marketplace has been extended with WeLive Building Block support and can be used as an alternative way to create and host building blocks. The most up-to-date version, which includes the latest WeLive-specific features, is currently available for beta testing at web site https://beta.cloudnsci.fi (CNS Site). Later on, those features will be included in the official production version.

Online help pages of the CNS Marketplace can be found at https://beta.cloudnsci.fi/info/help. They contain detailed descriptions about CNS marketplace features and Use Cases for variety of tasks and will be referred in this document for further details. Page specific help content can be quickly opened by clicking any of the title texts shown pages. For example, clicking the ‘CATALOG’ title word at the Application Catalog page will open the related help page.

5.2. Using Building Blocks hosted in the CNS Marketplace
--------------------------------------------------------

Building blocks can be used via their automatically generated HTTP REST API. In practice, all BB requests are handled by a CNS Marketplace component called the ‘Building Block Proxy’ (BBP) which takes care of user authentication and BB usage monitoring. From the CNS marketplace’s perspective, all BBs are applications that access data refining services via the CNS marketplace API (CNS-API) and BBP is a general purpose implementation of such BB applications. Therefore, Building Blocks can be found from the Application Catalog of the CNS Marketplace.

All building blocks hosted in the CNS Marketplace require a valid access token to be transmitted with every BB request. Both WeLive and CNS access tokens are accepted. In order to create a new access token, the application which accesses the BB should require user authentication with either WeLive or CNS login. 

Clicking the BB entry START button on the Application Catalog page opens a Swagger UI that shows detailed description of the building block REST API. The UI also demonstrates making API requests by allowing user to enter input parameters, submit request and see the responses. Submitting BB requests requires user authentication with either WeLive or CNS login. Clicking the authentication button in the BB Swagger UI initiates the authentication process using the WeLive AAC BB.
The CNS marketplace creates customer entries automatically for all WeLive users that access building blocks hosted by CNS either directly or via some public service. In other words, the end user does not need to be aware of the fact that the BB is hosted in the CNS Marketplace. Later on, if the same user also signs up to the CNS Marketplace, the automatically generated CNS customer entry can be associated to the CNS user account.

In case the user is already logged into the CNS Marketplace, a new BB session and access token will be generated automatically for the user whenever necessary. 

5.3. Becoming a CNS Marketplace user or provider
------------------------------------------------

In order to enable the CNS Login option for BBs or to use/create data refining services at the CNS marketplace, the user must first register as a CNS marketplace user and accept usage terms. A new user account can be created by entering the CNS Site, clicking “Login” from the upper-right corner, and selecting “Sign up” from the popup window. After submitting the form and clicking the confirmation link in the sent confirmation email, the account is ready for use. 

Each user is associated to at least one customer profile which defines the legal entity presented by the user that can currently be either a consumer or a company (more profiles types will be added when needed). During registration, a consumer profile which defines the individual properties is created for each user. User may create one or more company profiles as needed. User may be asked to fill missing details of customer profiles before making payments or entering agreements to ensure that the created invoices, receipts and legal documents are valid.

Users who would like to submit new content to the CNS marketplace must also register one or more provider accounts and accept related usage terms. The provider account can represent a person, a team, a company or an organization and works as a marketing brand for published content. However, provider account is always associated to a real-world customer profile who is responsible for provider activity. Registered providers and content published by them can be browsed in the CNS marketplace and each provider also has a forum page for user support and marketing purposes. You can create a provider account in your My Business page by clicking “ADD PROVIDER” and filling the form with basic information such as name, description, logo and associated customer profile.

5.4. Publishing existing data refining services as Building Blocks
------------------------------------------------------------------

Any data refining service available in the CNS marketplace can be published as a building block by any registered CNS user. This can be done simply by obtaining access to an existing data refining service in the CNS marketplace and allowing others to use it via an automatically generated REST API. In other words, the one who publishes a building block and pays for possible related costs does not need to be the original developer – it can be, for example a city that wants to make a nice BB available to citizens for free. Sections 6.3.1 to 6.3.4 describe the main steps for this process in order.

For more detailed instructions, see CNS help page Use Case: Creating a building block (https://beta.cloudnsci.fi/info/creating-a-building-block)

5.4.1. Obtaining access to a data refining solution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

CNS marketplace users can find available data refining solutions and active campaigns from the solution catalog. (https://beta.cloudnsci.fi/catalog/solutions). You can obtain access to an interesting solution by choosing the most suitable campaign and accepting its terms by signing a solution agreement contract. Contracts are effective for a limited period and may involve usage limitations such as maximum request count.

You may also want to create a new project for the building block costs. This can be done in the Projects page. Use this project when purchasing access to solutions that will be made available as building blocks.

5.4.2. Submitting a new Building Block
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To create a new building block, go to your My Business page and click the number under “BBs” in your Provider Accounts section. Then click the “ADD BUILDING BLOCK” button. This opens a simple form where you can give a name and a description for the building block and add one or more BB features by associating a function name to an existing solution contract. After submitting, the building block is available for use at the CNS marketplace via an automatically generated REST API which is based on the function and data interfaces of the solutions associated to the BB. The given function names are included in the BB entry point URLs and identify the contract to be applied for incoming BB requests.

Note that submitted building blocks are public by default. In other words, any WeLive or CNS user can access the BB and make requests to associated solution contracts at your cost. In case you wish to limit access to CNS users that are associated to your provider team, select option ‘Private’. When you are ready, click the ‘Submit’ button to create and publish the building block in the CNS Marketplace.

At the moment, BBs are not submitted to the WeLive Marketplace automatically. If you wish to make the BB available via the WeLive marketplace as well, follow the instructions in the next subsection to download an automatically generated WADL or Linked-USDL specification of the BB that can be submitted to the WeLive Marketplace.

5.4.3. BB Specifications
^^^^^^^^^^^^^^^^^^^^^^^^

Building Block specifications (e.g. WADL and Linked-USDL) can be viewed both in your My Business page and in the building blocks page. In My Business page, click “View” in the building blocks “Definition” column to see the specifications. Or you can access the building blocks page through Catalog/Applications if BB is public and in My Business page by clicking {..} in the building blocks row. Click “Definition” in the opened page to view the specifications. Follow instructions given in Section 3 to submit the BB into the WeLive Marketplace.

5.5. Implementing new data refining solutions
---------------------------------------------

This subsection describes the main principles for implementing new data refining solutions that can be hosted in the CNS Marketplace and published as WeLive Building Blocks.

5.5.1. Integration Principles
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Data refining services available in the CNS marketplace are based on software modules that implement one or more specified function interfaces. Software modules can be implemented in practically any programming language and may utilize third party libraries. This allows developers to use their favorite technologies which they find the most suitable for the purpose and significantly lowers the overhead of integrating an existing solution to the marketplace. 

Technical flexibility is achieved by CNS’s decision to do software integration on process and file system level instead of requiring developers to implement their software based on some specific software framework. Software modules are launched as stand-alone processes in a secure sandboxes and communicate with the marketplace system using standard input/output/error streams and via file system. The protocol used to communicate with algorithm processes is described in the following subsection.

5.5.2. Cloud’N’Sci Refinery Protocol
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When a new request to a specific data refining service should be performed, the CNS Marketplace first checks whether the process executing the software module implementing the service is already running and starts a new process if necessary. Then it transfers all input data items to the server running the process and extracts them into local file system folders accessible by the process (read/write allowed by the sandbox). After this the system writes a command line to the process’s standard input stream, including a unique request id, absolute file system paths to the request input data folders and a path for folder to which request output should be saved. When the request is completed (either ready, failed or aborted) the process should indicate this by writing a line to standard output stream including the completed request id and paths to output folders.

This protocol used to communicate with data refining processes details is called the Cloud’N’Sci Refinery Protocol (CNS-RP). Detailed documentation of the CNS-RP protocol can be found in GitHub repository at: https://github.com/CloudNSci/cloudnsci-guides. All software modules to be hosted in the CNS marketplace are required to implement this protocol. CNS will provide example implementations of the protocol as open source software in different programming languages such as Java and Python. In addition, CNS will publish refinery development toolkits for the most popular programming languages which take full care of the CNS-RP protocol so that developer only need to implement the actual solution based on the toolkit framework.

5.5.3. Java Refinery Toolkit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
(To be described: Java toolkit for developing new data refining solutions)

5.5.4. Python Refinery Toolkit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
(To be described: Python toolkit for developing new data refining solutions)

5.5.5. Quick Mockups
^^^^^^^^^^^^^^^^^^^^
(To be described: Creating quick mockups for new data refining solutions)

5.5.6. Docker
^^^^^^^^^^^^^
Docker technology is used to define the exact execution environment and dependencies required by the software module. Unless some specific refinery toolkit is used (which take care of Docker related things), the repository should have a file named Dockerfile in the root directory including instructions for setting up the virtual machine to run the software. Usually, defining a new Dockerfile is easy because it can be based on one of the existing base images which install required system tools and libraries. For example, in case the software is written in Python, base image continuumio/anaconda3 will set up a full python execution environment. After that, only module specific python source files needs to be added to the image in order to run the software.

5.6. Publishing data refining solutions
---------------------------------------
Solutions are high level descriptions of data refining services that can be published in the marketplace even before actual implementation. When a solution is implemented, released and made available by launching a sales campaign it becomes a concrete data refining service that can be purchased in the marketplace and accessed under the terms specified by the campaign. The following subsections describe the main steps of the solution submission processes. For more detailed instructions, see CNS help pages Use Case: Creating a solution (https://beta.cloudnsci.fi/info/creating-a-solution)

5.6.1. Data Interfaces
^^^^^^^^^^^^^^^^^^^^^^
Data Interfaces specify the detailed structure and semantics of input/output/error data items in the level which is reasonable. Developers can write their own data interface documentation, refer to existing standards, give valid data examples or give an exact schema definition, e.g. as a JSON Schema. Published data interfaces can be browsed and viewed in the CNS marketplaces Interfaces page and developers are highly recommended to re-use an existing data interface whenever possible instead of writing a duplicate. 

If you can’t find a data interfaces that suit your needs, you can create new ones in the CNS marketplace Interfaces page. Clicking “ADD DATA INTERFACE” opens a form, where you can enter a name, a provider and a description for the data interface. You can also choose MIME Type for the data and/or select to use JSON and insert a JSON Schema.

5.6.2. Function Interfaces
^^^^^^^^^^^^^^^^^^^^^^^^^^
Function Interfaces specify the input, output and error data for a valid request to a data refining service and describe high-level service functionality, i.e. how output depends on the given input. Each data refining service implements exactly one function interface. Each function interface defines 1-N input data items, 1-M output data items and 1-K error data items. Each data item has a unique name (in the scope of the function interface) and refers to one of the data interfaces.  

Function interfaces can be created in the CNS marketplaces Interfaces page. Clicking “ADD FUNCTION INTERFACE” opens a form, where you can enter a name, a provider and a description for the function interface. Next you have to select the input, output and error data types from the created data interfaces.

5.6.3. Submitting Solutions
^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can create a new solution in your My Business page by clicking “ADD SOLUTION”. Solution information must include a name, short description and a link to a function interface which specifies the usage instructions. Providers are highly recommended to re-use existing function interfaces whenever possible instead of writing their own. Optionally, solutions can also have a logo and links for further information or demos. 

5.6.4. Describing Algorithms
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Solution releases can be associated to an algorithm implemented by the solution. Please refer to an existing algorithm whenever possible so that people can find your solution easier by visiting the Algorithm Summary page. You can also submit new algorithms in your My Business page by selecting “Algorithms” under your Provider Accounts section and clicking “ADD ALGORITHM”. This opens a simple form, where you can give a name and a description for your algorithm and also add further information links and a logo. 

5.6.5. Building Images
^^^^^^^^^^^^^^^^^^^^^^
Images are snapshots of software modules which are deployed into the marketplace. An image can comprise implementations of multiple solutions and required resource files. Developers can build and deploy images by first pushing all necessary files into a Git repository (support for other version control system may be added later) that is accessible from Internet. 

To build an image, go to your My Business page, select “Images” in Provider Accounts section and click “ADD IMAGE” button. This opens a form, where you can enter a name, a type and a description for your image. You will also need a (git) repository url and a repository branch. If your repository is public, no key is needed. In case of private repositories, you must allow access to it with the SSH key generated by the marketplace. The key is generated when you click “Continue”. In GitHub, you can allow access by adding the generated public SSH key to the list of user keys or as a repository deployment key.

5.6.6. Making Solution Releases
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Releases bind solutions and images together. Developer can launch a new version of a solution by submitting a new release which is associated to an image and defines the exact launch command that will start the refinery process in the Docker container based on the image. The launch command can include any number of arguments which can be used to configure solution instances for specific purposes. For example, an optimization algorithm can take the maximum number of search iterations as command line argument which affects both running time and result quality. In other words, the developer can create different releases of the same solution targeted to different user groups and needs simply by modifying the launch command. 

To release your solution, go to your My Business page, select “Solutions” in Provider Accounts section and click your solution version. This opens a popup window, where you can use older releases as a template by clicking their version number. Or you can create a new release from empty template by clicking “New Release” without selecting a version. 

Version number has to be different from any of the earlier releases for the same solution and may contain text. Core algorithm is the algorithm that your solution implements. Image refers to the Docker image that contains your solution implementation (created in the last step). Launch command has to be the exact command that starts the refinery process in the Docker container based on the image. Dependencies are needed if your solution uses other solutions as a part of its process. You can also add release notes about this version. Maintenance & Support period is the time you agree to maintain and support this release.

5.6.7. Launching Sales Campaigns
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Campaigns make solution releases available to marketplace users as purchasable data refining services. You can design and launch sales campaigns by using a campaign plan wizard. To start the wizard, go to your My Business page, select “Solutions” in Provider Accounts section and select “Campaigns” in the solutions row. Opened popup window shows current campaigns. Click “NEW CAMPAIGN PLAN” to open the campaign plan wizard.

In the opened popup window you can select the solution version that this campaign is for. You can freely choose a transaction base price which is charged from users per successful request under the basic campaign terms. You can also choose to offer lowered transactions prices for users that make an initial purchase, i.e. pay for a certain number of transactions in advance. You can set this with “Volume discount limit”. Developer may also decide to offer few free trial transactions per user so that customers can try the solution before making a purchase decision. Regardless of the transaction price charged from end-users, developers have to pay a fixed hosting fee for each successfully executed transaction.

5.7. Maintenance and Support
----------------------------
(To be described)

5.8. Cost and Sales
-------------------
(To be described)