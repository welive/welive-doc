# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2015, WeLive Consortium
# This file is distributed under the same license as the WeLive  package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: WeLive  0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-07 12:52+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:4
msgid "WeLivePlayer API guide"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:6
msgid ""
"The WeLivePlayer acts as intermediary between weliveplayer mobile client "
"and server APIs. The application provide control over the data required "
"by client. It makes additional REST calls in PRE and POST stages of "
"operations to match requirement and to ensure data consistency. It help "
"improves overall security by validating access token from  client and at "
"the same time improves the performance of access by maintaining internal "
"cache."
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:11
msgid "WeLivePlayer exposes a set of APIs methods:"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:13
msgid "Get Applications By City and Type."
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:14
msgid "Read User Profile Using Acess Token."
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:15
msgid "Get Single Application Comments."
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:18
msgid "REST API"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:21
msgid "Get Applications By City and Type"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:23
msgid ""
"This operation returns list of Applications for requested city and type "
"after validating access token."
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:25
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:93
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:156
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:228
msgid "**Permission**"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:27
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:95
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:158
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:230
msgid "**Parameters**"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:29
msgid ""
"pilotId (path param) : determines the region/city of interest [mandatory "
"| STRING] (for e.g. /Trento, /Bilbao, /Novisad, /All)"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:30
msgid ""
"appType (path param) : Indicates the type of applications [mandatory | "
"STRING] (for e.g. /BuildingBlock, /Dataset, /All)"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:31
msgid "start (query param) : indicates the page number [optional | INTEGER]"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:32
msgid ""
"count (query param) : indicates number of applications per page [optional"
" | INTEGER]"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:34
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:101
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:160
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:232
msgid "**Request**::"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:40
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:107
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:179
#: ../../source/api-guide/weliveplayer/weliveplayer.rst:238
msgid "**Response**"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:84
msgid ""
"SampleCall: "
"https://dev.welive.eu/dev/api/weliveplayer/api/apps/Bilbao/PSA?start=0&count=20"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:89
msgid "Read User Profile Using Acess Token"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:91
msgid ""
"This operation returns associated user profile after validating access "
"token."
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:97
msgid ""
"artifactId (path param) : indicates the id of application [mandatory | "
"STRING]"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:98
msgid "start (query param) : indicates the page number [optional | INTEGER]."
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:99
msgid ""
"count (query param) : indicates number of applications per page [optional"
" | INTEGER]."
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:149
msgid "SampleCall: https://dev.welive.eu/dev/api/weliveplayer/api/userProfile"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:152
msgid "Update User Profile Using Acess Token"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:154
msgid ""
"This operation updates profile of user corresponding to the access token."
" The server authenticates post body against user profile before making an"
" update on profile data."
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:224
msgid "Get Single Application Comments"
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:226
msgid ""
"This operation returns list of comments for requested application after "
"validating access token."
msgstr ""

#: ../../source/api-guide/weliveplayer/weliveplayer.rst:255
msgid ""
"SampleCall: "
"https://dev.welive.eu/dev/api/weliveplayer/api/appComments/1418?start=0&count=20"
msgstr ""

