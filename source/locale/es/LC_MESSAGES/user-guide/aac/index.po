# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2015, WeLive Consortium
# This file is distributed under the same license as the WeLive  package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: WeLive  0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-07 12:52+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: ../../source/user-guide/aac/index.rst:2
msgid "AAC User Guide"
msgstr "Guía de usuario del AAC"

#: ../../source/user-guide/aac/index.rst:4
msgid ""
"The document presents the instructions on the use of the Authentication "
"and Authorization Control System."
msgstr ""
"Este documento presenta las instrucciones de empleo del Sistema de Control de "
"Autenticación y Autorización (Authentication and Authorization Control System)."

#: ../../source/user-guide/aac/index.rst:6
msgid ""
"In this user guide all the user scenarios and ineractions related to the "
"AAC are described."
msgstr ""
"En el siguiente manual de usuario se describen todos los escenarios de uso e "
"interacciones relacionadas con el AAC."
