# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2015, WeLive Consortium
# This file is distributed under the same license as the WeLive  package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: WeLive  0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-07 12:52+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: ../../source/user-guide/aac/using-aac-to-protect-resources.rst:2
msgid "4. USING AAC TO PROTECT RESOURCES AND TOKEN VALIDATION"
msgstr "4. USAR EL AAC PARA PROTEGER RECURSOS Y VALIDAR EL TOKEN"

#: ../../source/user-guide/aac/using-aac-to-protect-resources.rst:4
msgid ""
"When there is a need to expose a new WeLive building block protected with"
" the OAuth2.0 protocol capabilities, it is possible to engage the AAC "
"capabilities. In particular, it is possible to use the OAuth access "
"tokens and the model of AAC scopes. That is, the request to the exposed "
"API should be accompanied with the access token (As bearer Authorization "
"header). Since the token is merely an alpha-numeric secret key, in order "
"to ensure that the token is valid (i.e., corresponds to a correct token "
"and is not expired);the token is associated with the correct permission "
"scope.In order to validate the token, it is necessary to perform HTTP get"
" operation::"
msgstr ""
"Cuando existe la necesidad de proteger un Building Block de WeLive con las "
"características del protocolo OAuth2.0, es posible utilizar el AAC. En "
"particular, es posible utilizar los tokens de acceso OAuth y el modelo de "
"ámbitos del AAC. Esto es, la petición a la API deberá ir acompañada del token "
"de acceso (en la cabecera Authorization, del tipo Bearer). Ya que el token es "
"simplemente una clave secreta alfanumérica, para asegurar que el token es válido "
"(es decir, se corresponde con un token correcto y no está caducado); el token es "
"asociado con el ámbito de permisos correspondiente. Para validar el token, es "
"necesario ejecutar una operación HTTP get::"

#: ../../source/user-guide/aac/using-aac-to-protect-resources.rst:9
msgid "The token is passed as Authorization header::"
msgstr "El token es enviado en la cabezera Authorization::"

#: ../../source/user-guide/aac/using-aac-to-protect-resources.rst:13
msgid ""
"he permission scope to be verified against the token is passed as the "
"scope query parameter::"
msgstr ""
"el ámbito de permisos que debe ser verificado junto al token es enviado "
"a través del parámetro scope::"

#: ../../source/user-guide/aac/using-aac-to-protect-resources.rst:21
msgid ""
"The request return value of true (respectively, false) if the token is "
"valid and is applicable for the specified scope."
msgstr ""
"La petición retorna el valor true (o false en caso contrario) si el token es "
"válido y es aplicable para el ámbito solicitado."
