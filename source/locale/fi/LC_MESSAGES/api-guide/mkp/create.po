# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2015, WeLive Consortium
# This file is distributed under the same license as the WeLive  package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: WeLive  0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-07 12:52+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: ../../source/api-guide/mkp/create.rst:2
msgid "Publish Artefacts"
msgstr ""

#: ../../source/api-guide/mkp/create.rst:3
msgid ""
"Following methods allow to publish new artefacts in the WeLive "
"Marketplace."
msgstr ""

#: ../../source/api-guide/mkp/create.rst:6
msgid "Publish Service"
msgstr ""

#: ../../source/api-guide/mkp/create.rst:7
msgid "Publishes a service into the Marketplace."
msgstr ""

#: ../../source/api-guide/mkp/create.rst:9
msgid "**Endpoint**: /mkp/create-service"
msgstr ""

#: ../../source/api-guide/mkp/create.rst:10
#: ../../source/api-guide/mkp/create.rst:35
msgid "**Method**: POST"
msgstr ""

#: ../../source/api-guide/mkp/create.rst:11
#: ../../source/api-guide/mkp/create.rst:36
msgid "**Authorization Header**: Basic"
msgstr ""

#: ../../source/api-guide/mkp/create.rst:12
#: ../../source/api-guide/mkp/create.rst:37
msgid "**Consumes**: application/json"
msgstr ""

#: ../../source/api-guide/mkp/create.rst:13
#: ../../source/api-guide/mkp/create.rst:38
msgid "**Produces**: application/json"
msgstr ""

#: ../../source/api-guide/mkp/create.rst:15
#: ../../source/api-guide/mkp/create.rst:40
msgid "**Input schema**"
msgstr ""

#: ../../source/api-guide/mkp/create.rst:31
msgid "Publish Dataset"
msgstr ""

#: ../../source/api-guide/mkp/create.rst:32
msgid "Publishes a dataset into the Marketplace."
msgstr ""

#: ../../source/api-guide/mkp/create.rst:34
msgid "**Endpoint**: /mkp/create-dataset"
msgstr ""

#: ../../source/api-guide/mkp/create.rst:55
msgid "Output"
msgstr ""

#: ../../source/api-guide/mkp/create.rst:56
msgid "Previous methods returns a JSON with the following schema:"
msgstr ""

