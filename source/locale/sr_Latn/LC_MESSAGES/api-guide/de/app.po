# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2015, WeLive Consortium
# This file is distributed under the same license as the WeLive  package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: WeLive  0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-07 12:52+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: ../../source/api-guide/de/app.rst:2
msgid "App Operations"
msgstr ""

#: ../../source/api-guide/de/app.rst:4
msgid ""
"Following methods allow performing CRUD operations over the metadata of "
"the apps published by the Welive Marketplace."
msgstr ""

#: ../../source/api-guide/de/app.rst:23
msgid "PUT /de/app/{appID}"
msgstr ""

#: ../../source/api-guide/de/app.rst:7
msgid ""
"This method allows creating or updating metadata about an app. The "
"``{appID}`` parameter refers to the ID of the app in the marketplace. "
"This method requests the following JSON containing the information of the"
" app:"
msgstr ""

#: ../../source/api-guide/de/app.rst:18
msgid ""
"**lang:** the language of the app. Languages supported: danish, dutch, "
"english, finnish, french, german, hungarian, italian, norwegian, porter, "
"portuguese, romanian, russian, spanish and swedish."
msgstr ""

#: ../../source/api-guide/de/app.rst:21
msgid "**tags:** tags of the app."
msgstr ""

#: ../../source/api-guide/de/app.rst:22
msgid "**scope:** the city or location in which is scoped the app."
msgstr ""

#: ../../source/api-guide/de/app.rst:23
msgid "**minimum_age:** the minimum age required to use the app."
msgstr ""

#: ../../source/api-guide/de/app.rst:26
msgid "DELETE /de/app/{appID}"
msgstr ""

#: ../../source/api-guide/de/app.rst:26
msgid ""
"This method allows deleting metadata about an app. The ``{appID}`` "
"parameter refers to the ID of the app in the marketplace."
msgstr ""

#: ../../source/api-guide/de/app.rst:29
msgid "GET /de/app/{appID}/recommend/apps"
msgstr ""

#: ../../source/api-guide/de/app.rst:29
msgid ""
"This method returns recommended apps based on given app tags. The "
"``{appID}`` parameter refers to the ID of the app in the marketplace."
msgstr ""

#: ../../source/api-guide/de/app.rst:32
msgid "GET /de/app/{appID}/recommend/building-blocks"
msgstr ""

#: ../../source/api-guide/de/app.rst:32
msgid ""
"This method returns recommended building blocks based on given app tags. "
"The ``{appID}`` parameter refers to the ID of the app in the marketplace."
msgstr ""

#: ../../source/api-guide/de/app.rst:34
msgid "GET /de/app/{appID}/recommend/datasets"
msgstr ""

#: ../../source/api-guide/de/app.rst:35
msgid ""
"This method returns recommended datasets based on given app tags. The "
"``{appID}`` parameter refers to the ID of the app in the marketplace."
msgstr ""

