Overview
========

:doc:`installation/index`
	This guide contains information for the installation and maintenance of all components of WeLive platform.

:doc:`api-guide/index`
	For developers, this guide explains how to use the WeLive's API.
